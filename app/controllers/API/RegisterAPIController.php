<?php
require 'vendor/autoload.php';
use GuzzleHttp\Client;

class RegisterAPIController extends BaseController {

	public function signUpUser(){
		$data = Input::all();

		$return = User::signUpUser($data['users']);
		$results = json_decode($return);

		if($results->success == 'true'){

			$userdata = array(
				'u_email' 	=>$data['users']['u_email'],
				'password' => $data['users']['u_password'],
				'isactive' => '1',
				'u_accounttype'=>'local'
			);

			if (Auth::attempt($userdata)) {
				$result['success'] = $results->success;
				$result['page'] = '/profile';
				return $result;
			} else {
				$result['success'] = 'false';
				$result['msg'] = 'WARNING: Unknown error occur while logging in the account';
				$result['page'] = '/join';
				return Response::json($result);
			}
		}else{
			$result['success'] = $results->success;
			$result['msg'] = $results->msg;
			$result['page'] = '/join';
			return Response::json($result);
		}
	}



	public function saveEmployer() {
		$data = Input::all();

		if(strlen($data['employer']['password']) < 8){
			$result['success'] = 'false';
			$result['msg'] = "WARNING: Password doesn\'t match";
			return Response::json($result);
		}elseif($data['employer']['password'] != $data['employer']['password1']){
			$result['success'] = 'false';
			$result['msg'] = "WARNING: Password doesn\'t match";
			return Response::json($result);
		}else{

			$return = Employer::saveEmployer($data['employer']);
			$results = json_decode($return);

			if($results->success == 'true'){

				$userdata = array(
					'u_email' 	=>$data['employer']['email'],
					'password' => $data['employer']['password'],
					'isactive' => '1',
					'u_accounttype'=>'local'
				);

				if (Auth::attempt($userdata)) {
					$result['success'] = $results->success;
					$result['page'] = '/EmployerProfile';
					return Response::json($result);
				} else {
					$result['success'] = 'false';
					$result['msg'] = 'WARNING: Unknown error occur while logging in the account';
					$result['page'] = '/EmployerForm';
					return Response::json($result);
				}
			}elseif($results->field != ''){
					$result['field'] = $results->field;
					$result['fieldError'] =$results->fieldError;
					$result['success'] = $results->success;
					$result['msg'] =$results->msg;
					return Response::json($result);
			}else{
					$result['success'] = $results->success;
					$result['msg'] = $results->msg;
					$result['page'] = '/EmployerForm';
					return Response::json($result);
			}
		}

	}

}
