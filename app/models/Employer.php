<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Employer extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	protected $table = 'employers';

	public static function saveEmployer($data) {

		$checkEmail = DB::select('SELECT * FROM users WHERE u_email = ? AND u_accounttype = ?',array($data['email'],$data['u_accounttype']));

			if($checkEmail) {
				$result['field'] = '#email';
				$result['fieldError'] = '<li class="parsley-required">This email is already in use. <a href="/Employer"  class="ParsleyErrorHref"> Login here.</a></li>';
				$result['success'] = false;
				$result['msg'] = 'WARNING: Please use another email address.Email Adress already Exist!';
			} else {

				$token = bin2hex(mcrypt_create_iv(16, MCRYPT_DEV_RANDOM));
				$users = array(
					'u_email' 		=> 	$data['email'],
					'u_password' 	=> 	Hash::make($data['password']),
					'u_address'		=>	$data['address'],
					'u_telephoneno'	=>	$data['landline'],
					'u_mobileno'	=>	$data['mobile'],
					'u_website'		=>	$data['website'],
					'u_accounttype' =>	$data['u_accounttype'],
					'u_classification'	=> 'employer',
					'u_verificationurl'	=> $token,
					'u_isverified'	=>	0,
					'isactive'		=>	1,
					'isadmin'		=>	0);

				$u_id = DB::table('users')->insertGetId($users);

				$results = DB::table('employers')
						->insert(array(
							'e_companyname'	=> 	$data['compname'],
							'e_description'	=>	$data['desc'],
							'u_id'			=>	$u_id));

				$fullname =  $data['compname'];
				$email =  $data['email'];
				$info = array('email'=>$email,'fullname'=>$fullname,'token'=>$token,'mode'=>'e');

				Mail::send('emails.confirmation',$info, function($message)use($email,$fullname){
				    $message->to($email,$fullname);
				    $message->from('no-reply@mydomain.com','Mindanao Jobs');
				    $message->subject('Account Confirmation');
				});

				if($results){
					$result['success'] = 'true';
					$result['msg'] = 'Record Successfully Saved';
				}else{
					$result['success'] = 'false';
					$result['msg'] = 'WARNING: Unknown error occur while saving the record';
				}
			}
			return json_encode($result);
		}

		public static function getCurrentEmployer($u_id) {
			return DB::select('SELECT * FROM users u INNER JOIN employers e ON u.u_id = e.u_id WHERE u.u_id = ?', $u_id);
		}

		public static function updateEmployer($u_id,$data) {

			$info = array($data['compname'],$data['desc'],$data['email'],$data['website'],$data['address'],$data['stateInput'],$data['city'],$data['zipCode'],$data['telephone'],$data['mobile'],$u_id);
			$results = DB::update('UPDATE users u INNER JOIN employers e ON e.u_id = u.u_id set e.e_companyname = ?, e.e_description = ?, u.u_email = ?, u.u_website = ?, u.u_address = ?,u.u_province = ?,u.u_city = ?,u.u_zipcode = ?, u.u_telephoneno = ?, u.u_mobileno = ? Where u.u_id = ?', $info);

			if($results){
				$result['success'] = true;
				$result['msg'] = 'Record Successfully Saved';
			}else{
				$result['success'] = false;
				$result['msg'] = 'WARNING: Unknown error occur while saving the record';
			}
			return $result;
		}

		public static function updateEmployerPassword($id,$data){

			if($data['newpassword'] != $data['confirm_password']){
				/*$result['field'] = '#newPasswordInput';
				$result['fieldError'] = '<li class="parsley-required">New password does not match with Verify password</li>';*/
				$result['success'] = false;
				$result['msg'] = 'WARNING: New password does not match with Verify password!';
				return json_encode($result);
			}

		$user = DB::select('SELECT * FROM users WHERE u_id = ? LIMIT 1;', array($id));
		if (Hash::check($data['password'],$user[0]->u_password)){
			$arr = array(Hash::make($data['newpassword']),$id);
			$qry_results = DB::update('UPDATE users SET u_password=? WHERE u_id = ?', $arr);
			if($qry_results){
				$result['success'] = true;
				$result['msg'] = 'User password successfully changed';
			}else{
				$result['success'] = false;
				$result['msg'] = 'WARNING: Unknown error occur while changing password!';
			}
		}else{
			/*$result['field'] = '#password';
			$result['fieldError'] = '<li class="parsley-required">Your Current password does not match</li>';*/
			$result['success'] = false;
			$result['msg'] = 'WARNING: Your Current password does not match!';

		}
		return json_encode($result);
	}


	public static function uploadPic($u_id,$file) {
		$destinationPath = 'public/images/employer/';
		$extension =$file->getClientOriginalExtension();
		$fileName = $u_id .".".$extension;
		$file->move($destinationPath, $fileName);

		$info = array($fileName,$u_id);

		$results = DB::update('UPDATE users u SET u.u_displaypicture = ? Where u.u_id = ?', $info);

		if($results){
			$result['img1'] = $destinationPath.$fileName;
			$result['img'] = $destinationPath.$fileName;
			$result['success'] = true;
			$result['msg'] = 'Record Successfully Saved';
		}else{
			$result['success'] = false;
			$result['msg'] = 'WARNING: Unknown error occur while saving the record';
		}
		return $result;
	}

}
