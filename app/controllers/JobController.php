<?php
require 'vendor/autoload.php';
use GuzzleHttp\Client;

class JobController extends BaseController {

	public function CreateJob() {
		$data = Input::all();
		$id = Auth::id();
		$currentEmployer = Employer::getCurrentEmployer(array($id));
		$e_id = $currentEmployer[0]->e_id;

		if(is_numeric($data['job']['salaryfrom']) && is_numeric($data['job']['salaryTo']) && is_numeric($data['job']['experience'])){
			return Job::CreateJob($e_id,$data['job']);
		}else{
			$result['success'] = false;
			$result['msg'] = 'WARNING: Unknown error occur while adding jobs.';
			return $result;
		}

	}

	public function JobDelete(){

		$parameter = Input::get('id');
		if(!empty($parameter)){
			$callback = Job::DeleteJob(array($parameter));

			Session::flash('message', $callback);
			return $callback;

		}
	}

	public function searchJob(){

		$parameter = Input::all();

		if (Request::ajax()){

			$param = '%'.$parameter['value'].'%';
			$place = $parameter['place'];

			$pageLimit = 10;
			$page = Input::get('page', 1);
			$page = $page - 1;

			$AllJobs = Job::getAllJobs(array($param,$param,$param,$place),array($page,$pageLimit));

			$newData = array();
			$arrDay = [];
			foreach ($AllJobs['paging'] as $key ) {
				$skilssarr = array();
				$categoryArr = array();

				foreach ($AllJobs['skills'] as $row ) {
					if($key->j_id == $row->j_id){
						array_push($skilssarr,array('skills'=> $row));
					}
				}
				foreach ($AllJobs['category'] as $row ) {
					if($key->j_id == $row->j_id){
						array_push($categoryArr,array('category'=> $row));
					}
				}

				$newData['categories'] = $categoryArr;
				$newData['skills'] = $skilssarr;
				$newData['info'] = array('data'=> $key);
				array_push($arrDay,$newData);
			}
			$paginator = Paginator::make($arrDay, count($AllJobs['info']), $pageLimit );

			return $paginator;
		}
	}

	public function searchJobForEmployee(){

		$parameter = Input::all();

		if (Request::ajax()){

			$param = '%'.$parameter['value'].'%';
			$active = $parameter['active'];
			$type = $parameter['type'];

			$pageLimit = 10;
			$page = Input::get('page', 1);
			$page = $page - 1;

			$AllJobs = Job::SearchEmpjob(array($param,$active,$type),array($page,$pageLimit));

			$newData = array();
			$arrDay = [];
			foreach ($AllJobs['paging'] as $key ) {
				$skilssarr = array();
				$categoryArr = array();

				foreach ($AllJobs['skills'] as $row ) {
					if($key->j_id == $row->j_id){
						array_push($skilssarr,array('skills'=> $row));
					}
				}
				foreach ($AllJobs['category'] as $row ) {
					if($key->j_id == $row->j_id){
						array_push($categoryArr,array('category'=> $row));
					}
				}

				$newData['categories'] = $categoryArr;
				$newData['skills'] = $skilssarr;
				$newData['info'] = array('data'=> $key);
				array_push($arrDay,$newData);
			}
			$paginator = Paginator::make($arrDay, count($AllJobs['info']), $pageLimit );

			return $paginator;
		}
	}


	public function showJobListByCategory(){
		$parameter = Input::all();
		if (Request::ajax()){

			$param = '%'.$parameter['cat'].'%';
			$place = $parameter['place'];

			$pageLimit = 10;
			$page = Input::get('page', 1);
			$page = $page - 1;

			$AllJobs = Job::getJobByCategory(array($param,$place),array($page,$pageLimit));

			$newData = array();
			$arrDay = [];
			foreach ($AllJobs['info'] as $key ) {
				$skilssarr = array();
				$categoryArr = array();

				foreach ($AllJobs['skills'] as $row ) {
					if($key->j_id == $row->j_id){
						array_push($skilssarr,array('skills'=> $row));
					}
				}
				foreach ($AllJobs['category'] as $row ) {
					if($key->j_id == $row->j_id){
						array_push($categoryArr,array('category'=> $row));
					}
				}

				$newData['categories'] = $categoryArr;
				$newData['skills'] = $skilssarr;
				$newData['info'] = array('data'=> $key);
				array_push($arrDay,$newData);
			}

			return $arrDay;
		}
	}


	public function filterByPlace(){

		$parameter = Input::all();

		if (Request::ajax()){

			$param = '%'.$parameter['place'].'%';

			$pageLimit = 10;
			$page = Input::get('page', 1);
			$page = $page - 1;

			$AllJobs = Job::getJobByPlaces(array($param),array($page,$pageLimit));

			$newData = array();
			$arrDay = [];
			foreach ($AllJobs['info'] as $key ) {
				$skilssarr = array();
				$categoryArr = array();

				foreach ($AllJobs['skills'] as $row ) {
					if($key->j_id == $row->j_id){
						array_push($skilssarr,array('skills'=> $row));
					}
				}
				foreach ($AllJobs['category'] as $row ) {
					if($key->j_id == $row->j_id){
						array_push($categoryArr,array('category'=> $row));
					}
				}

				$newData['categories'] = $categoryArr;
				$newData['skills'] = $skilssarr;
				$newData['info'] = array('data'=> $key);
				array_push($arrDay,$newData);
			}

			return $arrDay;
		}
	}



	public function getJobSkills(){
		if (Request::ajax()){
			$j_id = Input::get( 'id' );
			if (!empty($j_id)) {
				$skills = Job::getJobSkills(array($j_id));
				return $skills;
			}
		}
	}

	public function getJobCategories(){
		$j_id = Input::get( 'id' );
		if (!empty($j_id)) {
			$categories = Job::getJobCategories(array($j_id));
			return $categories;
		}
	}

	public function UpdateJob(){
		$data = Input::all();
		$id = Auth::id();


		$currentEmployer = Employer::getCurrentEmployer(array($id));
		$e_id = $currentEmployer[0]->e_id;

		if(is_numeric($data['job']['salaryfrom']) && is_numeric($data['job']['salaryTo']) && is_numeric($data['job']['experience'])){
			return Job::updateJob($e_id,$data['job']);
		}else{
			$result['success'] = false;
			$result['msg'] = 'WARNING: Unknown error occur while updating job information!';
			return $result;
		}

	}

	public function applyJob(){

		$data = Input::all();
		if (Request::ajax()){

			$id = Auth::id();
			$currentUser = User::getCurrentUser(array($id));
			$currentUser = $currentUser[0];

			if ($currentUser->u_isverified =='1'){

				$fullname = $currentUser->js_firstname.' '.$currentUser->js_lastname;
				$email = Input::get('data')['mail'];
				$messages = nl2br(htmlspecialchars(Input::get('data')['message']));
				$info = array('email'=>$email,'fullname'=>$fullname,'messages'=>$messages);

				$pathToFile = public_path(). "/resume/".$currentUser->u_resume;
				$withAttach = $data['data']['attached'];

				if($withAttach == 'true'){
				    Mail::send('emails.blank',$info, function($message)use($email,$fullname,$pathToFile,$withAttach){
					    $message->to($email,$fullname);
					    $message->from('no-reply@mindanaojobs.com','Mindanao Jobs');
					    $message->subject('MindanaoJobs Job Application');
					    $message->attach($pathToFile,array('as' => 'resume','mime' => 'application/msword'));
					});
					if( count(Mail::failures()) > 0 ) {
					   	$result['success'] = false;
						$result['msg'] = 'There was an error while sending the email. Please try again';
					} else {
						$result['success'] = true;
						$result['msg'] = 'Email successfully sent';
					}
					return $result;
				}else{
					Mail::send('emails.blank',$info, function($message)use($email,$fullname){
					    $message->to($email,$fullname);
					    $message->from('no-reply@mydomain.com','Mindanao Jobs');
					    $message->subject('MindanaoJobs Job Application');
					});

					if( count(Mail::failures()) > 0 ) {
					   	$result['success'] = false;
						$result['msg'] = 'There was an error while sending the email. Please try again';
					} else {
						$result['success'] = true;
						$result['msg'] = 'Email successfully sent';
					}
					return $result;
				}
			}else{
				$result['success'] = false;
				$result['msg'] = 'Unable to apply job,account is not yet verify. Please verify to continue this process';
				return $result;
			}

		}
	}


}
