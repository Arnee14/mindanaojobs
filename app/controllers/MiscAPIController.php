<?php
require 'vendor/autoload.php';
use GuzzleHttp\Client;

class MiscAPIController extends BaseController {

	public function getAllJobLocation(){
		$client = new Client();
		$response = $client->get('/json/location.json');
		return json_decode('hello');
	}

	public function getAllJobLocation2(){
		$client = new Client();
		$response = $client->get($this->base_url.'/api/joblocation2');
		return json_decode($response->getBody());
	}

	public function getAllJobCategories(){
		$client = new Client();
		$response = $client->get($this->base_url.'/api/jobcategories');
		return json_decode($response->getBody());
	}

	public function getAllSkills(){
		$client = new Client();
		$response = $client->get($this->base_url.'/api/skills');
		return json_decode($response->getBody());
	}

	public function getAllMonths(){
		$client = new Client();
		$response = $client->get($this->base_url.'/api/allmonths');
		return json_decode($response->getBody());
	}

}

?>
