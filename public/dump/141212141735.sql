/*
MySQL Backup
Source Server Version: 5.6.12
Source Database: mydb
Date: 12/12/2014 14:17:36
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
--  Table structure for `education`
-- ----------------------------
DROP TABLE IF EXISTS `education`;
CREATE TABLE `education` (
  `js_id` int(11) NOT NULL,
  `ed_attainment` varchar(100) DEFAULT NULL,
  `ed_field` varchar(255) DEFAULT NULL,
  `ed_major` varchar(100) DEFAULT NULL,
  `ed_institution` text,
  `ed_address` varchar(45) DEFAULT NULL,
  `ed_yearfrom` year(4) DEFAULT NULL,
  `ed_yearto` year(4) DEFAULT NULL,
  `isactive` tinyint(1) DEFAULT NULL,
  KEY `fk_education_jobseekers1_idx` (`js_id`),
  CONSTRAINT `fk_education_jobseekers1` FOREIGN KEY (`js_id`) REFERENCES `jobseekers` (`js_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `employers`
-- ----------------------------
DROP TABLE IF EXISTS `employers`;
CREATE TABLE `employers` (
  `e_id` int(11) NOT NULL AUTO_INCREMENT,
  `e_companyname` text,
  `u_id` int(11) NOT NULL DEFAULT '0',
  `e_description` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`e_id`),
  KEY `fk_employers_users1_idx` (`u_id`),
  CONSTRAINT `fk_employers_users1` FOREIGN KEY (`u_id`) REFERENCES `users` (`u_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `job_applicants`
-- ----------------------------
DROP TABLE IF EXISTS `job_applicants`;
CREATE TABLE `job_applicants` (
  `j_id` int(11) NOT NULL,
  `js_id` int(11) NOT NULL,
  `isactive` tinyint(1) DEFAULT NULL,
  KEY `fk_job_applicants_jobs1_idx` (`j_id`),
  KEY `fk_job_applicants_jobseekers1_idx` (`js_id`),
  CONSTRAINT `fk_job_applicants_jobs1` FOREIGN KEY (`j_id`) REFERENCES `jobs` (`j_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_job_applicants_jobseekers1` FOREIGN KEY (`js_id`) REFERENCES `jobseekers` (`js_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `job_categories`
-- ----------------------------
DROP TABLE IF EXISTS `job_categories`;
CREATE TABLE `job_categories` (
  `j_id` int(11) DEFAULT NULL,
  `jc_jobcategory` varchar(255) DEFAULT NULL,
  KEY `j_id` (`j_id`),
  CONSTRAINT `j_id` FOREIGN KEY (`j_id`) REFERENCES `jobs` (`j_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `job_skills`
-- ----------------------------
DROP TABLE IF EXISTS `job_skills`;
CREATE TABLE `job_skills` (
  `j_id` int(11) DEFAULT NULL,
  `js_skill` varchar(100) DEFAULT NULL,
  KEY `FK_job_skills_1` (`j_id`),
  CONSTRAINT `FK_job_skills_1` FOREIGN KEY (`j_id`) REFERENCES `jobs` (`j_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `jobs`
-- ----------------------------
DROP TABLE IF EXISTS `jobs`;
CREATE TABLE `jobs` (
  `j_id` int(11) NOT NULL AUTO_INCREMENT,
  `j_title` varchar(255) DEFAULT NULL,
  `j_description` text,
  `j_requirements` text,
  `j_salaryfrom` varchar(30) DEFAULT NULL,
  `j_salaryto` varchar(30) DEFAULT NULL,
  `j_employmentbasis` varchar(50) DEFAULT NULL,
  `j_createdon` timestamp NULL DEFAULT NULL,
  `j_createdby` int(11) DEFAULT NULL,
  `isactive` tinyint(1) DEFAULT NULL,
  `j_experience` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`j_id`),
  KEY `fk_jobs_employers1_idx` (`j_createdby`),
  CONSTRAINT `fk_jobs_employers1` FOREIGN KEY (`j_createdby`) REFERENCES `employers` (`e_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `jobs_categories`
-- ----------------------------
DROP TABLE IF EXISTS `jobs_categories`;
CREATE TABLE `jobs_categories` (
  `js_id` int(11) NOT NULL,
  `jc_name` varchar(150) DEFAULT NULL,
  `isactive` tinyint(1) DEFAULT NULL,
  KEY `fk_jobs_categories_job_categories1_idx` (`js_id`),
  CONSTRAINT `fk_jobs_categories_jobseekers1` FOREIGN KEY (`js_id`) REFERENCES `jobseekers` (`js_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `jobseekers`
-- ----------------------------
DROP TABLE IF EXISTS `jobseekers`;
CREATE TABLE `jobseekers` (
  `js_id` int(11) NOT NULL AUTO_INCREMENT,
  `js_lastname` varchar(255) DEFAULT NULL,
  `js_firstname` varchar(255) DEFAULT NULL,
  `js_middleinitial` varchar(255) DEFAULT NULL,
  `js_gender` varchar(10) DEFAULT NULL,
  `js_birthdate` date DEFAULT NULL,
  `js_desiredsalary` decimal(10,2) DEFAULT NULL,
  `js_desc` text,
  `js_notif_job_posting` tinyint(1) DEFAULT NULL,
  `js_notif_job_updates` tinyint(1) DEFAULT NULL,
  `js_notif_tips` tinyint(1) DEFAULT NULL,
  `u_id` int(11) NOT NULL,
  PRIMARY KEY (`js_id`),
  KEY `fk_jobseekers_users1_idx` (`u_id`),
  CONSTRAINT `fk_jobseekers_users1` FOREIGN KEY (`u_id`) REFERENCES `users` (`u_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `skills`
-- ----------------------------
DROP TABLE IF EXISTS `skills`;
CREATE TABLE `skills` (
  `js_id` int(11) NOT NULL,
  `s_name` varchar(255) DEFAULT NULL,
  `s_level` varchar(30) DEFAULT NULL,
  `isactive` tinyint(1) DEFAULT NULL,
  KEY `fk_skills_jobseekers1_idx` (`js_id`),
  CONSTRAINT `fk_skills_jobseekers1` FOREIGN KEY (`js_id`) REFERENCES `jobseekers` (`js_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `u_id` int(11) NOT NULL AUTO_INCREMENT,
  `u_email` varchar(100) DEFAULT NULL,
  `u_password` varchar(100) DEFAULT NULL,
  `u_address` text,
  `u_city` varchar(255) DEFAULT NULL,
  `u_province` varchar(255) DEFAULT NULL,
  `u_zipcode` varchar(20) DEFAULT NULL,
  `u_telephoneno` varchar(100) DEFAULT NULL,
  `u_mobileno` varchar(100) DEFAULT NULL,
  `u_fax` varchar(100) DEFAULT NULL,
  `u_website` varchar(255) DEFAULT NULL,
  `u_displaypicture` varchar(255) DEFAULT NULL,
  `u_resume` varchar(150) DEFAULT NULL,
  `u_isverified` tinyint(1) DEFAULT NULL,
  `u_verificationurl` text,
  `u_recoveryurl` text,
  `u_isrecover` tinyint(1) DEFAULT NULL,
  `u_accounttype` varchar(45) DEFAULT NULL,
  `u_classification` varchar(45) DEFAULT NULL,
  `u_createdon` timestamp NULL DEFAULT NULL,
  `isadmin` tinyint(1) DEFAULT NULL,
  `isactive` tinyint(1) DEFAULT NULL,
  `remember_token` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`u_id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `work_experience`
-- ----------------------------
DROP TABLE IF EXISTS `work_experience`;
CREATE TABLE `work_experience` (
  `js_id` int(11) NOT NULL,
  `we_companyname` text,
  `we_position` varchar(255) DEFAULT NULL,
  `we_description` text,
  `we_location` varchar(45) DEFAULT NULL,
  `we_yearstart` year(4) DEFAULT NULL,
  `we_yearend` year(4) DEFAULT NULL,
  `isactive` tinyint(1) DEFAULT NULL,
  KEY `fk_work_experience_jobseekers1_idx` (`js_id`),
  CONSTRAINT `fk_work_experience_jobseekers1` FOREIGN KEY (`js_id`) REFERENCES `jobseekers` (`js_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records
-- ----------------------------
INSERT INTO `education` VALUES ('8','College','BS -it','database system','Mindao State University at naawan','naawan misamis oriental','2010','2014',NULL), ('8','College','DET','Computer Technology','Mindanao State University at naawan','naawan misamis oriental','2008','2010',NULL), ('14','fsdfs','it','database system','sdfs','patag','2000','2005',NULL);
INSERT INTO `employers` VALUES ('1','People index','3','Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.            asdsadsadsdasd\'\'\'                    '), ('5','Peopsdasdnk','4','smdkmaslkdmnlkasdnlkansdlnlasndlkasfhkehwrfjopqwjeoiqjwdojasl;kdjalsdjlasjdkljaklsdmlkasmkdlas\nd\nas\ndasdmaslkdjlkasdklasd\nasd;laskd;lask;d;asd\nasdaslmdlknlkadsnlasd\nasdjlaskdjlksajdklasd\nasdlkasjdlknklasdasd                                                    '), ('6','asdasdas','5','aksdaklshdklashjfklasjkldjklasldasf\na\nf\nas\nfaskljflkajslkfjklaslf\nasfkajsklflkasf\nasfkljaslkfjlkasf\nasfjasjfljlaskf\nasfkgowuronq\n                                                    '), ('7','Echo Solution','6','Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.                                                    '), ('9','hoasd','20','Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.'), ('10','wala lang','21','kani wala lang ni siya kay ako lang na ni i try hahahaha tro loang jodjl lkjsfdlksfdsd we4r are the chamooj my friend and we are keeep on fighting tell the end\n'), ('13','try lang inkilskdjf;alkdadf','25','sakldjfasdlkfjlaskjdfasdfasdf you ar e tl  sdlkj a   wla lang ni ahahaha yo nidlfljsd lkjlfd walala dkjlfsdkfj al  ljust tano ;aljdfa;lkjdf;akljdf;alkjd;flkajfd;lakjdf;lajd;lfkja;sdklfja you are the one who makes me happy'), ('14','4Loop','26','Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.                                                '), ('15','wala lang ','27','testing for the current resul  we are the champion my friend and we will keep on fighting tell the end , we are the champion of the world , what ate the , i guess the time is right for us to say , will thal'), ('16','wala lang','28','asdfadsfadfaasdfasdfasdf asdfadsfadfaasdfasdfasdf asdfadsfadfaasdfasdfasdf asdfadsfadfaasdfasdfasdf asdfadsfadfaasdfasdfasdf asdfadsfadfaasdfasdfasdf asdfadsfadfaasdfasdfasdf asdfadsfadfaasdfasdfasdfasdfadsfadfaasdfasdfasdf asdfadsfadfaasdfasdfasdf  asdfadsfadfaasdfasdfasdf asdfadsfadfaasdfasdfasdf                                                                                                                                                '), ('17','kwan','29','kwan jkwaljk a kwan jkwaljk a  kwan jkwaljk a  kwan jkwaljk a  kwan jkwaljk a  kwan jkwaljk a kwan jkwaljk a kwan jkwaljk a kwan jkwaljk a kwan jkwaljk a kwan jkwaljk a kwan jkwaljk a kwan jkwaljk a kwan jkwaljk a kwan jkwaljk a kwan jkwaljk a kwan jkwaljk a kwan jkwaljk a kwan jkwaljk a kwan jkwaljk a kwan jkwaljk a kwan jkwaljk a kwan jkwaljk a kwan jkwaljk a '), ('18','dasdasdas','30','Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.'), ('19','sadasdas','31','Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.                                                ');
INSERT INTO `job_categories` VALUES ('14',' Banking/Financial Jobs'), ('15',' Banking/Financial Jobs'), ('12',' Banking/Financial Jobs'), ('12',' Audit & Taxation Jobs'), ('12',' General/Cost Accounting Jobs'), ('13',' Audit & Taxation Jobs'), ('13',' General/Cost Accounting Jobs'), ('13',' Human Resources Jobs'), ('16',' Human Resources Jobs'), ('20',' Human Resources Jobs'), ('21',' Human Resources Jobs'), ('22',' Banking/Financial Jobs'), ('23',' Clerical/Administrative Jobs'), ('24',' General/Cost Accounting Jobs'), ('24',' General/Cost Accounting Jobs'), ('24',' Environmental Engineering Jobs');
INSERT INTO `job_skills` VALUES ('14','3ds Max'), ('14','Adobe Flash'), ('15','3D Modelling'), ('15','ActionScript'), ('15','Samsung'), ('12','3D Rendering'), ('12','3D Design'), ('12','Advertisement Design'), ('12','After Effects'), ('13','3D Animation'), ('13','3D Modelling'), ('13','Flash 3D'), ('16','3D Modelling'), ('20','3D Design'), ('21','Infographics'), ('21','Microsoft MapPoint'), ('22','3D Design'), ('23','3D Design'), ('24','3D Design'), ('24','3D Design'), ('24','3D Design');
INSERT INTO `jobs` VALUES ('12','Andsa','Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.','asdasd','122222','222222','Full-time','2014-11-12 02:43:51','1','1','1'), ('13','Janitor','Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.','The aldajks','9000','10000','Full-time','2014-11-12 02:46:17','1','1','1'), ('14','asd','Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.','asdas','1212','122','Full-time','2014-11-12 06:00:55','1','1','1'), ('15','asdasdsa','Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.','13131','1231','12313','Contract','2014-11-12 06:02:33','7','1','1'), ('16','adasd','Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.','asdasd','12312','1231','Part-time','2014-12-11 04:00:09','1','1','1'), ('20','vvvvvvvvvvvvv','Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.','asdas','12','1212','Full-time','2014-12-11 05:54:07','1','1','1'), ('21','Manager','kani kay gra be ka nin dotsadfadfadfadfadfadfasdfsdfsdfsfdsdsdsdfsdf , asdhfkjahdsfjahdfjaldjad, aosdjfladjflakjsdflksjd, alskdjflajdsflajdlfkjld, you are  the one who makes me happy','ok nasdf','50000','1000000','Part-time','2014-12-11 10:10:37','13','1','5'), ('22','fghdsf','Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.','2132131','1231','12','Full-time','2014-12-12 01:30:08','14','1','12'), ('23','sdfs','sdfssdfsf   sdfssdfsf  sdfssdfsf  sdfssdfsf  sdfssdfsf  sdfssdfsf  sdfssdfsf sdfssdfsf sdfssdfsf sdfssdfsf sdfssdfsf sdfssdfsf sdfssdfsf sdfssdfsf sdfssdfsf ','ssdfs','500','5000','Part-time','2014-12-12 01:52:58','16','1','80'), ('24','php developerdsfsdf','wer are the chapmopskflkjs;lkdjf a   wala lang ni tyr labng  lskjdfls wer are the chamoijlkdj;lkdjf \n\n\ndlsjflskjdf \nwe lfjadlskjdfsfddfsa','dfssdf','5005','505','Part-time','2014-12-12 02:26:38','19','1','8');
INSERT INTO `jobs_categories` VALUES ('8','IT - Hardware Jobs','1'), ('8','IT - Network/Sys/DB Admin Jobs','1'), ('8','IT - Software Jobs','1'), ('12','IT - Hardware Jobs','1'), ('12','IT - Network/Sys/DB Admin Jobs','1'), ('12','IT - Software Jobs','1'), ('14','Manufacturing Jobs','1'), ('14','Geology/Geophysics Jobs','1'), ('3','Clerical/Administrative Jobs','1'), ('3','Human Resources Jobs','1'), ('3','Secretarial Jobs','1'), ('3','Top Management Jobs','1'), ('3','IT - Hardware Jobs','1'), ('3','IT - Network/Sys/DB Admin Jobs','1'), ('3','IT - Software Jobs','1');
INSERT INTO `jobseekers` VALUES ('3','Garay','Philip Cesar','B','Male','1992-06-30','25000.00','Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.\n\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,NULL,NULL,'7'), ('4','Garay','Janphil',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'8'), ('5','Garay','Philip Cesar',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'9'), ('6','Garay','Philip',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'10'), ('8','kwan','juneril',NULL,NULL,NULL,'20000.00','this is the salary, that i can do sa sdfsdfsdfmsdkflskdjflkjsldkjfljsldkjflklskjdfjlsjdlfjsldjfklsjlfsdfsdfsdsfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsfdsfsfds',NULL,NULL,NULL,'12'), ('12','asdasd','pi.juneiril',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'16'), ('14','Kwan','Juneril','l','Male','0000-00-00','50000.00','we are the champions my friend and we will keep on fighting tell the end we are the champions of the world',NULL,NULL,NULL,'19'), ('15','Ragadio','Neil',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'22');
INSERT INTO `skills` VALUES ('8','Database Programming','','1'), ('8','3D Modelling','','1'), ('8','Adobe Dreamweaver','','1'), ('8','Visual Basic','','1'), ('8','Java','','1'), ('12','3D Animation','','1'), ('12','Autodesk Revit','','1'), ('12','Photoshop','','1'), ('3','3D Animation','','1'), ('3','Photoshop Design','','1'), ('3','PhoneGap','','1');
INSERT INTO `users` VALUES ('1','admin@admin.com','$2y$10$qPG79tRgg/gSWhjzK1wXpu2zENgQ/m90IlL4HQ1uSOSMlAoU7E1z6',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0',NULL,NULL,NULL,'local','employer',NULL,'0','1','LNTC6YDEm09cj7yvS6pj7eo9l6tUlVKyRiL4FhZIUWyMfvQxRi2VsIdECEHf','2014-12-11 08:18:04'), ('3','admin1@admin1.com','$2y$10$HIqEV8jPOvIs56rEWmSKsudzu336gcznaaZOiVZACfTR9m4K6AUau','Bukidnon','Bukidnon','Misamis Oriental','9023','677 888 999','0910985875',NULL,'www.com.com\'\'\'\'','3.png',NULL,'0','b3f77e181195bf7a99a62a78b439dc57',NULL,NULL,'local','employer',NULL,'0','1','QOAPQOPr1SRMYSIRrWfi7A5EFIjFsW1osvULjg4lL0ntfCtNh34lZ3ZrqZuI','2014-12-12 05:37:51'), ('4','wew@wew.com','$2y$10$eomTxXaGAZ3/L0BxKrg56O4ZzyhMyyB3LcCaFoIqFYHjfh89vqiW6','hksadnkajsndk',NULL,NULL,NULL,'7438974892379','98789723894789',NULL,'ww.omc.omc.',NULL,NULL,'0',NULL,NULL,NULL,'local','employer',NULL,'0','1','qakyUJZkecNUD3OAE6lmvOPZovX7lzq313vrAI4TC0JdUGJod1WT5IOwiPAH','2014-11-11 07:41:53'), ('5','w@w.com','$2y$10$5P5sGithO1pmelHU/KhinOzAudzf4ULZhzWJO6tX0Ma/hUt3JgTJm','dfasda',NULL,NULL,NULL,'123112312','12321',NULL,'www.woc.',NULL,NULL,'0',NULL,NULL,NULL,'local','employer',NULL,'0','1',NULL,NULL), ('6','echo@info.com','$2y$10$9EK8ZXLESGc1gz.3yjqWyubeEIzu6L3C.tr/0b1tteNwawa/Z/omu','Naawan',NULL,NULL,NULL,'7839247','798789',NULL,'www.echo.com',NULL,NULL,'0',NULL,NULL,NULL,'local','employer',NULL,'0','1','Nc0QbmoIKAwMm2JSJhBMmjnhFtSoOvKckxS1P094XU1UDvyGNs5vIQInz0tp','2014-12-08 11:45:18'), ('7','philipgaray2@gmail.com','$2y$10$E6POTIKw0B7ZBISNozi6rurFD5OfFgqSBAY0cqQFRoItzYeC0kP.m','Zone 2 Luis Hambre Str.','Cagayan de Oro City','Misamis Oriental','9023','09394049310','09394049310','N/A','www.facebook.com','../images/users/7.jpg','7.docx','1','62283bd669fc425e470b27a9d8eb9673','025c0872b3a36c6b6af998aac8d07571','1','local','jobseeker','2014-11-12 07:57:16','0','1','Qu4W42l4hYl42tAE0exll5xA9ZRUFkGtvUWpBeueH1ndCUWsTCbOtB7ZcMgq','2014-12-12 06:06:48'), ('8','philipgaray2@gmail.com','$2y$10$zbhoCmsU0Cize.azBqqhE.P89tpLoYhnzfsuRT7det5gcuixCp.oa',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0',NULL,NULL,NULL,'facebook','jobseeker','2014-11-28 10:04:07','0','1','M42l8hR77jdHd0DBWarcYhjVqcKf1NDNoiayF423j7HfRgonzFhkKHpWtu6Q','2014-11-28 10:24:44'), ('9','philipgaray2@gmail.com','$2y$10$S8EmlRNHdurFg17jkDMw/OyO4tjmmiWo4v1fj3aCrqKKZwpgr3Kb.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0',NULL,NULL,NULL,'google','jobseeker','2014-11-28 10:25:44','0','1','FSO3Mg7SXadkeoREvXpbDOwbXCV9lVlCt0S3sA1ng9WENx1kiSEY8aKYlIzz','2014-12-11 11:48:18'), ('10','philipgaray2@gmail.com','$2y$10$zsP53EcuAi0/wUK9hG6f7eIhJxpuZJqhgvpmTyjTiE6OskVXydW5y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0',NULL,NULL,NULL,'linkedin','jobseeker','2014-11-28 10:28:53','0','1','9IwE77vb2SoWGWstzDEaGdaJmCHzUn5u8T0Pdc8zC2l9eqyNiHF0AsXwQ8M9','2014-11-28 10:34:54'), ('12','pi.juneril@gmail.com','$2y$10$tQlfk45YSfpPupgpJ4W15eKhyTKnEh5BFWBL3vIoXGF0hY9YK7EDK',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0',NULL,NULL,NULL,'local','jobseeker','2014-12-09 10:29:38','0','1','Uiq48oX3yDoRv7tVxr2sUe0eQk1IwatPbh3ZAcvFE0gj9ZErGvA9DbcPVw2b','2014-12-09 11:01:05'), ('16','pi.philipcesar@gmail.com','$2y$10$0CgaLDGeiJjE4Z89Ujq7zuS7nwbX63BvaHFz4RDMT/LGEHkMFZ9EK',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','ecb899bf352b3f00f71fcd53e8d5f4cb',NULL,NULL,'local','jobseeker','2014-12-10 06:34:57','0','1','mfCc7QMhEWe9oLodeu4HTaAQIXsjCvj87NntRCpCCHufNiS1KO6Umu32yrcq','2014-12-10 06:46:06'), ('19','kwan.juneril08@gmail.com','$2y$10$IA.jJO9DlnvDU2J5h5HhAOET9e2zJ48XJ3z82Qvzru4pCJMp3CPdS','patag, naawan misamis oriental','Agusan del Norte','misamis oriental','9023zXCX','1212112','09109688918','1212','','../images/users/19.jpg','19.docx','1','2ea5a6d933057f08570c508aed7a3ce1',NULL,NULL,'facebook','jobseeker','2014-12-11 05:43:47','0','1','qwVoDV8JSCAiJn0dAoABChnRVxpDunvmSpl0JzkTYBt89pgomCTq4nYvSY03','2014-12-12 05:54:02'), ('20','h@h.com','$2y$10$2zEqhT.9gmqB4n84NZ8yBuWK.VO4OEEBrF/wudeZGz2hofGNqfDv6','Bukidnon',NULL,NULL,NULL,'456452432','asdasda',NULL,'asdasd',NULL,NULL,'0','cd3826c06871ec4c4c941e3c711538b0',NULL,NULL,'local','employer',NULL,'0','1','PLF7eTxbjmo1Jl72Ab0PSPj6LlPyUUly2TWQNxqwh9Zhuyun9mnbwTtH82PY','2014-12-11 06:12:35'), ('21','kwan.juneril08@gmail.com','$2y$10$E/w8fGQ.pQvidAmEcBfju.P0jOzWQAdLqKtVR6yYadsQIh6VTZ8K6','Cagayan de Oro City',NULL,NULL,NULL,'','09109688918',NULL,'kwan.juneril@example.com',NULL,NULL,'1','e84f5d30aa5f37c4cb473252330d23d5',NULL,NULL,'local','employer',NULL,'0','1','RwtbnmMLAWMHzd1tlBjty9bU4HHDTrtlEBNu2lOjpyUmNBqV4VuzJFJjEhGH','2014-12-11 06:37:37'), ('22','ngr_24@ymail.com','$2y$10$tmpYvNGiHV3M6Wg0tJAtaeon484GG0c3qQEF9RKrDIlP9HJRLHcoy',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','9d26f6eb9bbf51c8d7fedae65ca4591d',NULL,NULL,'facebook','jobseeker','2014-12-11 08:20:31','0','1','jKIfQRvagdNseNUu9m3O5OBls91bz95CVVJODLhEYPhj8ClzWkTKDUHveXFK','2014-12-12 05:49:59'), ('25','admin@kwan.com','$2y$10$vEVnOmxLi47Uhw/xWJGTOuGG9xLHUW5wupP3gAfvrfZMYHtsUwV8O','Cagayan de Oro City',NULL,NULL,NULL,'','09109688918',NULL,'kwan.juneril@example.com',NULL,NULL,'0','4ceda17a8962bd8ccdd51f499f462527',NULL,NULL,'local','employer',NULL,'0','1','NBbAzGAq1EiSrS5wdX6gU4VAhVTu3jmtNh0UeqqJMejvy9dFL6T7SzVbQm0J','2014-12-11 10:20:38'), ('26','pi.neilr23@gmail.com','$2y$10$JjfL7yGCjGJCf3wlWaJdwujUzjpZhmIh2C4sWkE6JW37k.xjila06','Zamboanga del Sur','Bukidnon','dad123123','12312','657234','432423',NULL,'www.com.com',NULL,NULL,'1','a217d3c814eadf9c07fa51a6b1d07b05',NULL,NULL,'local','employer',NULL,'0','1','C4C1t53eP9BPeVbnNU6tZdPh4BpGAS7v3dwowLhpVWz5CVSyfWwKrzds8725','2014-12-12 01:35:43'), ('27','.@example.com','$2y$10$LbhJBUn6cBxWAhfSiDQzB.FA9Lz/fWqr7Rndd9azc5l7bzTdeIr5e','Bukidnon',NULL,NULL,NULL,'','09109688918',NULL,'.@example.com',NULL,NULL,'0','2c550c86c58b5da5d6c55c1e90b78f12',NULL,NULL,'local','employer',NULL,'0','1',NULL,NULL), ('28','kwan@example.com','$2y$10$nYogL/4YI0.Z7B0ic5VgoOdpbLthsqfqBep2phxBDfbnRXjCzQqpi','Bukidnon','Basilan','misamis oriental','9023','','09109688918',NULL,'kwan@example.com','28.png',NULL,'0','3d174505c4027702ab1c234d5d68282e',NULL,NULL,'local','employer',NULL,'0','1','wZVv3BjjCPdeANRA7PaLcy38SDJkcAEmtoYUH7hhp5PKC6iMkWj7Ex8qRNHi','2014-12-12 01:58:22'), ('29','aaaa@example.com','$2y$10$Kt1plJPoL.KRXAgCJrdFz.yGg2RGMVeRHjLOoSUBqlcks8KRasCRe','Bukidnon',NULL,NULL,NULL,'sdfsdfsf','09109688918',NULL,'aaaa@example.com',NULL,NULL,'0','611f3eeab822b3083f055c022564f30d',NULL,NULL,'local','employer',NULL,'0','1','TJLumSkpZAKliapXTqGHAq2z6sGR2kbv11BvHYWVtQsSpg42rHiIlaJ1tqfm','2014-12-12 02:10:23'), ('30','bago@bago.com','$2y$10$u66Z4g2bzPqYNtXygl2D4OBgCVeUU0S5ZMmm29qD0PvgepbS.mwtq','Bukidnon',NULL,NULL,NULL,'234324','234234',NULL,'www.bago.com',NULL,NULL,'0','6b929c1a62c4c8961db73ebfc8ab42e3',NULL,NULL,'local','employer',NULL,'0','1','a9lJWrq5EX3uDDtEfNPXqrAb5HG9jrOvjmlcRSgxWok6pgkjEohtzL6GPKGr','2014-12-12 02:18:56'), ('31','com@com.com','$2y$10$MCMz6haHA/0WPMxIdS0LiusJVJtSRyXOkAV8B3ZJa18F6TrGRxHua','Bukidnon','Bukidnon','asdas','123','354235','12321',NULL,'www.com.com',NULL,NULL,'0','3a2156ca86faafeb08b4d8b51953f81a',NULL,NULL,'local','employer',NULL,'0','1','ZBiXJWBFcaMegXFii8sKJr1bOUIzZThmiunUyaUqOfHwuPQnrCwKL2TIa7u7','2014-12-12 06:08:49');
INSERT INTO `work_experience` VALUES ('8','LCG','Computer Hardware and Software Maintenance','maintain Computer hardware and software and also  networking jobs','CDO','2009','2010',NULL), ('14','nothing ','managers','wala lang lin siys ltaljklfdsl lskdjl s and this is the one who makes me happpy','cdo','1998','1997',NULL);
