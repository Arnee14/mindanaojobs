<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Carbon\Carbon;

	class Job extends Eloquent implements UserInterface, RemindableInterface {
		use UserTrait, RemindableTrait;

		protected $table = 'jobs';

		public static function getJobById($EmpId,$EmpId2) {
			$resultCat = DB::select('SELECT * FROM job_categories WHERE j_id IN
						(SELECT j_id FROM jobs j
							INNER JOIN employers e ON e.e_id = j.j_createdby
							INNER JOIN users u ON u.u_id = e.u_id Where e.e_id = ? and j.isactive = 1)', $EmpId);
			$resultSki = DB::select('SELECT * FROM job_skills WHERE j_id IN
						(SELECT j_id FROM jobs j
							INNER JOIN employers e ON e.e_id = j.j_createdby
							INNER JOIN users u ON u.u_id = e.u_id Where e.e_id = ? and j.isactive = 1)', $EmpId);

			$resultInfo =  DB::select('SELECT * FROM employers e
										LEFT JOIN jobs j ON j.j_createdby = e.e_id
										WHERE e.e_id = ? and j.isactive = 1', $EmpId);

			$resultInfo =  DB::select('SELECT * FROM employers e
										LEFT JOIN jobs j ON j.j_createdby = e.e_id
										WHERE e.e_id = ? and j.isactive = 1 LIMIT ?,?', $EmpId2);

			return $arrayName = array('category' => $resultCat, 'skills' => $resultSki, 'info' => $resultInfo, 'paging'=>$resultPage);
		}

		public static function CreateJob($u_id, $data) {
			$info = array('j_title'		=>	$data['title'],
						'j_description'	=>	$data['description'],
						'j_requirements'=>	$data['requirements'],
						'j_salaryfrom'	=>	$data['salaryfrom'],
						'j_salaryto'	=>	$data['salaryTo'],
						'j_createdon'	=>	Carbon::now(),
						'j_employmentbasis' => $data['employmentbasis'],
						'j_createdby'	=>	$u_id,
						'isactive'		=>	'1',
						'j_experience'	=>	$data['experience']);

			$infoSkills = array();
			$infoCategory = array();

			$j_id =  DB::table('jobs')->insertGetid($info);

			if($j_id){
				foreach ($data['categories'] as $key) {
					$infoCategory[] = array('j_id' => $j_id,'jc_jobcategory' => $key['jc_jobcategory']);
				}

				foreach ($data['skills'] as $key) {
					$infoSkills[] = array('j_id' => $j_id,'js_skill' => $key['js_skills']);
				}

				$resultqueryCat = DB::table('job_categories')->insert($infoCategory);
				$resultquerySki = DB::table('job_skills')->insert($infoSkills);

				$result['success'] = true;
				$result['msg'] = 'Information successfully saved';
			}else{
				$result['success'] = false;
				$result['msg'] = 'WARNING: Unknown error occur while adding jobs.';
			}

			return json_encode($result);
		}

		public static function getJobDetails($j_id){
			$resultCat = DB::select('SELECT * FROM job_categories WHERE j_id = ?', $j_id);

			$resultSki = DB::select('SELECT * FROM job_skills WHERE j_id = ?', $j_id);

			$resultInfo = DB::select('SELECT * FROM jobs j
							INNER JOIN employers e ON e.e_id = j.j_createdby
							INNER JOIN users u ON u.u_id = e.u_id
							WHERE j.j_id=?',$j_id);

			return $arrayName = array('category' => $resultCat, 'skills' => $resultSki, 'info' => $resultInfo );
		}

		public static function getMyJobs($u_id,$u_id2) {


			$resultCat = DB::select('SELECT * FROM job_categories WHERE j_id IN
						(SELECT j_id FROM jobs j
							INNER JOIN employers e ON e.e_id = j.j_createdby
							INNER JOIN users u ON u.u_id = e.u_id Where u.u_id = ?  and j.isactive = 1)', $u_id);
			$resultSki = DB::select('SELECT * FROM job_skills WHERE j_id IN
						(SELECT j_id FROM jobs j
							INNER JOIN employers e ON e.e_id = j.j_createdby
							INNER JOIN users u ON u.u_id = e.u_id Where u.u_id = ? and j.isactive = 1)', $u_id);

			$resultInfo =  DB::select(DB::raw('SELECT * FROM jobs j
							INNER JOIN employers e ON e.e_id = j.j_createdby
							INNER JOIN users u ON u.u_id = e.u_id Where u.u_id = ? and j.isactive = 1'), $u_id);

			$resultPage =  DB::select(DB::raw('SELECT * FROM jobs j
							INNER JOIN employers e ON e.e_id = j.j_createdby
							INNER JOIN users u ON u.u_id = e.u_id Where u.u_id = ? and j.isactive = 1 LIMIT ?,?'), $u_id2);


			return $arrayName = array('category' => $resultCat, 'skills' => $resultSki, 'info' => $resultInfo, 'paging'=>$resultPage);
		}

		public static function getSearchMyJobs($u_id,$u_id2) {

			$resultCat = DB::select(DB::raw("SELECT * FROM job_categories WHERE j_id IN
						(SELECT j_id FROM jobs j
							INNER JOIN employers e ON e.e_id = j.j_createdby
							INNER JOIN users u ON u.u_id = e.u_id Where (u.u_id = '$u_id[0]' and j.j_title LIKE '$u_id[1]')  and j.isactive = 1)"));
			$resultSki = DB::select(DB::raw("SELECT * FROM job_skills WHERE j_id IN
						(SELECT j_id FROM jobs j
							INNER JOIN employers e ON e.e_id = j.j_createdby
							INNER JOIN users u ON u.u_id = e.u_id Where (u.u_id = '$u_id[0]' and j.j_title LIKE '$u_id[1]') and j.isactive = 1)"));

			$resultInfo =  DB::select(DB::raw("SELECT * FROM jobs j
							INNER JOIN employers e ON e.e_id = j.j_createdby
							INNER JOIN users u ON u.u_id = e.u_id Where (u.u_id = '$u_id[0]' and j.j_title LIKE '$u_id[1]') and j.isactive = 1"));

			$resultPage =  DB::select(DB::raw("SELECT * FROM jobs j
							INNER JOIN employers e ON e.e_id = j.j_createdby
							INNER JOIN users u ON u.u_id = e.u_id Where (u.u_id = '$u_id2[0]' and j.j_title LIKE '$u_id2[1]') and j.isactive = 1 LIMIT $u_id2[2],$u_id2[3]"));


			return $arrayName = array('category' => $resultCat, 'skills' => $resultSki, 'info' => $resultInfo, 'paging'=>$resultPage);
		}

		public static function getAllJobs($value='',$limit =''){

			if($value[3] == 'category'){
				$resultCat = DB::select('SELECT * FROM job_categories WHERE j_id IN
						(SELECT j_id FROM jobs j
							INNER JOIN employers e ON e.e_id = j.j_createdby
							INNER JOIN users u ON u.u_id = e.u_id)');

				$resultSki = DB::select('SELECT * FROM job_skills WHERE j_id IN
							(SELECT j_id FROM jobs j
								INNER JOIN employers e ON e.e_id = j.j_createdby
								INNER JOIN users u ON u.u_id = e.u_id)');

				$resultInfo = DB::select(DB::raw("SELECT * FROM jobs j
						INNER JOIN employers e ON e.e_id = j.j_createdby
						INNER JOIN users u ON u.u_id = e.u_id
						LEFT JOIN job_categories jc ON j.j_id = jc.j_id
						WHERE jc.jc_jobcategory LIKE '%$value[2]%'"));

				$resultPage = DB::select(DB::raw("SELECT * FROM `jobs` j
						INNER JOIN `employers` e ON e.e_id = j.j_createdby
						INNER JOIN `users` u ON u.u_id = e.u_id
						LEFT JOIN job_categories jc ON j.j_id = jc.j_id
						WHERE jc.jc_jobcategory LIKE '%$value[2]%'  AND j.isactive = 1 LIMIT $limit[0],$limit[1]"));
			}elseif($value[3] == 'search') {
				if($value[0] != '' && $value[1] == '' && $value[2] == ''){

					$resultCat = DB::select(DB::raw("SELECT * FROM `job_categories`
							WHERE `j_id` IN (SELECT `j_id` FROM `jobs` j
							INNER JOIN `employers` e ON e.e_id = j.j_createdby
							INNER JOIN `users` u ON u.u_id = e.u_id WHERE j.j_title LIKE '%$value[0]%' AND j.isactive = 1 )"));

					$resultSki = DB::select(DB::raw("SELECT * FROM `job_skills`
								WHERE `j_id` IN (SELECT `j_id` FROM `jobs` j
								INNER JOIN `employers` e ON e.e_id = j.j_createdby
								INNER JOIN `users` u ON u.u_id = e.u_id
								WHERE j.j_title LIKE '%$value[0]%' AND j.isactive = 1)"));

					$resultInfo = DB::select(DB::raw("SELECT * FROM `jobs` j
								INNER JOIN `employers` e ON e.e_id = j.j_createdby
								INNER JOIN `users` u ON u.u_id = e.u_id
								WHERE j.j_title LIKE '%$value[0]%' AND j.isactive = 1"));

					$resultPage = DB::select(DB::raw("SELECT * FROM `jobs` j
								INNER JOIN `employers` e ON e.e_id = j.j_createdby
								INNER JOIN `users` u ON u.u_id = e.u_id
								WHERE j.j_title LIKE '%$value[0]%' AND j.isactive = 1 LIMIT $limit[0],$limit[1]"));

				}elseif($value[0] != '' && $value[1] != '' && $value[2] == ''){

					$resultCat = DB::select(DB::raw("SELECT * FROM `job_categories`
							WHERE `j_id` IN (SELECT `j_id` FROM `jobs` j
							INNER JOIN `employers` e ON e.e_id = j.j_createdby
							INNER JOIN `users` u ON u.u_id = e.u_id WHERE j.j_title LIKE '%$value[0]%' AND u.u_address LIKE '%$value[1]%' AND j.isactive = 1)"));

					$resultSki = DB::select(DB::raw("SELECT * FROM `job_skills`
								WHERE `j_id` IN (SELECT `j_id` FROM `jobs` j
								INNER JOIN `employers` e ON e.e_id = j.j_createdby
								INNER JOIN `users` u ON u.u_id = e.u_id
								WHERE j.j_title LIKE '%$value[0]%' AND u.u_address LIKE '%$value[1]%' AND j.isactive = 1)"));

					$resultInfo = DB::select(DB::raw("SELECT * FROM `jobs` j
								INNER JOIN `employers` e ON e.e_id = j.j_createdby
								INNER JOIN `users` u ON u.u_id = e.u_id
								WHERE j.j_title LIKE '%$value[0]%' AND u.u_address LIKE '%$value[1]%' AND j.isactive = 1"));

					$resultPage = DB::select(DB::raw("SELECT * FROM `jobs` j
								INNER JOIN `employers` e ON e.e_id = j.j_createdby
								INNER JOIN `users` u ON u.u_id = e.u_id
								WHERE j.j_title LIKE '%$value[0]%' AND u.u_address LIKE '%$value[1]%' AND j.isactive = 1 LIMIT $limit[0],$limit[1]"));
				}elseif($value[0] != '' && $value[1] != '' && $value[2] != ''){

					$resultCat = DB::select('SELECT * FROM job_categories WHERE j_id IN
							(SELECT j_id FROM jobs j
								INNER JOIN employers e ON e.e_id = j.j_createdby
								INNER JOIN users u ON u.u_id = e.u_id)');

					$resultSki = DB::select('SELECT * FROM job_skills WHERE j_id IN
								(SELECT j_id FROM jobs j
									INNER JOIN employers e ON e.e_id = j.j_createdby
									INNER JOIN users u ON u.u_id = e.u_id)');

					$resultInfo = DB::select(DB::raw("SELECT * FROM jobs j
							INNER JOIN employers e ON e.e_id = j.j_createdby
							INNER JOIN users u ON u.u_id = e.u_id
							LEFT JOIN job_categories jc ON j.j_id = jc.j_id
							WHERE j.j_title LIKE '%$value[0]%' AND u.u_address LIKE '%$value[1]%' AND jc.jc_jobcategory LIKE '%$value[2]%' AND j.isactive = 1"));

					$resultPage = DB::select(DB::raw("SELECT * FROM `jobs` j
							INNER JOIN `employers` e ON e.e_id = j.j_createdby
							INNER JOIN `users` u ON u.u_id = e.u_id
							LEFT JOIN job_categories jc ON j.j_id = jc.j_id
							WHERE j.j_title LIKE '%$value[0]%' AND u.u_address LIKE '%$value[1]%' AND jc.jc_jobcategory LIKE '%$value[2]%'  AND j.isactive = 1 LIMIT $limit[0],$limit[1]"));
				}elseif($value[0] != '' && $value[1] == '' && $value[2] != ''){

					$resultCat = DB::select('SELECT * FROM job_categories WHERE j_id IN
							(SELECT j_id FROM jobs j
								INNER JOIN employers e ON e.e_id = j.j_createdby
								INNER JOIN users u ON u.u_id = e.u_id)');

					$resultSki = DB::select('SELECT * FROM job_skills WHERE j_id IN
								(SELECT j_id FROM jobs j
									INNER JOIN employers e ON e.e_id = j.j_createdby
									INNER JOIN users u ON u.u_id = e.u_id)');

					$resultInfo = DB::select(DB::raw("SELECT * FROM jobs j
							INNER JOIN employers e ON e.e_id = j.j_createdby
							INNER JOIN users u ON u.u_id = e.u_id
							LEFT JOIN job_categories jc ON j.j_id = jc.j_id
							WHERE j.j_title LIKE '%$value[0]%' AND jc.jc_jobcategory LIKE '%$value[2]%' AND j.isactive = 1"));

					$resultPage = DB::select(DB::raw("SELECT * FROM `jobs` j
							INNER JOIN `employers` e ON e.e_id = j.j_createdby
							INNER JOIN `users` u ON u.u_id = e.u_id
							LEFT JOIN job_categories jc ON j.j_id = jc.j_id
							WHERE j.j_title LIKE '%$value[0]%' AND jc.jc_jobcategory LIKE '%$value[2]%'  AND j.isactive = 1 LIMIT $limit[0],$limit[1]"));
				}elseif($value[0] == '' && $value[1] != '' && $value[2] != ''){

					$resultCat = DB::select('SELECT * FROM job_categories WHERE j_id IN
							(SELECT j_id FROM jobs j
								INNER JOIN employers e ON e.e_id = j.j_createdby
								INNER JOIN users u ON u.u_id = e.u_id)');

					$resultSki = DB::select('SELECT * FROM job_skills WHERE j_id IN
								(SELECT j_id FROM jobs j
									INNER JOIN employers e ON e.e_id = j.j_createdby
									INNER JOIN users u ON u.u_id = e.u_id)');

					$resultInfo = DB::select(DB::raw("SELECT * FROM jobs j
							INNER JOIN employers e ON e.e_id = j.j_createdby
							INNER JOIN users u ON u.u_id = e.u_id
							LEFT JOIN job_categories jc ON j.j_id = jc.j_id
							WHERE u.u_address LIKE '%$value[1]%' AND jc.jc_jobcategory LIKE '%$value[2]%' AND j.isactive = 1"));

					$resultPage = DB::select(DB::raw("SELECT * FROM `jobs` j
							INNER JOIN `employers` e ON e.e_id = j.j_createdby
							INNER JOIN `users` u ON u.u_id = e.u_id
							LEFT JOIN job_categories jc ON j.j_id = jc.j_id
							WHERE u.u_address LIKE '%$value[1]%' AND jc.jc_jobcategory LIKE '%$value[2]%'  AND j.isactive = 1 LIMIT $limit[0],$limit[1]"));
				}elseif($value[0] == '' && $value[1] == '' && $value[2] != ''){

					$resultCat = DB::select('SELECT * FROM job_categories WHERE j_id IN
							(SELECT j_id FROM jobs j
								INNER JOIN employers e ON e.e_id = j.j_createdby
								INNER JOIN users u ON u.u_id = e.u_id)');

					$resultSki = DB::select('SELECT * FROM job_skills WHERE j_id IN
								(SELECT j_id FROM jobs j
									INNER JOIN employers e ON e.e_id = j.j_createdby
									INNER JOIN users u ON u.u_id = e.u_id)');

					$resultInfo = DB::select(DB::raw("SELECT * FROM jobs j
							INNER JOIN employers e ON e.e_id = j.j_createdby
							INNER JOIN users u ON u.u_id = e.u_id
							LEFT JOIN job_categories jc ON j.j_id = jc.j_id
							WHERE jc.jc_jobcategory LIKE '%$value[2]%' AND j.isactive = 1"));

					$resultPage = DB::select(DB::raw("SELECT * FROM `jobs` j
							INNER JOIN `employers` e ON e.e_id = j.j_createdby
							INNER JOIN `users` u ON u.u_id = e.u_id
							LEFT JOIN job_categories jc ON j.j_id = jc.j_id
							WHERE jc.jc_jobcategory LIKE '%$value[2]%'  AND j.isactive = 1 LIMIT $limit[0],$limit[1]"));
				}elseif($value[0] == '' && $value[1] == '' && $value[2] == ''){
					$resultCat = DB::select('SELECT * FROM job_categories WHERE j_id IN
								(SELECT j_id FROM jobs j
									INNER JOIN employers e ON e.e_id = j.j_createdby
									INNER JOIN users u ON u.u_id = e.u_id)');

					$resultSki = DB::select('SELECT * FROM job_skills WHERE j_id IN
								(SELECT j_id FROM jobs j
									INNER JOIN employers e ON e.e_id = j.j_createdby
									INNER JOIN users u ON u.u_id = e.u_id)');

					$resultInfo = DB::select('SELECT * FROM jobs j
								INNER JOIN employers e ON e.e_id = j.j_createdby
								INNER JOIN users u ON u.u_id = e.u_id');

					if (isset($limit) && !empty($limit)){
						$resultPage = DB::select('SELECT * FROM jobs j
								INNER JOIN employers e ON e.e_id = j.j_createdby
								INNER JOIN users u ON u.u_id = e.u_id LIMIT ?,?',$limit);
					}
				}
			}else{

				$resultCat = DB::select('SELECT * FROM job_categories WHERE j_id IN
							(SELECT j_id FROM jobs j
								INNER JOIN employers e ON e.e_id = j.j_createdby
								INNER JOIN users u ON u.u_id = e.u_id)');

				$resultSki = DB::select('SELECT * FROM job_skills WHERE j_id IN
							(SELECT j_id FROM jobs j
								INNER JOIN employers e ON e.e_id = j.j_createdby
								INNER JOIN users u ON u.u_id = e.u_id)');

				$resultInfo = DB::select('SELECT * FROM jobs j
							INNER JOIN employers e ON e.e_id = j.j_createdby
							INNER JOIN users u ON u.u_id = e.u_id');

				if (isset($limit) && !empty($limit)){
					$resultPage = DB::select('SELECT * FROM jobs j
							INNER JOIN employers e ON e.e_id = j.j_createdby
							INNER JOIN users u ON u.u_id = e.u_id LIMIT ?,?',$limit);
				}

			}
			return $arrayName = array('category' => $resultCat, 'skills' => $resultSki, 'info' => $resultInfo,'paging'=>$resultPage);
		}

		public static function getAllJobsByCategoryAndPlace($value='',$limit =''){

				if($value[1] == 'category'){

					$resultCat = DB::select('SELECT * FROM job_categories WHERE j_id IN
							(SELECT j_id FROM jobs j
								INNER JOIN employers e ON e.e_id = j.j_createdby
								INNER JOIN users u ON u.u_id = e.u_id)');

					$resultSki = DB::select('SELECT * FROM job_skills WHERE j_id IN
								(SELECT j_id FROM jobs j
									INNER JOIN employers e ON e.e_id = j.j_createdby
									INNER JOIN users u ON u.u_id = e.u_id)');

					$resultInfo = DB::select(DB::raw("SELECT * FROM jobs j
							INNER JOIN employers e ON e.e_id = j.j_createdby
							INNER JOIN users u ON u.u_id = e.u_id
							LEFT JOIN job_categories jc ON j.j_id = jc.j_id
							WHERE jc.jc_jobcategory LIKE '%$value[0]%'"));

					$resultPage = DB::select(DB::raw("SELECT * FROM `jobs` j
							INNER JOIN `employers` e ON e.e_id = j.j_createdby
							INNER JOIN `users` u ON u.u_id = e.u_id
							LEFT JOIN job_categories jc ON j.j_id = jc.j_id
							WHERE jc.jc_jobcategory LIKE '%$value[0]%'  AND j.isactive = 1 LIMIT $limit[0],$limit[1]"));

				}elseif($value[1] == 'place'){
					if($value[2] == ''){
						$resultCat = DB::select('SELECT * FROM job_categories WHERE j_id IN
								(SELECT j_id FROM jobs j
									INNER JOIN employers e ON e.e_id = j.j_createdby
									INNER JOIN users u ON u.u_id = e.u_id)');

						$resultSki = DB::select('SELECT * FROM job_skills WHERE j_id IN
									(SELECT j_id FROM jobs j
										INNER JOIN employers e ON e.e_id = j.j_createdby
										INNER JOIN users u ON u.u_id = e.u_id)');

						$resultInfo = DB::select(DB::raw("SELECT * FROM jobs j
								INNER JOIN employers e ON e.e_id = j.j_createdby
								INNER JOIN users u ON u.u_id = e.u_id
								WHERE u.u_address LIKE '%$value[3]%'  AND j.isactive = 1 "));

						$resultPage = DB::select(DB::raw("SELECT * FROM `jobs` j
								INNER JOIN `employers` e ON e.e_id = j.j_createdby
								INNER JOIN `users` u ON u.u_id = e.u_id
								WHERE u.u_address LIKE '%$value[3]%'  AND j.isactive = 1 LIMIT $limit[0],$limit[1]"));
					}else{
						$resultCat = DB::select('SELECT * FROM job_categories WHERE j_id IN
								(SELECT j_id FROM jobs j
									INNER JOIN employers e ON e.e_id = j.j_createdby
									INNER JOIN users u ON u.u_id = e.u_id)');

						$resultSki = DB::select('SELECT * FROM job_skills WHERE j_id IN
									(SELECT j_id FROM jobs j
										INNER JOIN employers e ON e.e_id = j.j_createdby
										INNER JOIN users u ON u.u_id = e.u_id)');

						$resultInfo = DB::select(DB::raw("SELECT * FROM jobs j
								INNER JOIN employers e ON e.e_id = j.j_createdby
								INNER JOIN users u ON u.u_id = e.u_id
								WHERE u.u_address LIKE '%$value[3]%' AND j.j_title LIKE '%$value[2]%'   AND j.isactive = 1 "));

						$resultPage = DB::select(DB::raw("SELECT * FROM `jobs` j
								INNER JOIN `employers` e ON e.e_id = j.j_createdby
								INNER JOIN `users` u ON u.u_id = e.u_id
								WHERE u.u_address LIKE '%$value[3]%' AND j.j_title LIKE '%$value[2]%'  AND j.isactive = 1 LIMIT $limit[0],$limit[1]"));
					}

				}elseif($value[1] == 'search'){

					if($value[3]==''){
						$resultCat = DB::select('SELECT * FROM job_categories WHERE j_id IN
								(SELECT j_id FROM jobs j
									INNER JOIN employers e ON e.e_id = j.j_createdby
									INNER JOIN users u ON u.u_id = e.u_id)');

						$resultSki = DB::select('SELECT * FROM job_skills WHERE j_id IN
									(SELECT j_id FROM jobs j
										INNER JOIN employers e ON e.e_id = j.j_createdby
										INNER JOIN users u ON u.u_id = e.u_id)');

						$resultInfo = DB::select(DB::raw("SELECT * FROM jobs j
								INNER JOIN employers e ON e.e_id = j.j_createdby
								INNER JOIN users u ON u.u_id = e.u_id
								WHERE j.j_title LIKE '%$value[2]%'  AND j.isactive = 1 "));

						$resultPage = DB::select(DB::raw("SELECT * FROM `jobs` j
								INNER JOIN `employers` e ON e.e_id = j.j_createdby
								INNER JOIN `users` u ON u.u_id = e.u_id
								WHERE j.j_title LIKE '%$value[2]%'  AND j.isactive = 1 LIMIT $limit[0],$limit[1]"));
					}else{
						$resultCat = DB::select('SELECT * FROM job_categories WHERE j_id IN
								(SELECT j_id FROM jobs j
									INNER JOIN employers e ON e.e_id = j.j_createdby
									INNER JOIN users u ON u.u_id = e.u_id)');

						$resultSki = DB::select('SELECT * FROM job_skills WHERE j_id IN
									(SELECT j_id FROM jobs j
										INNER JOIN employers e ON e.e_id = j.j_createdby
										INNER JOIN users u ON u.u_id = e.u_id)');

						$resultInfo = DB::select(DB::raw("SELECT * FROM jobs j
								INNER JOIN employers e ON e.e_id = j.j_createdby
								INNER JOIN users u ON u.u_id = e.u_id
								WHERE j.j_title LIKE '%$value[2]%' AND u.u_address LIKE '%$value[3]%'  AND j.isactive = 1 "));

						$resultPage = DB::select(DB::raw("SELECT * FROM `jobs` j
								INNER JOIN `employers` e ON e.e_id = j.j_createdby
								INNER JOIN `users` u ON u.u_id = e.u_id
								WHERE j.j_title LIKE '%$value[2]%' AND u.u_address LIKE '%$value[3]%'  AND j.isactive = 1 LIMIT $limit[0],$limit[1]"));
					}

				}else{
					$resultPage = [];
				}

			return $arrayName = array('category' => $resultCat, 'skills' => $resultSki, 'info' => $resultInfo,'paging'=>$resultPage);
		}

		public static function SearchEmpjob($value='',$limit =''){

			if($value[2] == 'show'){
				$resultCat = DB::select(DB::raw("SELECT * FROM `job_categories`
					WHERE `j_id` IN (SELECT `j_id` FROM `jobs` j
					INNER JOIN `employers` e ON e.e_id = j.j_createdby
					INNER JOIN `users` u ON u.u_id = e.u_id WHERE j.isactive = '0')"));

				$resultSki = DB::select(DB::raw("SELECT * FROM `job_skills`
							WHERE `j_id` IN (SELECT `j_id` FROM `jobs` j
							INNER JOIN `employers` e ON e.e_id = j.j_createdby
							INNER JOIN `users` u ON u.u_id = e.u_id
							WHERE j.isactive = '0')"));

				$resultInfo = DB::select(DB::raw("SELECT * FROM `jobs` j
							INNER JOIN `employers` e ON e.e_id = j.j_createdby
							INNER JOIN `users` u ON u.u_id = e.u_id
							WHERE  j.isactive = '0' "));

				$resultPage = DB::select(DB::raw("SELECT * FROM `jobs` j
							INNER JOIN `employers` e ON e.e_id = j.j_createdby
							INNER JOIN `users` u ON u.u_id = e.u_id
							WHERE j.isactive = '0' LIMIT $limit[0],$limit[1]"));
			}elseif($value[2] == 'notshow'){
				$resultCat = DB::select(DB::raw("SELECT * FROM `job_categories`
					WHERE `j_id` IN (SELECT `j_id` FROM `jobs` j
					INNER JOIN `employers` e ON e.e_id = j.j_createdby
					INNER JOIN `users` u ON u.u_id = e.u_id WHERE j.isactive = '1')"));

				$resultSki = DB::select(DB::raw("SELECT * FROM `job_skills`
							WHERE `j_id` IN (SELECT `j_id` FROM `jobs` j
							INNER JOIN `employers` e ON e.e_id = j.j_createdby
							INNER JOIN `users` u ON u.u_id = e.u_id
							WHERE j.isactive = '1')"));

				$resultInfo = DB::select(DB::raw("SELECT * FROM `jobs` j
							INNER JOIN `employers` e ON e.e_id = j.j_createdby
							INNER JOIN `users` u ON u.u_id = e.u_id
							WHERE  j.isactive = '1' "));

				$resultPage = DB::select(DB::raw("SELECT * FROM `jobs` j
							INNER JOIN `employers` e ON e.e_id = j.j_createdby
							INNER JOIN `users` u ON u.u_id = e.u_id
							WHERE j.isactive = '1' LIMIT $limit[0],$limit[1]"));
			}else{
				$resultCat = DB::select(DB::raw("SELECT * FROM `job_categories`
					WHERE `j_id` IN (SELECT `j_id` FROM `jobs` j
					INNER JOIN `employers` e ON e.e_id = j.j_createdby
					INNER JOIN `users` u ON u.u_id = e.u_id WHERE j.j_title LIKE '$value[0]'  AND j.isactive = '$value[1]')"));

				$resultSki = DB::select(DB::raw("SELECT * FROM `job_skills`
							WHERE `j_id` IN (SELECT `j_id` FROM `jobs` j
							INNER JOIN `employers` e ON e.e_id = j.j_createdby
							INNER JOIN `users` u ON u.u_id = e.u_id
							WHERE j.j_title LIKE '$value[0]'  AND j.isactive = '$value[1]')"));

				$resultInfo = DB::select(DB::raw("SELECT * FROM `jobs` j
							INNER JOIN `employers` e ON e.e_id = j.j_createdby
							INNER JOIN `users` u ON u.u_id = e.u_id
							WHERE j.j_title LIKE '$value[0]'  AND j.isactive = '$value[1]' "));

				$resultPage = DB::select(DB::raw("SELECT * FROM `jobs` j
							INNER JOIN `employers` e ON e.e_id = j.j_createdby
							INNER JOIN `users` u ON u.u_id = e.u_id
							WHERE j.j_title LIKE '$value[0]'  AND j.isactive = '$value[1]' LIMIT $limit[0],$limit[1]"));
			}

			return $arrayName = array('category' => $resultCat, 'skills' => $resultSki, 'info' => $resultInfo,'paging'=>$resultPage);

		}

		public static function getJobByCategory($value){

			if($value[1] == ''){
				$resultCat = DB::select('SELECT * FROM job_categories WHERE j_id IN
						(SELECT j_id FROM jobs j
							INNER JOIN employers e ON e.e_id = j.j_createdby
							INNER JOIN users u ON u.u_id = e.u_id)');

				$resultSki = DB::select('SELECT * FROM job_skills WHERE j_id IN
							(SELECT j_id FROM jobs j
								INNER JOIN employers e ON e.e_id = j.j_createdby
								INNER JOIN users u ON u.u_id = e.u_id)');

				$resultInfo = DB::select(DB::raw("SELECT * FROM jobs j
						INNER JOIN employers e ON e.e_id = j.j_createdby
						INNER JOIN users u ON u.u_id = e.u_id
						LEFT JOIN job_categories jc ON j.j_id = jc.j_id
						WHERE jc.jc_jobcategory LIKE '$value[0]'"));
			}else{
				$resultCat = DB::select('SELECT * FROM job_categories WHERE j_id IN
						(SELECT j_id FROM jobs j
							INNER JOIN employers e ON e.e_id = j.j_createdby
							INNER JOIN users u ON u.u_id = e.u_id)');

				$resultSki = DB::select('SELECT * FROM job_skills WHERE j_id IN
							(SELECT j_id FROM jobs j
								INNER JOIN employers e ON e.e_id = j.j_createdby
								INNER JOIN users u ON u.u_id = e.u_id)');

				$resultInfo = DB::select(DB::raw("SELECT * FROM jobs j
						INNER JOIN employers e ON e.e_id = j.j_createdby
						INNER JOIN users u ON u.u_id = e.u_id
						LEFT JOIN job_categories jc ON j.j_id = jc.j_id
						WHERE jc.jc_jobcategory LIKE '$value[0]' AND u.u_address='$value[1]' "));
			}

			return $arrayName = array('category' => $resultCat,'skills' => $resultSki, 'info' => $resultInfo );
		}

		public static function getJobByPlaces($value){

			$resultCat = DB::select('SELECT * FROM job_categories WHERE j_id IN
						(SELECT j_id FROM jobs j
							INNER JOIN employers e ON e.e_id = j.j_createdby
							INNER JOIN users u ON u.u_id = e.u_id)');

			$resultSki = DB::select('SELECT * FROM job_skills WHERE j_id IN
						(SELECT j_id FROM jobs j
							INNER JOIN employers e ON e.e_id = j.j_createdby
							INNER JOIN users u ON u.u_id = e.u_id)');

			$resultInfo = DB::select('SELECT * FROM jobs j
							INNER JOIN employers e ON e.e_id = j.j_createdby
							INNER JOIN users u ON u.u_id = e.u_id
							WHERE u.u_address LIKE ?',$value);

			return $arrayName = array('category' => $resultCat,'skills' => $resultSki, 'info' => $resultInfo );
		}

		public static function DeleteJob($id){
			$results = DB::update('UPDATE jobs SET isactive = 0 Where j_id = ?', $id);

			if($results){
				$result['success'] = true;
				$result['msg'] = 'Information successfully Deleted';
			}else{
				$result['success'] = false;
				$result['msg'] = 'Transaction failed';
			}
			return $result;
		}

		public static function getJobSkills($j_id){
			return $results = DB::select('SELECT * FROM job_skills WHERE j_id=?;',$j_id);
		}

		public static function getJobCategories($j_id){
			return $results = DB::select('SELECT * FROM job_categories WHERE j_id=?;',$j_id);
		}

		public static function updateJob($u_id,$data){
			$info = array(
				'j_title'			=>	$data['title'],
				'j_description' 	=>	$data['description'],
				'j_requirements'	=>	$data['requirements'],
				'j_salaryfrom'		=>	$data['salaryfrom'],
				'j_salaryto'		=>	$data['salaryTo'],
				'j_employmentbasis'	=>	$data['employmentbasis'],
				'j_createdby'		=>	$u_id,
				'isactive'			=>	1,
				'j_experience'		=>	$data['experience']);

			// DB::table('job_categories')->where('j_id', $data['j_id'])->delete();
			// DB::table('job_skills')->where('j_id', $data['j_id'])->delete();

			// $callback = DB::update('UPDATE jobs set j_title = ?,
			// 				j_description = ?, j_requirements = ?, j_salaryfrom = ?,
			// 				j_salaryto = ?, j_employmentbasis = ?,
			// 				j_createdby = ?, isactive = ? , j_experience = ? where j_id = ?', $info);
			$callback = DB::table('jobs')
				            ->where('j_id', $data['j_id'])
				            ->update($info);

			if($callback){
				foreach ($data['categories'] as $key) {
					$infoCategory[] = array('j_id' => $data['j_id'],'jc_jobcategory' => $key['jc_jobcategory']);
				}

				foreach ($data['skills'] as $key) {
					$infoSkills[] = array('j_id' => $data['j_id'],'js_skill' => $key['js_skills']);
				}

				$resultqueryCat = DB::table('job_categories')->insert($infoCategory);
				$resultquerySki = DB::table('job_skills')->insert($infoSkills);

				$result['success'] = true;
				$result['msg'] = 'Information successfully updated';
			}else{
				$result['success'] = false;
				$result['msg'] = 'WARNING: Unknown error occur while updating job information!';
			}

			return json_encode($result);
		}
	}
