<?php
require 'vendor/autoload.php';
use GuzzleHttp\Client;

class SocialMediaController extends BaseController {

	public function loginWithFacebook() {

	    // get data from input
	    $code = Input::get( 'code' );

	    $fb = OAuth::consumer('Facebook');

	    if ( !empty( $code ) ) {

	        $token = $fb->requestAccessToken($code);
	        $result = json_decode( $fb->request( '/me' ), true );

	        $data['users'] = array(
	        	'js_lastname'=>$result['last_name'],
	        	'js_firstname'=>$result['first_name'],
	        	'u_email'=>$result['email'],
	        	'u_accounttype'=>'facebook',
	        	'u_password'=>'');

	        $return = User::signUpUser($data['users']);
	        $value = json_decode($return);

	        if ($value->success == 'true'){

	        	$userdata = array(
					'u_email' 	=> $result['email'],
					'password' => '',
					'isactive' => '1',
					'u_accounttype'=>'facebook'
				);

				if (Auth::attempt($userdata)) {
					return Redirect::to('/profile');
				} else {
					return Redirect::to('/login')
						->with('flash_error', 'Your username/password combination was incorrect.');
				}

	        }else{
				return Redirect::to('/join');
	        }

	    }
	    else {
	        $url = $fb->getAuthorizationUri();
	         return Redirect::to( (string)$url );
	    }
	}


	public function loginWithGoogle() {

	    $code = Input::get( 'code' );

	    $googleService = OAuth::consumer('Google');

	    if (!empty($code)){

	        $token = $googleService->requestAccessToken( $code );
	        $result = json_decode( $googleService->request( 'https://www.googleapis.com/oauth2/v1/userinfo' ), true );

	        // echo '<pre>';
	        // print_r($result);die();

	        $data['users'] = array(
	        	'js_lastname'=>$result['family_name'],
	        	'js_firstname'=>$result['given_name'],
	        	'u_email'=>$result['email'],
	        	'u_accounttype'=>'google',
	        	'u_password'=>'');

	        $return = User::signUpUser($data['users']);
	       	$value = json_decode($return);

	        if ($value->success == 'true'){

	        	$userdata = array(
					'u_email' 	=> $result['email'],
					'password' => '',
					'isactive' => '1',
					'u_accounttype'=>'google'
				);

				if (Auth::attempt($userdata)) {
					return Redirect::to('/profile');
				} else {
					return Redirect::to('/login')
						->with('flash_error', 'Your username/password combination was incorrect.');
				}

	        }else{
				return Redirect::to('/join');
	        }
	    }
	    else {
	        $url = $googleService->getAuthorizationUri();
	        return Redirect::to( (string)$url );
	    }
	}


	public function loginWithLinkedin() {

        $code = Input::get( 'code' );

        $linkedinService = OAuth::consumer('Linkedin');

        if ( !empty( $code ) ) {

            $token = $linkedinService->requestAccessToken($code);

            $result = json_decode($linkedinService->request('/people/~:(id,first-name,last-name,headline,location,public-profile-url,email-address)?format=json'), true);

            $data['users'] = array(
	        	'js_lastname'=>$result['lastName'],
	        	'js_firstname'=>$result['firstName'],
	        	'u_accounttype'=>'linkedin',
	        	'u_email'=>$result['emailAddress'],
	        	'u_password'=>'');

	        $return = User::signUpUser($data['users']);
	       	$value = json_decode($return);

	        if ($value->success == 'true'){

	        	$userdata = array(
					'u_email' 	=> $result['emailAddress'],
					'password' => '',
					'isactive' => '1',
					'u_accounttype'=>'linkedin'
				);

				if (Auth::attempt($userdata)) {
					return Redirect::to('/profile');
				} else {
					return Redirect::to('/login')
						->with('flash_error', 'Your username/password combination was incorrect.');
				}

	        }else{
				return Redirect::to('/join');
	        }
        }// if not ask for permission first
        else {
            // get linkedinService authorization
            $url = $linkedinService->getAuthorizationUri(array('state'=>'DCEEFWF45453sdffef424'));

            // return to linkedin login url
            return Redirect::to( (string)$url );
        }


    }

}
