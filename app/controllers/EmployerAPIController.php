<?php

class EmployerAPIController extends BaseController {


	public function employerlogin(){
		$javascript = array('js/mindanaojobs.js');
		$css = array('css/style.css','css/flatui.css','css/parsley.css','css/animation.css');

        $data = [
			'page_js' =>$javascript,
			'page_css' =>$css
		];

		return View::make('index')
			->nest('header', 'include.header-login')
			->nest('main', 'page.employer-login')
			->nest('footer', 'include.footer')->with($data);
	}


	public function showRecentJobList(){
		$javascript = array('js/mindanaojobs.js');
		$css = array('css/style.css','css/flatui.css','css/parsley.css','css/animation.css');

        $data = [
			'page_js' =>$javascript,
			'page_css' =>$css
		];

		return View::make('index')
			->nest('header', 'include.header-login')
			->nest('main', 'page.joblist')
			->nest('footer', 'include.footer')->with($data);
	}

	public function showJobPost(){
		$javascript = array('js/mindanaojobs.js');
		$css = array('css/style.css','css/flatui.css','css/parsley.css','css/animation.css');

        $data = [
			'page_js' =>$javascript,
			'page_css' =>$css
		];

		return View::make('index')
			->nest('header', 'include.header-login')
			->nest('main', 'page.job-post')
			->nest('footer', 'include.footer')->with($data);
	}


}
