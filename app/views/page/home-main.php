<div class="main-container no-bottom-space">
    <?php if(isset($error) && !empty($error)){?>
    <div class="alert alert-danger alert-dismissible fade in" role="alert" style="z-index:99;position:fixed;width:100%;text-align: center;">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">Ã—</span><span class="sr-only">Close</span></button>
        <h4><?php echo $error['title'];?></h4>
        <p><?php echo $error['message'];?></p>
    </div>
    <?php } ?>
    <div class="section main-image">
        <div class="carousel fade-carousel slide" data-ride="carousel" data-interval="2000" id="bs-carousel">
            <div class="overlay"></div>
            <ol class="carousel-indicators">
                <li data-target="#bs-carousel" data-slide-to="0" class="active"></li>
                <li data-target="#bs-carousel" data-slide-to="1"></li>
                <li data-target="#bs-carousel" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="item slides active">
                  <div class="slide-1"></div>
                  <div class="hero">
                    <hgroup>
                        <h1>We are creative</h1>
                        <h3>Get start your next awesome project</h3>
                    </hgroup>
                    <button class="btn btn-hero btn-lg" role="button">See all features</button>
                  </div>
                </div>
                <div class="item slides">
                  <div class="slide-2"></div>
                  <div class="hero">
                    <hgroup>
                        <h1>We are smart</h1>
                        <h3>Get start your next awesome project</h3>
                    </hgroup>
                    <button class="btn btn-hero btn-lg" role="button">See all features</button>
                  </div>
                </div>
                <div class="item slides">
                  <div class="slide-3"></div>
                  <div class="hero">
                    <hgroup>
                        <h1>We are amazing</h1>
                        <h3>Get start your next awesome project</h3>
                    </hgroup>
                    <button class="btn btn-hero btn-lg" role="button">See all features</button>
                  </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ./section -->

    <div class="section how-it-work-section bg-grey section-border-bottom">
        <div class="container">
            <div class="section-inner text-center">
                <div class="line-38">
                    <div class="font-30 font-lato font-ultra-bold">Find your desired jobs here at Mindanao Jobs</div>
                    <div class="font-30 font-lato font-italic">dream jobs for professionals like you...</div>
                </div>
                <div class="col-md-10 col-md-offset-1">
                    <div class="row">
                        <div class="col-sm-4">

                        </div>
                        <!-- ./col -->
                        <div class="col-sm-4">

                        </div>
                        <!-- ./col -->
                        <div class="col-sm-4">

                        </div>
                        <!-- ./col -->
                    </div>
                    <!-- ./row -->
                </div>
            </div>
            <!-- ./section-inner -->
        </div>
    </div>
    <!-- section -->

    <div class="section bg-dark-grey post-job-section section-border-bottom">
        <div class="container">
            <div class="font-24 text-center m-bottom-40">
                <div class="title font-lato line-1">Post a Job and
                    <span class="font-ultra-bold">Start Receiving Applicants Today</span>
                </div>
            </div>

            <div class="text-center m-top-20 m-bottom-20">
                <div class="font-18 inline-block vertical-middle m-right-40 line-24 block-xs no-margin-xs">
                    <div class="font-semi-bold">See how much your business can save!</div>
                    It's free to get quotes with no obligations.</div>
                <a href="/jobpost" class="btn btn-primary btn-lg vertical-middle m-top-20-xs">Post Your Job<i class="fa fa-angle-double-right font-16 m-left-10"></i></a>
            </div>
        </div>
    </div>

    <div class="section bg-light-grey last-section">
        <div class="container padding-lg line-32">
            <div class="section-inner">
                <div class="section-image">
                    <img src="../img/home/logo.png">
                </div>
                <div class="section-content">
                    <p class="font-16">
                        <span class="font-semi-bold">MindanaoJobs.com</span> allows you to easily outsource work and build a quality team of the world's best freelancers. Post a job to receive quotes, compare profiles, and hire great freelancers for your job. MindanaoJobs.com has a wide variety of freelancers including: web developers, data analysts, social media specialists , web designers, graphic designers, researchers, app developers, software programmers and more.</p>
                </div>
            </div>
        </div>
    </div>
    <!-- ./section -->
</div>
