<div class="main-container">
    <?php if(isset($User) && !empty($User)){
        if ($User->u_isverified != '1'){
    ?>
    <div class="section bg-danger paddingTB-10 alert-top section-border-bottom">
        <div class="container">
          <div class="row">
            <div class="col-sm-7">
                <i class="pull-left m-right-15 m-top-5 no-margin-xs fa fa-3x fa-exclamation-triangle text-danger"></i>
                <div class="font-21 font-semi-bold title-text"> Your account is currently disabled. </div>
                <div class="font-lato information-text">
                    <span class="font-14 font-semi-bold">Check your email to verify your account. <a href="javascript:resendConfirmation()">Click here</a></span> to resend confirmation
                </div>
            </div>
          </div>
        </div>
    </div>
    <?php
            }
        }
    ?>
    <form class="form-horizontal search-form" action="/joblist" method="get">
    <div class="section paddingT-40">
        <div class="container">
            <div class="font-lato font-30">
                <span class="font-ultra-bold">Find a Job</span>
            </div>
            <div class="font-16">We found
                <span class="pc-cnt"><?php echo count($Jobs); ?> </span>jobs that match your skills</div>
            <div class="row paddingTB-40">
                <div class="col-md-12">
                    <div class="col-md-4" style="padding-right: 0px!important;">
                        <input type="text" class="form-control" placeholder="Search here..." id="search_name" name="search_name" value="<?php if(isset($search_name)){echo $search_name;} ?>">

                        <input type="hidden" id="type" name="type" value="search">
                    </div>
                    <div class="col-md-3" style="padding-right: 0px!important;">
                        <select name="places" class="form-control cboFilter">
                            <option value="">- All Places -</option>
                            <?php
                                if (isset($joblocation['location'])){
                                    foreach ($joblocation['location'] as $location =>$value) {
                                        if(isset($places) && ($places == $value['town_cities_name'])){
                                            echo '<option value="'. $value['town_cities_name'].'" selected = "selected">'. $value['town_cities_name'] .'</option>';
                                        }else{
                                            echo '<option value="'. $value['town_cities_name'].'">'. $value['town_cities_name'] .'</option>';
                                        }
                                    }
                                }
                            ?>
                        </select>
                    </div>
                    <div class="col-md-3">
                    <select name="categories" class="form-control cboFilter">
                            <option value="">- All Categories -</option>
                        <?php

                            if (isset($jobcategories['jobcategories'])){
                                    foreach ($jobcategories['jobcategories'] as $cat =>$value) {
                                        if (isset($value['subcategory']) && !empty($value['subcategory'])){
                                            foreach ($value['subcategory'] as $cat) {
                                                if(isset($categories) && ($categories == $cat['description'])){
                                                    echo '<option value="'. $cat['description'].'" selected = "selected">'. $cat['description'] .'</option>';
                                                }
                                                    echo '<option value="'. $cat['description'].'">'. $cat['description'] .'</option>';
                                            }
                                        }
                                    }
                                }

                         ?>
                     </select>
                    </div>
                    <div class="col-md-2" style="padding-right: 0px!important;">
                        <span class="input-group-btn">
                            <button class="btn btn-info" type="submit" ><i class="fa fa-search"></i> search
                            </button>
                        </span>
                    </div>
                </div>
                <!-- ./col -->
            </div>
            <!-- ./row -->
        </div>
    </div>
    <div class="section ">
        <div class=" paddingL-40 paddingR-20">
            <div class="bg-light row find-project-grid">
                    <div class="col-md-9">
                        <div class="project-list m-top-md">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                      <th>Job Title</th>
                                      <th>Company</th>
                                      <th>Location</th>
                                      <th>Yrs Exp</th>
                                      <th>Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php if(isset($Jobs) && !empty($Jobs)){
                                        foreach ($Jobs as $key) {
                                            foreach ($key['info'] as $key1 ) {?>
                                        <tr>
                                            <td>
                                                <a href="/jobdetail?id=<?php echo $key1->j_id; ?>" class="font-18 font-semi-bold"><?php echo  $key1->j_title; ?></a>
                                                <p></p>
                                               <?php
                                                    if(isset($key['categories']) && !empty($key['categories'])){
                                                        foreach ($key['categories'] as $keys ) {
                                                            echo '<span class="text-primary font-10 m-bottom-5">'.$keys['category']->jc_jobcategory.' > </span>';
                                                        }
                                                    }
                                                ?>
                                            </td>
                                            <td>
                                                <a href="/CompanyProfile?u_id=<?php echo $key1->u_id; ?>&e_id=<?php echo $key1->e_id; ?>"><?php echo  $key1->e_companyname; ?></a>
                                            </td>
                                            <td>
                                                <?php echo  $key1->u_address; ?>
                                            </td>
                                            <td>
                                                <?php echo  $key1->j_experience; ?>
                                            </td>
                                            <td>
                                                <?php echo date_format(date_create($key1->j_createdon), 'M d, Y'); ?>
                                            </td>
                                        </tr>
                                    <?php }
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                            <div class="panel-group text-center">
                                <div class="SearchPagination">
                                    <?php print $Jobs->appends(array('search_name' =>$search_name,'type'=> $type,'places'=> $places,'categories'=>$categories))->links(); ?>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-3 m-bottom-40 hidden-xs hidden-sm filter-search">
                        <div class="bg-grey paddingB-40 paddingT-30 paddingLR-15">
                            <div class="font-16 font-semi-bold">Filter Search Results</div>
                            <div class="font-16 font-semi-bold m-top-20">Categories</div>
                            <?php
                                if (isset($jobcategories['jobcategories'])){
                                    foreach ($jobcategories['jobcategories'] as $cat =>$value) {
                                        echo '<div class="m-top-10 font-12 font-semi-bold">'.
                                                $value['description'].
                                                '</div>';

                                        if (isset($value['subcategory']) && !empty($value['subcategory'])){
                                            foreach ($value['subcategory'] as $cat) {
                                                echo '<div class="m-top-10 font-12">
                                                        <a href="joblist?categories='.$cat['description'].'&type=category" class="clscategories" data-category="'.$cat['description'].'">'.
                                                        $cat['description'].
                                                        '</a>
                                                    </div>';
                                            }
                                        }
                                    }
                                }
                            ?>
                            <hr class="line-dotted">
                            </hr>

                        </div>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>
