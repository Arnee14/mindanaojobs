$(function() {

    $('#myTab a').click(function(e) {
        e.preventDefault()
        $(this).tab('show')
    });


    jQuery('[data-dismiss="modal"]').on('click', function() {
        jQuery('.modal').hide();
        jQuery('.modal-backdrop').hide();
    });

    bindFileUploadCrop('#upload-profile-photo');
});

function saveUserPassword() {

    if (true === $('#passwordchange').parsley().validate('block-account')) {

        $('#overlay').fadeIn(800);

        var users = new Object();
        users.password = $('#password').val();
        users.newpassword = $('#newPasswordInput').val();
        users.confirm_password = $('#verifyPasswordInput').val();

        $.ajax({
            type: 'PUT',
            url: '/EmployerPassword',
            data: {
                employer: users
            },
            dataType: 'json',
            success: function(data) {
                if (data.success === true) {
                    MessageBox('success',data.msg,true);
                    $('#overlay').fadeOut(800);
                } else {
                    $('#overlay').fadeOut(800);
                    if (data.field) {
                        $(data.field).next('ul').html(data.fieldError);
                        MessageBox('error',data.msg);
                        return false;
                    }
                    MessageBox('error',data.msg);
                    return false;
                }
            }
        });
    }
}

function bindFileUploadCrop(photoid) {
    var btnUpload = $(photoid);
    new AjaxUpload(btnUpload, {
        action: '/uploadpic',
        name: 'qqfile',
        responseType: 'json',
        onSubmit: function(file, ext) {
            if (!(ext && /^(jpg|png|jpeg|gif)$/.test(ext))) {
                // extension is not allowed
                MessageBox('error','Only JPG, PNG or GIF files are allowed');
                return false;
            }
            $('#overlay').fadeIn(800);
        },
        onComplete: function(file, response) {
            if (response.msg) {
                window.location.reload();
            } else {
                MessageBox('error',response.msg);
            }
        }
    });
    $('#ajax_upload_file_swer').mouseover(function() {
        $('.profile-picture-wrapper').addClass('active');
    });
    $('#ajax_upload_file_swer').mouseout(function() {
        $('.profile-picture-wrapper').removeClass('active');
    });


}
