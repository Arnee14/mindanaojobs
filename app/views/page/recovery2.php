<div class="main-container">
    <div class="container" id="step1">

        <div class="signup-widget">
            <div class="text-center">
                <div class="font-30 font-lato">Update
                    <span class="font-ultra-bold">My Password</span>
                </div>
            </div>

            <div class="row">
                <form class="form-horizontal" id="dopasswordchage" data-parsley-validate>
                    <input type="hidden" name="mode" value="<?php if(isset($mode) && !empty($mode)){ echo $mode;}?>" />
                    <input type="hidden" name="id" value="<?php if(isset($id) && !empty($id)){ echo $id;}?>" />
                    <div class="padding-md">
                        <div class="form-group m-top-md">
                            <label for="newPasswordInput" class="control-label col-sm-3">New Password</label>
                            <div class="col-sm-10 col-md-7">
                                <input type="password" class="form-control" name="newpass" id="newPasswordInput" data-parsley-group="block1" required>
                            </div>
                        </div>

                        <div class="form-group m-top-md">
                            <label for="verifyPasswordInput" class="control-label col-sm-3">Re-type Password</label>
                            <div class="col-sm-10 col-md-7">
                                <input type="password" class="form-control" name="newpass1" id="verifyPasswordInput" data-parsley-group="block1" required>
                                <strong class="error_recovery" style="color:red"></strong>
                            </div>
                        </div>

                        <div class="text-center m-top-lg m-bottom-lg">
                            <button class="btn btn-primary" id="passwordchange" type="button">
                                <!--<i class="fa fa-save m-right-xs"></i>-->Save Changes</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="container paddingTB-lg" id="step2">
        <div class="signup-widget">
            <div class="row m-top-15">
                <div class="text-center">
                    <div class="font-30 font-lato">
                        Your password has been updated successfully
                    </div>
                </div>
                <div class="text-center m-top-lg m-bottom-lg">
                    <a href="/login"> <button class="btn btn-primary btn-wide" type="button">Login as Job Seeker </button> </a>
                    <a href="/Employer"> <button class="btn btn-primary btn-wide" type="button">Login as Employer</button> </a>
                </div>
            </div>
        </div>
    </div>
</div>
