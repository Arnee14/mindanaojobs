<?php
return array(
    'index' => 'social',
    'login' => 'social/login/{provider}',
    'loginredirect' => '',   // set this if you want a default redirect after login, else it will use back()
    'logout' => 'social/logout',
    'logoutredirect' => '/',
    'authfailed' => 'user/login',
    'endpoint' => 'social/endpoint', // set this if you want a default redirect after logout, else it will use back()
);
