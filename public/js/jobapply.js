$(function(){

    $.get("api/findSkills", function(data) {
        var results = [];
        $.each(data.Skills, function(index, item) {
            results.push({
                id: item.skills_name,
                text: item.skills_name
            });
        });
        $('#cboSkills').select2({
            data: results,
            multiple: true
        });
    });

    $.get("api/jobcategories", function(data) {
        var results = [];
        $.each(data.jobcategories, function(index, item) {
            $.each(item.subcategory, function(index2, item2){
                results.push({
                    id: item2.description,
                    text: item2.description
                });
            })

        });
        $('#cboCategories').select2({
            data: results,
            multiple: true
        });
    });

    if (queryParameters().param === 'edit') {
        var id = queryParameters().id;
        $.get("jobs/jobskills?id=" + id, function(data) {
            var results = [];
            $.each(data, function(index, item) {
                results.push({
                    id: item.js_skill,
                    text: item.js_skill
                });
            });
            $('#cboSkills').select2("data", results);
        });


        $.get("jobs/jobcategories?id=" + id, function(data) {
            var results = [];
            $.each(data, function(index, item) {
                results.push({
                    id: item.jc_jobcategory,
                    text: item.jc_jobcategory
                });
            });
            $('#cboCategories').select2("data", results);
        });
    }


});

function queryParameters () {
    var result = {};

    var params = window.location.search.split(/\?|\&/);

    params.forEach( function(it) {
        if (it) {
            var param = it.split("=");
            result[param[0]] = param[1];
        }
    });

    return result;
}

function create() {
    if (true === $('#jobForm').parsley().validate('block1')) {
        $('#overlay').fadeIn(800);

        var data = new Object();

        data.j_id = $('#j_id').val();
        data.title = $('#title').val();
        data.description = $('#description').val();
        data.requirements = $('#requirements').val();
        data.salaryfrom = $('#salaryfrom').val();
        data.salaryTo = $('#salaryTo').val();
        data.employmentbasis = $('#employmentbasis').val();
        data.experience = $('#noOfExp').val();

        var arrCatrgories = new Array();
        var arrSkill = new Array();

        $.each($("#cboCategories").select2('data'), function() {
            var users = new Object();
            users.jc_jobcategory = $(this)[0].text;
            arrCatrgories.push(users);
        });

        $.each($("#cboSkills").select2('data'), function() {
            var users = new Object();
            users.js_skills = $(this)[0].text;
            arrSkill.push(users);
        });
        if(arrCatrgories.length == 0){
            MessageBox('warning','Please choose categories.');
            $('#overlay').fadeOut(800);
        }else if(arrSkill.length == 0){
            MessageBox('warning','Please choose skills.');
            $('#overlay').fadeOut(800);
        }else{
            data.categories = arrCatrgories;
            data.skills = arrSkill;

            $.ajax({
                url: '/jobs/create',
                async: true,
                type: 'GET',
                dataType: 'json',
                success: function(response) {
                    if(response.success == 'true'){
                        $.ajax({
                            url: '/jobpost',
                            type: 'PUT',
                            dataType: 'json',
                            data: {
                                job: data
                            },
                            success: function(response) {
                                    if (response.success === true) {
                                        MessageBox('success',response.msg);
                                        $('#overlay').fadeOut(800);
                                        window.location.href = '/MyJobs';
                                    } else {
                                        $('#overlay').fadeOut(800);

                                        if (response.field) {
                                            $(response.field).next('ul').html(response.fieldError);
                                            MessageBox('error',response.msg);
                                            return false;
                                        }
                                        MessageBox('error',response.msg);
                                        return false;
                                        window.location.reload();
                                    }
                            },
                            error: function(error) {
                                console.log(error);
                            }
                        });
                    }else{
                        $('#overlay').fadeOut(800);
                        MessageBox('error',response.msg);
                        return;
                    }
                },
            });

        }
    }
}

function submitApplication(){
    $('#overlay').fadeIn(800);

	var emailArr = new Array();
	var email = new Object();
	email.message = $('#proposals').val();
    email.mail =$('#hidden_email').val();
	if ($('#attach_resume').prop("checked") == true) {
        email.attached = true;
    } else {
        email.attached = false;
    }

    $.ajax({
        type: 'POST',
        url: 'api/email/applyJob',
        data: {
        	data:email
        },
        dataType: 'json',
        success: function(data) {
            $('#overlay').fadeOut(800);
            if (data.success == false) {
                if (data.msg != '') {
                    MessageBox('error',data.msg);
                    return false;
                }
            } else {
                MessageBox('success',data.msg,true);
            }
        }
    });

}
