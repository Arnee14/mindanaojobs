<div class="main-container" style="margin-bottom:70px;">
    <div class="section paddingT-40 paddingB-10">
        <div class="container">
          <div class="row col-md-8 col-sm-8">
            <div class="col-md-2 col-sm-2">
                <a href="#"
                    onclick="window.open('<?php echo $Share["facebook"] ?>','sharer','toolbar=0,status=0,width=548,height=325'); return false;"
                    class="btn btn-sm btn-social btn-facebook">
                    <i class="fa fa-facebook"></i> Share
                </a>
            </div>
            <div class="col-md-2 col-sm-2">
                <a href="#" onclick="window.open('<?php echo $Share["gplus"] ?>', 'print','width=620,height=400,scrollbars=1,resizable=1')"  class="btn btn-sm btn-social btn-google-plus">
                    <i class="fa fa-google-plus"></i> Share
                </a>
            </div>
            <div class="col-md-2 col-sm-2">
                <a href="#" onclick="window.open('<?php echo $Share["linkedin"] ?>','print','width=620,height=400,scrollbars=1,resizable=1')" class="btn btn-sm btn-social btn-linkedin">
                    <i class="fa fa-linkedin"></i> Share
                </a>
            </div>
             <div class="col-md-2 col-sm-2">
                <a href="#" onclick="window.open('<?php echo $Share["twitter"] ?>','print','width=620,height=400,scrollbars=1,resizable=1')" class="btn btn-sm btn-social btn-twitter">
                    <i class="fa fa-twitter"></i> Share
                </a>
            </div>
          </div>
        </div>
    </div>
    <div class="section paddingTB-lg">
        <div class="container block1">
            <form class="form-horizontal" action="https://outsource.com/job-request" data-parsley-validate id="proposal-form" method="post" enctype="multipart/form-data">
                <input type="hidden" name="hidden_email" id="hidden_email" value="<?php if(isset($Job) && !empty($Job)){ echo $Job->u_email; } ?>">
                <div class="row">
                    <div class="col-md-8">
                        <div class="font-lato font-24">
                            <span class="font-ultra-bold"><?php if(isset($Job) && !empty($Job)){ echo $Job->j_title; } ?></span>
                            <!---->
                            <!--(0)-->
                        </div>
                        <div class="m-top-10 font-16">
                            <p><?php if(isset($Job) && !empty($Job)){ echo $Job->j_description; } ?></p>
                        </div>
                        <hr class="line-dotted m-top-30 m-bottom-30">


                        <div class="font-18 font-lato font-ultra-bold">
                            Requirements</div>
                        <div class="row m-top-20">
                            <div class="col-sm-12 col-xs-12 m-top-10-xs">
                                <?php if(isset($Job) && !empty($Job)){ echo $Job->j_requirements; } ?>
                            </div>
                        </div>

                        <div class="font-18 font-lato font-ultra-bold">
                            Specialization</div>
                        <div class="row m-top-20">
                            <div class="col-sm-12 col-xs-12 m-top-10-xs">
                                <?php
                                    foreach ($Categories as $keys ) {
                                        echo '<div class="skill-tag skill-sm m-right-10">'.$keys->jc_jobcategory.'</div>';
                                    }
                                ?>
                            </div>
                        </div>

                        <div class="font-18 font-lato font-ultra-bold m-top-30">
                            About <?php if(isset($Job) && !empty($Job)){ echo $Job->e_companyname; } ?></div>
                        <div class="row m-top-20">
                            <div class="col-sm-12 col-xs-12 m-top-10-xs">
                                <p><?php if(isset($Job) && !empty($Job)){ echo $Job->e_description; } ?></p>
                            </div>
                        </div>

                        <div class="font-18 font-lato font-ultra-bold m-top-20">Message</div>
                        <p class="font-16 m-top-10 m-bottom-20">Introduce yourself and describe why you are a great candidate for this project.</p>
                        <textarea class="form-control" onkeyup="parsleyLeft(this)" data-parsley-length="[100, 5000]" rows="12" data-parsley-group="block1" data-parsley-rangelength="[100,5000]" data-parsley-minlength="100" data-parsley-maxlength="5000" maxlength="5000" rows="12" required name="proposals" id="proposals" placeholder="Enter your message here.." onfocus="if($(this).val() == $(this).attr('placeholder')) { $(this).val(''); }" onclick="if($(this).val() == $(this).attr('placeholder')) { $(this).val(''); }" onblur="if($(this).val() == '') { $(this).val( $(this).attr('placeholder')); }">
Hello and good day!

This letter is in response to your "<?php if(isset($Job) && !empty($Job)){ echo $Job->j_title; } ?>" job advertisement in MindanaoJobs.com on <?php echo date_format(date_create($Job->j_createdon), 'M d, Y'); ?> .

[Short description of your achievements and contact information here]

Thank you very much.

Sincerely yours,

<?php if(isset($User) && !empty($User)){ echo ($User->Fullname); } ?></textarea>
                        <div class="col-md-12 text-right">
                            <span class="left">5000</span>characters left</div>
                        <div class="checkbox m-left-10 m-top-5 file_uploader">
                            <label>
                              <input type="checkbox" id="attach_resume"> <i class="fa fa-paperclip m-right-5"></i> Attach a Resume
                            </label>
                        </div>

                        <!-- ./row -->

                        <hr class="line-dotted m-top-30 m-bottom-40">
                        <div class="m-top-40 m-left-20 m-bottom-40-sm">
                            <div class="row">
                                <div class="col-sm-4">
                                    <a href="javascript:void(0);" onclick="submitApplication()" class="btn btn-primary btn-wide font-16">Apply Now<i class="fa fa-angle-double-right font-14 m-left-5"></i></a>
                                </div>
                                <!-- ./col -->
                            </div>
                            <!-- ./row -->
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="bg-grey bordered m-top-30 rounded padding-25">
                            <div class="font-semi-bold font-18 line-1">Posted on</div>
                            <span><?php echo date_format(date_create($Job->j_createdon), 'M d, Y h:i A'); ?></span>

                            <h4 class="font-semi-bold m-top-30">Location</h4>
                            <address>
                                <span>
                                    <?php if(isset($Job) && !empty($Job)){ echo $Job->u_city; } ?>
                                </span>
                            </address>

                            <h4 class="font-semi-bold m-top-30">Employment Basis</h4>
                            <address>
                                <span>
                                    <a href="javascript:void(0)" class="btn btn-link"><?php if(isset($Job) && !empty($Job)){ echo $Job->j_employmentbasis; } ?></a>
                                </span>
                            </address>

                            <h4 class="font-semi-bold m-top-30">Skills Required</h4>
                            <div class="m-top-20 m-bottom-10">
                                <?php
                                    if(isset($Skills) && !empty($Skills)){
                                        foreach ($Skills as $keys ) {
                                            echo '<div class="skill-tag skill-sm static-text m-right-10" style="font-size: 8px;">'.$keys->js_skill.'</div>';
                                        }
                                    }
                                ?>
                            </div>

                            <h4 class="font-semi-bold font-18 line-1">Company Profile</h4>
                            <address>
                                <strong><?php if(isset($Job) && !empty($Job)){ echo $Job->u_address; } ?></strong><br>
                                <?php if(isset($Job) && !empty($Job)){ echo $Job->u_address; } ?><br>
                                <?php if(isset($Job) && !empty($Job)){ echo $Job->u_city; } ?>, <?php if(isset($Job) && !empty($Job)){ echo $Job->u_province; } ?> <?php if(isset($Job) && !empty($Job)){ echo $Job->u_zipcode; } ?><br>
                                <abbr title="Phone">Telephone:</abbr><?php if(isset($Job) && !empty($Job)){ echo $Job->u_telephoneno; } ?>
                                <br>
                                <abbr title="MPhone">Mobile Phone:</abbr><?php if(isset($Job) && !empty($Job)){ echo $Job->u_mobileno; } ?>
                                <br>
                                <abbr title="Fax">Fax:</abbr><?php if(isset($Job) && !empty($Job)){ echo $Job->u_fax; } ?>
                            </address>
                            <div class="m-top-5 font-ultra-bold font-24">
                                <?php if(isset($Job) && !empty($Job)){ echo $Job->e_companyname; } ?>
                            </div>
                            <ul class="list-unstyled m-top-10">
                                <li class="m-bottom-5">
                                    <span class="font-semi-bold">Salary: </span><?php if(isset($Job) && !empty($Job)){ echo $Job->j_salaryfrom. ' - '.$Job->j_salaryfrom; } ?></li>
                                <li class="m-bottom-5">
                                    <span class="font-semi-bold">Website:</span>
                                    <a class="text-blue" href='<?php if(isset($Job) && !empty($Job)){ echo $Job->u_website; } ?>'><?php if(isset($Job) && !empty($Job)){ echo $Job->u_website; } ?></a>
                                </li>
                            </ul>
                        </div>

                    </div>

                    <!-- ./col -->
                </div>
            </form>
            <!-- ./container -->
        </div>
        <!-- section -->
    </div>
    <!-- ./main-container -->
