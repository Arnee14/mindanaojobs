$(function(){
	// JOB SEEKER SKILL AUTOCOMPLETE
    $.get("api/findSkills", function(data) {
        var results = [];
        $.each(data.Skills, function(index, item) {
            results.push({
                id: item.skills_name,
                text: item.skills_name
            });
        });
        $('#cboSkills').select2({
            data: results,
            multiple: true
        });
    });

    $.get("api/jobcategories", function(data) {
        var results = [];
        $.each(data.jobcategories, function(index, item) {
        	$.each(item.subcategory, function(index2, item2){
	        	results.push({
	                id: item2.description,
	                text: item2.description
	            });
        	})

        });
        $('#cboCategories').select2({
            data: results,
            multiple: true
        });
    });

    var prevKey = -1, prevControl = '';
    $('input[name="noOfExp"]').keydown(function (event) {
        if (!(
            event.keyCode == 8 // backspace
            || event.keyCode == 9 // tab
            || event.keyCode == 17 // ctrl
            || event.keyCode == 46 // delete
            || (event.keyCode >= 35 && event.keyCode <= 40) // arrow keys/home/end
            || (event.keyCode >= 48 && event.keyCode <= 57) // numbers on keyboard
            || (event.keyCode >= 96 && event.keyCode <= 105) // number on keypad
            || (event.keyCode == 65 && prevKey == 17 && prevControl == event.currentTarget.id) // ctrl + a, on same control
            || ((event.keyCode == 190 || event.keyCode == 110) && $(this).val().indexOf('.') == -1 && $(this).val() != '') // If '.' and it's not existing
            )
        ) {
            event.preventDefault();     // Prevent character input
        }
        else {
            if((event.keyCode == 96 || event.keyCode == 48) && $(this).val() == '') { // Prevent '.' character if there is no existing value
                event.preventDefault();
            }
            if($(this).val().indexOf('.') != -1) { // Prevent input of the number of decimal places is already 2
                if(
                    $(this).val().substr(parseInt($(this).val().indexOf('.') + 1)).length == 2 &&
                    (
                        (event.keyCode >= 48 && event.keyCode <= 57) ||
                        (event.keyCode >= 96 && event.keyCode <= 105)
                    )
                ) {
                    event.preventDefault();
                }
            }
            prevKey = event.keyCode;
            prevControl = event.currentTarget.id;
        }
    });

	$('input[name="salaryfrom"]').keydown(function (event) {
        if (!(
            event.keyCode == 8 // backspace
            || event.keyCode == 9 // tab
            || event.keyCode == 17 // ctrl
            || event.keyCode == 46 // delete
            || (event.keyCode >= 35 && event.keyCode <= 40) // arrow keys/home/end
            || (event.keyCode >= 48 && event.keyCode <= 57) // numbers on keyboard
            || (event.keyCode >= 96 && event.keyCode <= 105) // number on keypad
            || (event.keyCode == 65 && prevKey == 17 && prevControl == event.currentTarget.id) // ctrl + a, on same control
            || ((event.keyCode == 190 || event.keyCode == 110) && $(this).val().indexOf('.') == -1 && $(this).val() != '') // If '.' and it's not existing
            )
        ) {
            event.preventDefault();     // Prevent character input
        }
        else {
            if((event.keyCode == 96 || event.keyCode == 48) && $(this).val() == '') { // Prevent '.' character if there is no existing value
                event.preventDefault();
            }
            if($(this).val().indexOf('.') != -1) { // Prevent input of the number of decimal places is already 2
                if(
                    $(this).val().substr(parseInt($(this).val().indexOf('.') + 1)).length == 2 &&
                    (
                        (event.keyCode >= 48 && event.keyCode <= 57) ||
                        (event.keyCode >= 96 && event.keyCode <= 105)
                    )
                ) {
                    event.preventDefault();
                }
            }
            prevKey = event.keyCode;
            prevControl = event.currentTarget.id;
        }
    });

	$('input[name="salaryTo"]').keydown(function (event) {
        if (!(
            event.keyCode == 8 // backspace
            || event.keyCode == 9 // tab
            || event.keyCode == 17 // ctrl
            || event.keyCode == 46 // delete
            || (event.keyCode >= 35 && event.keyCode <= 40) // arrow keys/home/end
            || (event.keyCode >= 48 && event.keyCode <= 57) // numbers on keyboard
            || (event.keyCode >= 96 && event.keyCode <= 105) // number on keypad
            || (event.keyCode == 65 && prevKey == 17 && prevControl == event.currentTarget.id) // ctrl + a, on same control
            || ((event.keyCode == 190 || event.keyCode == 110) && $(this).val().indexOf('.') == -1 && $(this).val() != '') // If '.' and it's not existing
            )
        ) {
            event.preventDefault();     // Prevent character input
        }
        else {
            if((event.keyCode == 96 || event.keyCode == 48) && $(this).val() == '') { // Prevent '.' character if there is no existing value
                event.preventDefault();
            }
            if($(this).val().indexOf('.') != -1) { // Prevent input of the number of decimal places is already 2
                if(
                    $(this).val().substr(parseInt($(this).val().indexOf('.') + 1)).length == 2 &&
                    (
                        (event.keyCode >= 48 && event.keyCode <= 57) ||
                        (event.keyCode >= 96 && event.keyCode <= 105)
                    )
                ) {
                    event.preventDefault();
                }
            }
            prevKey = event.keyCode;
            prevControl = event.currentTarget.id;
        }
    });

});



$(document).on("click",".remove-jobs",function() {
	$('#hidden_j_id').val($(this).data('id'));

	$.ajax({
        url: '/jobs/create',
        async: true,
        type: 'GET',
        dataType: 'json',
        success: function(response) {
            if(response.success == 'true'){
                $('#confirmDeleteJob').modal();
            }else{
                MessageBox('error',response.msg);
                return;
            }
        },
    });
});

function beforeCreate(){
	$.ajax({
	    url: '/jobs/create',
	    async: true,
	    type: 'GET',
	    dataType: 'json',
	    success: function(response) {
	    	if(response.success == 'true'){
	    		window.location.href='/jobpost';
	    	}else{
	    		MessageBox('error',response.msg);
	    		return;
	    	}
	    },
	    error: function(error) {
	    	console.log(error);
	    }
	});
}

function confirmactionSuccess(){
	$.ajax({
	    url: '/delete',
	    async: true,
	    type: 'DELETE',
	    crossDomain: true,
	    dataType: 'json',
	    data: {
	        id: $('#hidden_j_id').val()
	    },
	    success: function(response) {
	    	if(response.success == true){
	    		MessageBox('success',response.msg,true);
	    	}else{
	    		MessageBox('error',response.msg);
	    		return;
	    	}
	    },
	    error: function(error) {
	    	console.log(error);
	    }
	});
}

function create() {
	if (true === $('#jobForm').parsley().validate('block1')) {
		$('#overlay').fadeIn(800);
		var data = new Object();
		data.title = $('#title').val();
		data.description = $('#description').val();
		data.requirements = $('#requirements').val();
		data.salaryfrom = $('#salaryfrom').val();
		data.salaryTo = $('#salaryTo').val();
		data.experience = $('#noOfExp').val();
		data.employmentbasis = $('#employmentbasis').val();

		var arrCatrgories = new Array();
		var arrSkill = new Array();

	    $.each($("#cboCategories").select2('data'), function() {
	        var users = new Object();
	        users.jc_jobcategory = $(this)[0].text;
	        arrCatrgories.push(users);
	    });

	     $.each($("#cboSkills").select2('data'), function() {
	        var users = new Object();
	        users.js_skills = $(this)[0].text;
	        arrSkill.push(users);
	    });


	    data.categories = arrCatrgories;
	    data.skills = arrSkill;

	    if(arrCatrgories.length == 0){
	    	MessageBox('warning','Please choose categories.');
	    	$('#overlay').fadeOut(800);
	    }else if(arrSkill.length == 0){
	    	MessageBox('warning','Please choose skills.');
	    	$('#overlay').fadeOut(800);
	    }else{
	    	$.ajax({
			    url: '/jobs/create',
			    async: true,
			    type: 'GET',
			    dataType: 'json',
			    success: function(response) {
			    	if(response.success == 'true'){
			    		$.ajax({
						    url: '/jobpost',
						    type: 'POST',
						    dataType: 'json',
						    data: {
						        job: data
						    },
						    success: function(response) {
					                if (response.success === true) {
					                    MessageBox('success',response.msg);
					                    $('#overlay').fadeOut(800);
					                    window.location.href = '/MyJobs';
					                } else {
					                    $('#overlay').fadeOut(800);

					                    if (response.field) {
					                        $(response.field).next('ul').html(response.fieldError);
					                        MessageBox('error',response.msg);
					                        return false;
					                    }
					                    MessageBox('error',response.msg);
					                    return false;
					                    window.location.reload();
					                }
						    },
						});
			    	}else{
			    		$('#overlay').fadeOut(800);
			    		MessageBox('error',response.msg);
			    		return;
			    	}
			    },
			});
	    }
	}
}

function view(id){
	$.ajax({
	    url: '/MyJobs',
	    async: true,
	    type: 'POST',
	    crossDomain: true,
	    dataType: 'json',
	    data: {
	        data : id
	    },
	    success: function(response) {
	    	console.log(response);
	    },
	    error: function(error) {
	    	console.log(error);
	    }
	});
}

