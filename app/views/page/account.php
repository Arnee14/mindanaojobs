<div class="main-container">
    <?php if(isset($User) && !empty($User)){
        if ($User->u_isverified != '1'){
    ?>
    <div class="section bg-danger paddingTB-10 alert-top section-border-bottom">
        <div class="container">
          <div class="row">
            <div class="col-sm-7">
                <i class="pull-left m-right-15 m-top-5 no-margin-xs fa fa-3x fa-exclamation-triangle text-danger"></i>
                <div class="font-21 font-semi-bold title-text"> Your account is currently disabled. </div>
                <div class="font-lato information-text">
                    <span class="font-14 font-semi-bold">Check your email to verify your account. <a href="javascript:resendConfirmation()">Click here</a></span> to resend confirmation
                </div>
            </div>
          </div>
        </div>
    </div>
    <?php
            }
        }
    ?>
    <div class="section section-border-bottom paddingB-40">
        <div class="container">
            <div class="paddingTB-40">
                <div class="row">
                    <div class="col-md-6">
                        <div class="font-30 font-ultra-bold font-lato line-1">
                            Account Information
                        </div>
                        <div class="font-18 m-top-10 m-bottom-20">
                            Update/Edit Your Account
                        </div>
                    </div>
                </div>
                <!-- ./row -->
            </div>
        </div>
    </div>
    <div class="section">
        <div class="bg-grey">
            <div class="container">
                <div class="row">
                    <ul id="myTab" class="nav nav-tabs custom-tab">
                        <li class="active"><a href="#accountinfoTab" > <i class="fa fa-book"></i> Account Information</a></li>
                        <?php if(isset($User) && !empty($User)){
                            if ($User->u_accounttype == 'local'){
                        ?>
                        <li class=""><a href="#passwordTab"><i class="fa fa-lock"></i> Password</a></li>
                        <?php }
                            }
                        ?>
                        <!-- <li class="tab-lg "><a href="#emailNotifTab"><i class="fa fa-envelope m-right-5"></i> Email Notification</a></li> -->
                    </ul>
                    <div id="myTabContent" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="accountinfoTab" aria-labelledBy="accountinfoTab">
                            <div class="padding-10 paddingTB-40 m-bottom-20">
                                <div class="font-lato font-30 font-ultra-bold">Account Information</div>
                                <div class="font-16 m-top-5">Change your basic account settings and location.</div>
                                <form class="form-horizontal" id="accountdetails" data-parsley-validate>
                                    <div class="form-group m-top-40">
                                        <label for="professionalTitle" class="col-sm-3 control-label font-semi-bold font-14">Add Photo :</label>
                                        <div class="col-sm-9 col-md-7">
                                            <div class="clearfix">
                                                <a href="#" class="thumbnail inline-block pull-left m-right-md" style="margin-bottom:0;">
                                                    <?php if(isset($User) && !empty($User) && !empty($User->u_displaypicture)) { ?>
                                                        <img src="<?php echo $User->u_displaypicture; ?>" alt="..." class="img-rounded" style="max-width: 143px; max-height: 143px;">
                                                    <?php }else{ ?>
                                                        <img src="../img/photo.jpg" alt="..." class="img-rounded" style="max-width: 143px; max-height: 143px;">
                                                    <?php } ?>
                                                </a>
                                                <div class="m-top-20"> <a class="btn btn-primary m-left-sm" id="upload-profile-photo">Upload</a>
                                                  <p class="text-muted m-top-10"><small>Tip:  Add a professional photo of yourself to attract more clients.</small></p>
                                                </div>
                                            </div>
                                            <div class="m-left-20 m-top-5 editlogo">
                                                <a href="javascript:void(0)" onclick="delete_logo()">Delete</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group m-top-md">
                                        <label for="LastName" class="control-label col-sm-3 font-semi-bold">Website</label>
                                        <div class="col-sm-9 col-md-7">
                                            <input type="text" class="form-control" id="inputWebsite" name="inputWebsite" value="<?php if(isset($User) && !empty($User)) { echo $User->u_website; } ?>" data-parsley-group="block-account">
                                        </div>
                                    </div>
                                    <div class="form-group m-top-40">
                                        <label for="emailInput" class="control-label col-sm-3 font-semi-bold">Email Address</label>
                                        <div class="col-sm-9 col-md-7">
                                            <input type="email" class="form-control" id="emailInput" name="emailInput" value="<?php if(isset($User) && !empty($User)) { echo $User->u_email; } ?>" required data-parsley-group="block-account">
                                        </div>
                                    </div>
                                    <div class="form-group m-top-md">
                                        <label for="FirstName" class="control-label col-sm-3 font-semi-bold">First Name</label>
                                        <div class="col-sm-9 col-md-7">
                                            <input type="text" class="form-control" id="FirstName" name="FirstName" value="<?php if(isset($User) && !empty($User)) { echo $User->js_firstname; } ?>" required data-parsley-group="block-account">
                                        </div>
                                    </div>
                                    <div class="form-group m-top-md">
                                        <label for="LastName" class="control-label col-sm-3 font-semi-bold">Last Name</label>
                                        <div class="col-sm-9 col-md-7">
                                            <input type="text" class="form-control" id="LastName" name="LastName" value="<?php if(isset($User) && !empty($User)) { echo $User->js_lastname; } ?>" required data-parsley-group="block-account">
                                        </div>
                                    </div>
                                    <div class="form-group m-top-md">
                                        <label for="LastName" class="control-label col-sm-3 font-semi-bold">Middle Initial</label>
                                        <div class="col-sm-9 col-md-7">
                                            <input type="text" class="form-control" id="middleInitial" name="middleInitial" value="<?php if(isset($User) && !empty($User)) { echo $User->js_middleinitial; } ?>">
                                        </div>
                                    </div>
                                    <div class="form-group m-top-md">
                                        <label for="age" class="control-label col-sm-3 font-semi-bold">Gender</label>
                                        <div class="col-sm-9 col-md-7">
                                            <select class="form-control style-form" id="gender" name="gender" required placeholder="Gender" data-parsley-group="block-account">
                                                <option value="Male" <?php if(isset($User) && !empty($User)) { if ($User->js_gender == "Male"){ echo 'selected = "selected"';}}?>>
                                                    Male
                                                </option>
                                                <option value="Female" <?php if(isset($User) && !empty($User)) { if ($User->js_gender == "Female"){ echo 'selected = "selected"';}}?>>
                                                    Female
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group m-top-md">
                                        <label for="LastName" class="control-label col-sm-3 font-semi-bold">Mobile No</label>
                                        <div class="col-sm-9 col-md-7">
                                            <input type="text" class="form-control" id="mobileNo" name="mobileNo" value="<?php if(isset($User) && !empty($User)) { echo $User->u_mobileno; } ?>">
                                        </div>
                                    </div>
                                    <div class="form-group m-top-md">
                                        <label for="LastName" class="control-label col-sm-3 font-semi-bold">Telephone No</label>
                                        <div class="col-sm-9 col-md-7">
                                            <input type="text" class="form-control" id="telephoneNo" name="telephoneNo" value="<?php if(isset($User) && !empty($User)) { echo $User->u_telephoneno; } ?>">
                                        </div>
                                    </div>
                                    <div class="form-group m-top-md">
                                        <label for="LastName" class="control-label col-sm-3 font-semi-bold">Fax No</label>
                                        <div class="col-sm-9 col-md-7">
                                            <input type="text" class="form-control" id="faxNo" name="faxNo" value="<?php if(isset($User) && !empty($User)) { echo $User->u_fax; } ?>">
                                        </div>
                                    </div>
                                    <div class="form-group m-top-md">
                                        <label for="desiredSalary" class="control-label col-sm-3 font-semi-bold">Birthdate</label>
                                        <div class="col-sm-9 col-md-7">
                                            <div class='input-group date' id='datetimepicker1'>
                                                <input type='text' class="form-control" data-date-format="YYYY-MM-DD" value="<?php if(isset($User) && !empty($User)) { echo $User->js_birthdate; } ?>"/>
                                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group m-top-20">
                                        <label class="control-label col-sm-3 font-semi-bold font-21">Location</label>
                                        <div class="col-sm-9 col-md-7"></div>
                                    </div>
                                    <div class="form-group m-top-10">
                                        <label for="address" class="control-label col-sm-3 font-semi-bold">Address</label>
                                        <div class="col-sm-9 col-md-7">
                                            <input type="text" class="form-control" id="address" name="address" value="<?php if(isset($User) && !empty($User)) { echo $User->u_address; } ?>" required data-parsley-group="block-account">
                                        </div>
                                    </div>
                                    <div class="form-group m-top-md">
                                        <label for="stateInput" class="control-label col-sm-3 font-semi-bold">State / Province</label>
                                        <div class="col-sm-9 col-md-7">
                                            <input type="text" class="form-control" id="stateInput" name="stateInput" value="<?php if(isset($User) && !empty($User)) { echo $User->u_province; } ?>" required data-parsley-group="block-account">
                                        </div>
                                    </div>
                                    <div class="form-group m-top-md">
                                        <label for="provinceInput" class="control-label col-sm-3 font-semi-bold">City</label>
                                        <div class="col-sm-9 col-md-7">
                                            <select name="city" id="city" class="form-control">
                                                <?php
                                                    if (isset($joblocation2['location2']) && !empty($joblocation2['location2'])){
                                                        foreach ($joblocation2['location2'] as $location) { ?>
                                                            <optgroup label="<?php echo $location['region']; ?>">
                                                                <?php
                                                                    if (isset($location['Town_Cities']) && !empty($location['Town_Cities'])){
                                                                        foreach ($location['Town_Cities'] as $item) {
                                                                            if ($item['locationname'] == $User->u_city){
                                                                                echo '<option selected="selected" value="'.$item['locationname'].'">
                                                                                    '.$item['locationname'].'
                                                                                </option>';
                                                                            }else{
                                                                                echo '<option value="'.$item['locationname'].'">
                                                                                    '.$item['locationname'].'
                                                                                </option>';
                                                                            }
                                                                        }
                                                                    }
                                                                ?>
                                                            </optgroup>';
                                                <?php
                                                        }
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group m-top-md">
                                        <label for="age" class="control-label col-sm-3 font-semi-bold">Zip Code</label>
                                        <div class="col-sm-9 col-md-7">
                                            <input type="text" class="form-control" id="zipCode" name="zipCode" value="<?php if(isset($User) && !empty($User)) { echo $User->u_zipcode; } ?>" required data-parsley-group="block-account">
                                        </div>
                                    </div>
                                    <div class="text-center m-top-30">
                                        <a class="btn btn-lg btn-info m-right-10 btn-savechange" href="javascript:saveUserPersonalInfo();">Save Changes</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <?php if(isset($User) && !empty($User)){
                            if ($User->u_accounttype == 'local'){
                        ?>
                        <div role="tabpanel" class="tab-pane fade" id="passwordTab" aria-labelledBy="passwordTab">
                            <div class="padding-10 paddingTB-40 m-bottom-20">
                                <div class="font-lato font-30 font-ultra-bold">Password</div>
                                <div class="font-16 m-top-5">Change your password or request a new one.</div>
                                <form class="form-horizontal" id="passwordchange" data-parsley-validate>
                                    <input type="hidden" name="cmd" value="_do-password-change">
                                    <div class="form-group m-top-40">
                                        <label for="password" class="control-label col-sm-3 font-semi-bold">Current Password</label>
                                        <div class="col-sm-9 col-md-7 has-icon-alert">
                                            <span class="form-check relative"><i class="fa fa-check text-dark-green tick_absolute"></i>
                                            </span>
                                            <input type="password" class="form-control" id="password" name="password" required data-parsley-group="block-account">
                                        </div>
                                    </div>
                                    <div class="form-group m-top-20">
                                        <label for="newPasswordInput" class="control-label col-sm-3 font-semi-bold">New Password</label>
                                        <div class="col-sm-9 col-md-7 has-icon-alert">
                                            <span class="form-check relative"><i class="fa fa-check text-dark-green tick_absolute"></i>
                                            </span>
                                            <input type="password" class="form-control" id="newPasswordInput" name="newPasswordInput" required data-parsley-group="block-account">
                                        </div>
                                    </div>
                                    <div class="form-group m-top-20">
                                        <label for="verifyPasswordInput" class="control-label col-sm-3 font-semi-bold">Verify Password</label>
                                        <div class="col-sm-9 col-md-7 has-icon-alert">
                                            <span class="form-check relative"><i class="fa fa-check text-dark-green tick_absolute"></i>
                                            </span>
                                            <input type="password" class="form-control" id="verifyPasswordInput" name="verifyPasswordInput" required data-parsley-group="block-account">
                                        </div>
                                    </div>
                                    <div class="form-group m-top-30">
                                        <label class="control-label col-sm-3"></label>
                                        <div class="col-sm-9 col-md-7">
                                            <button onclick="saveUserPassword()" class="btn btn-lg btn-info m-left-20" type="button">Save Changes</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <?php }
                            }
                        ?>
                        <!-- <div role="tabpanel" class="tab-pane fade" id="emailNotifTab" aria-labelledBy="emailNotifTab">
                            <div class="padding-10 paddingTB-40 m-bottom-20">
                                <div class="font-lato font-30 font-ultra-bold">Email Notifications</div>
                                <div class="font-16 m-top-5">Control the emails that you receive from Outsource.</div>
                                <form class="form-horizontal m-top-30" id="notifications">
                                    <div class="m-bottom-20">
                                        <div class="custom-checkbox">
                                            <input type="checkbox" id="optionCheck" class="item-check checkbox-green" name="newproject_update" value="1" checked>
                                            <label for="optionCheck"></label>
                                        </div>
                                        <span class="font-16">New Job Postings</span>
                                    </div>
                                    <div class="m-bottom-20">
                                        <div class="custom-checkbox">
                                            <input type="checkbox" id="optionCheck1" class="item-check checkbox-green" name="project_update" value="1" checked>
                                            <label for="optionCheck1"></label>
                                        </div>
                                        <span class="font-16">Job Updates</span>
                                    </div>
                                    <div class="m-bottom-20">
                                        <div class="custom-checkbox">
                                            <input type="checkbox" id="optionCheck2" class="item-check checkbox-green" name="newsletter_update" value="1" checked>
                                            <label for="optionCheck2"></label>
                                        </div>
                                        <span class="font-16">Newsletters & Tips to Get More Out of MindanaoJobs.com</span>
                                    </div>
                                    <div class="m-top-30 m-left-30">
                                        <a class="btn btn-info btn-lg btn-wide"  href="javascript:saveUserNotification();">Save Changes</a>
                                    </div>
                                    <p class="font-12 m-top-20 m-left-20">You will always receive important emails related to your MindanaoJobs.com account such as those related to password recovery, Terms and Conditions, billing, and account activity.</p>

                                    <p class="font-12 m-top-20 m-left-20">If you would like to have your account deactivated, please email us at <a href="mailto:support@mindanaojobs.com" class="dnd">support@mindanaojobs.com</a>.</p>
                                </form>
                            </div>
                        </div> -->
                    </div>
                </div>
                <!-- ./row -->
            </div>
        </div>
    </div>
</div>
