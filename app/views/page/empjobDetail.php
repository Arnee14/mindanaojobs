<div class="main-container" style="margin-bottom:70px;">
    <div class="section paddingTB-lg">
        <div class="container block1">
            <form class="form-horizontal" data-parsley-validate id="proposal-form" method="post" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-8">
                        <div class="font-lato font-24">
                            <span class="font-ultra-bold"><?php if(isset($Job) && !empty($Job)){ echo $Job->j_title; } ?></span>
                            <!---->
                            <!--(0)-->
                        </div>
                        <div class="m-top-10 font-16">
                            <p>
                                <span class="font-semi-bold"><?php if(isset($Job) && !empty($Job)){ echo $Job->e_companyname; } ?></span>
                                is looking for a
                                <span class="font-semi-bold"><?php if(isset($Job) && !empty($Job)){ echo $Job->j_title; } ?></span>
                                 to join our team.
                            </p>
                            <p><?php if(isset($Job) && !empty($Job)){ echo $Job->j_description; } ?></p>
                        </div>
                        <hr class="line-dotted m-top-30 m-bottom-30">


                        <div class="font-18 font-lato font-ultra-bold">
                            Requirements</div>
                        <div class="row m-top-20">
                            <div class="col-sm-12 col-xs-12 m-top-10-xs">
                                <?php if(isset($Job) && !empty($Job)){ echo $Job->j_requirements; } ?>
                            </div>
                        </div>

                        <div class="font-18 font-lato font-ultra-bold">
                            Specialization</div>
                        <div class="row m-top-20">
                            <div class="col-sm-12 col-xs-12 m-top-10-xs">
                                <?php
                                    foreach ($Categories as $keys ) {
                                        echo '<div class="skill-tag skill-sm m-right-10">'.$keys->jc_jobcategory.'</div>';
                                    }
                                ?>
                            </div>
                        </div>

                        <div class="font-18 font-lato font-ultra-bold m-top-30">
                            About <?php if(isset($Job) && !empty($Job)){ echo $Job->e_companyname; } ?></div>
                        <div class="row m-top-20">
                            <div class="col-sm-12 col-xs-12 m-top-10-xs">
                                <p><?php if(isset($Job) && !empty($Job)){ echo $Job->e_description; } ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="bg-grey bordered m-top-30 rounded padding-25">
                            <div class="font-semi-bold font-18 line-1">Posted on</div>
                            <span><?php echo date_format(date_create($Job->j_createdon), 'M d, Y h:i A'); ?></span>

                            <h4 class="font-semi-bold m-top-30">Location</h4>
                            <address>
                                <span>
                                    <?php if(isset($Job) && !empty($Job)){ echo $Job->u_city; } ?>
                                </span>
                            </address>

                            <h4 class="font-semi-bold m-top-30">Employment Basis</h4>
                            <address>
                                <span>
                                    <a href="javascript:void(0)" class="btn btn-link"><?php if(isset($Job) && !empty($Job)){ echo $Job->j_employmentbasis; } ?></a>
                                </span>
                            </address>

                            <h4 class="font-semi-bold m-top-30">Skills Required</h4>
                            <div class="m-top-20 m-bottom-10">
                                <?php
                                    if(isset($Skills) && !empty($Skills)){
                                        foreach ($Skills as $keys ) {
                                            echo '<div class="skill-tag skill-sm static-text m-right-10" style="font-size: 8px;">'.$keys->js_skill.'</div>';
                                        }
                                    }
                                ?>
                            </div>

                            <h4 class="font-semi-bold font-18 line-1">Company Profile</h4>
                            <address>
                                <strong><?php if(isset($Job) && !empty($Job)){ echo $Job->u_address; } ?></strong><br>
                                <?php if(isset($Job) && !empty($Job)){ echo $Job->u_address; } ?><br>
                                <?php if(isset($Job) && !empty($Job)){ echo $Job->u_city; } ?>, <?php if(isset($Job) && !empty($Job)){ echo $Job->u_province; } ?> <?php if(isset($Job) && !empty($Job)){ echo $Job->u_zipcode; } ?><br>
                                <abbr title="Phone">Telephone:</abbr><?php if(isset($Job) && !empty($Job)){ echo $Job->u_telephoneno; } ?>
                                <br>
                                <abbr title="MPhone">Mobile Phone:</abbr><?php if(isset($Job) && !empty($Job)){ echo $Job->u_mobileno; } ?>
                                <br>
                                <abbr title="Fax">Fax:</abbr><?php if(isset($Job) && !empty($Job)){ echo $Job->u_fax; } ?>
                            </address>
                            <div class="m-top-5 font-ultra-bold font-24">
                                <?php if(isset($Job) && !empty($Job)){ echo $Job->e_companyname; } ?>
                            </div>
                            <ul class="list-unstyled m-top-10">
                                <li class="m-bottom-5">
                                    <span class="font-semi-bold">Salary: </span><?php if(isset($Job) && !empty($Job)){ echo $Job->j_salaryfrom. ' - '.$Job->j_salaryfrom; } ?></li>
                                <li class="m-bottom-5">
                                    <span class="font-semi-bold">Website:</span>
                                    <a class="text-blue" href='<?php if(isset($Job) && !empty($Job)){ echo  "http://".$Job->u_website; } ?>'><?php if(isset($Job) && !empty($Job)){ echo $Job->u_website; } ?></a>
                                </li>
                            </ul>
                        </div>

                    </div>

                    <!-- ./col -->
                </div>
            </form>
            <!-- ./container -->
        </div>
        <!-- section -->
    </div>
    <!-- ./main-container -->
