$(document).ready(function() {
    var k = $('.contacts-form .contacts-area').length;

    $('.registeremployer').on('click', function() {
        registerEmployer('#registerform');
    });

    if (k == 0) {
        FormAdd('contacts')
        $('.contacts-form .rm-contacts').hide();
    }

});

function FormAdd(lobj) {
    $('.' + lobj + '-form').append($('.' + lobj + '-form-hidden').html()).find('.form-active').fadeIn(400).removeClass('form-active');
}

function RForm(obj, lobj) {
    var k = $('.' + lobj + '-form .' + lobj + '-area').length;
    if (k > 1) {
        $(obj).parent().parent().parent().fadeOut(400, function() {
            $(this).remove()
        });
    }
}



function registerEmployer(obj) {

    if (true === $('#registerform').parsley().validate('block1')) {


        $('#overlay').fadeIn(800);

        if($('#password').val().length < 8){
            MessageBox('warning',"WARNING: Password too short");
            $('#overlay').fadeOut(800);
            return;
        }else if($('#password').val() != $('#password1').val()){
            MessageBox('warning',"WARNING: Password doesn't match");
            $('#overlay').fadeOut(800);
            return;
        }else{
            var data = new Object();
            data.compname = $('#compname').val();
            data.desc = $('#desc').val();
            data.address = $('#address').val();
            data.website = $('#website').val();
            data.email = $('#email').val();
            data.password = $('#password').val();
            data.password1 = $('#password1').val();
            data.landline = $('#telephone').val();
            data.mobile = $('#mobile').val();
            data.u_accounttype = 'local';

            $.ajax({
                type: 'POST',
                url: '/EmployerForm',
                data: {
                    employer : data
                },
                dataType: 'json',
                success: function(response) {
                    var decode = response;
                    if (decode.success === 'true') {
                        window.location.href=decode.page;
                    } else {
                       $('#overlay').fadeOut(800);
                        if (decode.field) {
                            MessageBox('error',decode.msg);
                            $(decode.field).next('ul').html(decode.fieldError);
                            return false;
                        }else{
                            MessageBox('error',decode.msg);
                            window.location.href=data.page;
                        }
                    }
                }
            });
        }
    }
}
