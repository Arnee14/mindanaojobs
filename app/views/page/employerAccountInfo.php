<div class="main-container">
    <?php if(isset($error) && !empty($error)){?>
    <div class="alert alert-danger alert-dismissible fade in" role="alert" style="z-index:99;position:fixed;width:100%;text-align: center;">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">Ã—</span><span class="sr-only">Close</span></button>
        <h4><?php echo $error['title'];?></h4>
        <p><?php echo $error['message'];?></p>
    </div>
    <?php }
    if(isset($Employer) && !empty($Employer)){
        if ($Employer->u_isverified != '1'){
    ?>
    <div class="section bg-danger paddingTB-10 alert-top section-border-bottom">
        <div class="container">
          <div class="row">
            <div class="col-sm-7">
                <i class="pull-left m-right-15 m-top-5 no-margin-xs fa fa-3x fa-exclamation-triangle text-danger"></i>
                <div class="font-21 font-semi-bold title-text"> Your account is currently disabled. </div>
                <div class="font-lato information-text">
                    <span class="font-14 font-semi-bold">Check your email to verify your account. <a href="javascript:resendConfirmation()">Click here</a></span> to resend confirmation
                </div>
            </div>
          </div>
        </div>
    </div>
    <?php
            }
        }
    ?>
    <div class="section section-border-bottom paddingB-40">
        <div class="container">
            <div class="paddingTB-40">
                <div class="row">
                    <div class="col-md-6">
                        <div class="font-30 font-ultra-bold font-lato line-1">
                            Account Information
                        </div>
                        <div class="font-18 m-top-10 m-bottom-20">
                            Update/Edit Your Account
                        </div>
                    </div>
                </div>
                <!-- ./row -->
            </div>
            <!-- ./paddingTB -->
        </div>
        <!-- ./container -->
    </div>
    <div class="section">
        <div class="bg-grey">
            <div class="container">
                <div class="row">
                     <ul id="myTab" class="nav nav-tabs custom-tab">
                        <li class="active"><a href="#accountinfoTab" > <i class="fa fa-book"></i> Account Information</a></li>
                        <li class=""><a href="#passwordTab"><i class="fa fa-lock"></i> Password</a></li>
                    </ul>

                    <div id="myTabContent" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="accountinfoTab" aria-labelledBy="accountinfoTab">
                            <div class="padding-10 paddingTB-40 m-bottom-20">
                                <div class="font-lato font-30 font-ultra-bold">Account Information</div>
                                <div class="font-16 m-top-5">Change your basic account settings and location.</div>
                                <form class="form-horizontal" id="EmployerAccount" name="EmployerAccount" action="/EmployerAccount" method="post" data-parsley-validate enctype="multipart/form-data">
                                    <div class="col-lg-9">
                                        <input type="hidden" name="cmd" value="clinet-account-detail">
                                        <div class="form-group m-top-40">
                                            <label for="name" class="col-lg-12 col-sm-12 control-label font-normal font-14 text-left">Company Name:</label>
                                            <div class="col-sm-12 col-md-12">
                                                <input type="text" class="form-control" id="compname" name="compname" required data-parsley-group="block-account" value='<?php if(isset($Employer) && !empty($Employer)) {echo ($Employer->e_companyname); }?>'>
                                            </div>
                                        </div>
                                        <div class="form-group m-top-10">
                                            <label for="name" class="col-lg-12 col-sm-12 control-label font-normal font-14 text-left">Description:</label>
                                            <div class="col-sm-12 col-md-12">
                                                <textarea id="desc" name="desc" class="form-control leftchar" data-parsley-length="[100, 3000]" rows="12" data-parsley-group="block1" required data-parsley-rangelength="[50,3000]" data-parsley-minlength="50" data-parsley-maxlength="3000" maxlength="3000"><?php echo ($Employer->e_description); ?>
                                                </textarea>
                                                <div class="col-md-12 text-right">
                                                    <span class="left">3000</span>characters left</div>
                                                <p class="block text-muted font-12 m-top-10">Outsource is built on trust. Help clients get to know you and your professional background.</p>
                                            </div>
                                        </div>
                                        <div class="form-group m-top-md">
                                            <label for="FirstName" class="col-lg-12 col-sm-12 control-label font-normal font-14 text-left">Email Address;</label>
                                            <div class="col-sm-12 col-md-12">
                                                <input type="text" class="form-control" id="email" name="email" value='<?php if(isset($Employer) && !empty($Employer)) { echo ($Employer->u_email); }?>' required data-parsley-group="block-account">
                                                <p class="help-block text-normal font-12 no-margin-bottom">Your email will never be shared or displayed publicly.</p>
                                            </div>
                                        </div>
                                        <div class="form-group m-top-md">
                                            <label for="Website" class="col-lg-12 col-sm-12 control-label font-normal font-14 text-left">Website:</label>
                                            <div class="col-sm-12 col-md-12">
                                                <input type="text" class="form-control" id="website" name="website" value='<?php if(isset($Employer) && !empty($Employer)) { echo ($Employer->u_website); }?>' required data-parsley-group="block-account">
                                            </div>
                                        </div>
                                        <div class="form-group m-top-10">
                                        <label for="address" class="col-lg-12 col-sm-12 control-label font-normal font-14 text-left">Address:</label>
                                        <div class="col-sm-12 col-md-12">
                                            <input type="text" class="form-control" id="address" name="address" value="<?php if(isset($Employer) && !empty($Employer)) { echo $Employer->u_address; } ?>" required data-parsley-group="block-account">
                                        </div>
                                    </div>
                                    <div class="form-group m-top-10">
                                        <label for="stateInput" class="col-lg-12 col-sm-12 control-label font-normal font-14 text-left">State / Province:</label>
                                        <div class="col-sm-12 col-md-12">
                                            <input type="text" class="form-control" id="stateInput" name="stateInput" value="<?php if(isset($Employer) && !empty($Employer)) { echo $Employer->u_province; } ?>" required data-parsley-group="block-account">
                                        </div>
                                    </div>
                                    <div class="form-group m-top-10">
                                        <label for="provinceInput" class="col-lg-12 col-sm-12 control-label font-normal font-14 text-left">City:</label>
                                        <div class="col-sm-12 col-md-12">
                                            <select name="city" id="city" class="form-control">
                                                <?php
                                                    if (isset($joblocation2['location2']) && !empty($joblocation2['location2'])){
                                                        foreach ($joblocation2['location2'] as $location) { ?>
                                                            <optgroup label="<?php echo $location['region']; ?>">
                                                                <?php
                                                                    if (isset($location['Town_Cities']) && !empty($location['Town_Cities'])){
                                                                        foreach ($location['Town_Cities'] as $item) {
                                                                            if ($item['locationname'] == $Employer->u_city){
                                                                                echo '<option selected="selected" value="'.$item['locationname'].'">
                                                                                    '.$item['locationname'].'
                                                                                </option>';
                                                                            }else{
                                                                                echo '<option value="'.$item['locationname'].'">
                                                                                    '.$item['locationname'].'
                                                                                </option>';
                                                                            }
                                                                        }
                                                                    }
                                                                ?>
                                                            </optgroup>';
                                                <?php
                                                        }
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group m-top-10">
                                        <label for="age" class="col-lg-12 col-sm-12 control-label font-normal font-14 text-left">Zip Code:</label>
                                        <div class="col-sm-12 col-md-12">
                                            <input type="text" class="form-control" id="zipCode" name="zipCode" value="<?php if(isset($Employer) && !empty($Employer)) { echo $Employer->u_zipcode; } ?>" required data-parsley-group="block-account">
                                        </div>
                                    </div>
                                         <div class="form-group m-top-10">
                                            <label for="address" class="col-lg-12 col-sm-12 control-label font-normal font-14 text-left">Landline:</label>
                                            <div class="col-sm-12 col-md-12">
                                                <input type="text" class="form-control" id="telephone" name="telephone" value='<?php if(isset($Employer) && !empty($Employer)) { echo ($Employer->u_telephoneno); }?>'>
                                            </div>
                                        </div>
                                        <div class="form-group m-top-10">
                                            <label for="address" class="col-lg-12 col-sm-12 control-label font-normal font-14 text-left">Mobile:</label>
                                            <div class="col-sm-12 col-md-12">
                                                <input type="text" class="form-control" id="mobile" name="mobile" value='<?php if(isset($Employer) && !empty($Employer)) { echo ($Employer->u_mobileno); }?>'>
                                            </div>
                                        </div>
                                        <div class="text-left m-top-30">
                                            <button class="btn btn-info m-right-10 btn-savechange" type="submit">Save Changes</button>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label for="professionalTitle" class="col-lg-5 col-sm-5 control-label font-normal font-14 text-left">Add Photo :</label>
                                            <div class="col-sm-12 col-lg-12">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="clearfix">
                                                        <a href="#" class="thumbnail inline-block pull-left m-right-md" style="margin-bottom:0;">
                                                            <?php if(isset($Employer) && !empty($Employer) && !empty($Employer->u_displaypicture)) { ?>
                                                                <img src="../images/employer/<?php echo $Employer->u_displaypicture; ?>" alt="..." class="img-rounded" style="max-width: 143px; max-height: 143px;">
                                                            <?php }else{ ?>
                                                                <img src="../img/photo.jpg" alt="..." class="img-rounded" style="max-width: 143px; max-height: 143px;">
                                                            <?php } ?>
                                                        </a>
                                                    </div>
                                                    <div class="pull-left m-right-md m-top-10">
                                                        <a class="btn btn-primary m-left-sm" id="upload-profile-photo">Upload</a>
                                                        <a class="btn btn-danger m-left-sm">Delete</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="passwordTab" aria-labelledBy="passwordTab">
                            <div class="padding-10 paddingTB-40 m-bottom-20">
                                <div class="font-lato font-30 font-ultra-bold">Password</div>
                                <div class="font-16 m-top-5">Change your password or request a new one.</div>
                                <form class="form-horizontal" id="passwordchange" data-parsley-validate>
                                    <input type="hidden" name="cmd" value="_do-password-change">
                                    <div class="form-group m-top-40">
                                        <label for="password" class="control-label col-sm-3 font-semi-bold">Current Password</label>
                                        <div class="col-sm-9 col-md-7 has-icon-alert">
                                            <span class="form-check relative"><i class="fa fa-check text-dark-green tick_absolute"></i>
                                            </span>
                                            <input type="password" class="form-control" id="password" name="password" required data-parsley-group="block-account">
                                        </div>
                                    </div>
                                    <div class="form-group m-top-20">
                                        <label for="newPasswordInput" class="control-label col-sm-3 font-semi-bold">New Password</label>
                                        <div class="col-sm-9 col-md-7 has-icon-alert">
                                            <span class="form-check relative"><i class="fa fa-check text-dark-green tick_absolute"></i>
                                            </span>
                                            <input type="password" class="form-control" id="newPasswordInput" name="newPasswordInput" required data-parsley-group="block-account">
                                        </div>
                                    </div>
                                    <div class="form-group m-top-20">
                                        <label for="verifyPasswordInput" class="control-label col-sm-3 font-semi-bold">Verify Password</label>
                                        <div class="col-sm-9 col-md-7 has-icon-alert">
                                            <span class="form-check relative"><i class="fa fa-check text-dark-green tick_absolute"></i>
                                            </span>
                                            <input type="password" class="form-control" id="verifyPasswordInput" name="verifyPasswordInput" required data-parsley-group="block-account">
                                        </div>
                                    </div>
                                    <div class="form-group m-top-30">
                                        <label class="control-label col-sm-3"></label>
                                        <div class="col-sm-9 col-md-7">
                                            <button class="btn btn-info m-left-20" type="button" onclick="saveUserPassword()">Save Changes</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ./row -->
            </div>
        </div>
    </div>
</div>
