<div class="main-container">
    <div class="container" id="step1">
        <div class="clearfix">
            <div class="pull-right text-right m-top-20">
                Are you an Applicant?
                <a href="/join" class="block">Create an account here<i class="fa fa-angle-double-right text-normal font-12 m-left-5"></i></a>
            </div>
        </div>

        <div class="signup-widget">
            <div class="text-center">
                <div class="font-30 font-lato">Recover
                    <span class="font-ultra-bold">My Password</span>
                </div>
                <div class="font-16">Let's get started!</div>

                <p class="m-top-15">We will send you an email containing instructions on how you can retrieve your password. Please enter the login email address associated with your account.</p>
            </div>

            <div class="row m-top-15">
                <form class="form-horizontal autosubmit" data-parsley-validate id="recoverpassword">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="form-group has-icon-alert">
                            <label class="font-normal">Email:
                                <span class="form-check"><i class="fa fa-check text-dark-green"></i>
                                </span>
                            </label>
                            <input type="email" class="form-control" name="email" data-parsley-group="block1" required value="">
                            <strong class="error_recovery" style="color:red"></strong>
                        </div>
                    </div>
                </form>
            </div>

            <div class="m-top-30 text-center">
                <a href="javascript:void(0);" id="sendemail" class="btn btn-lg btn-primary btn-wide autosubmitform">Send Email <i class="fa fa-angle-double-right font-12"></i></a>
            </div>

        </div>
    </div>

    <div class="container paddingTB-lg" id="step2">
        <div class="row">
            <div class="col-sm-7">
                <div class="h4 no-margin">Email Sent Successfully</div>

                <p class="m-top-md">An email has been sent to the email address provided. Please check your spam folder if you do not see it. If you have any final questions or comments, please contact us at
                    <strong>support@mindanaojobs.com</strong>
                    <strong class="error_recovery"></strong>
                </p>

            </div>
        </div>
    </div>

</div>
