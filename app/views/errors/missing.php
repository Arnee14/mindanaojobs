<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
  <meta charset="utf-8">
  <title>Hire and Find Jobs Today | MindanaoJobs.com</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="keywords" content="" />
  <meta name="author" content="">


  <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />

  <!-- Bootstrap core CSS -->
  <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
  <!-- Font Awesome -->
  <link href="bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet">

  <style type="text/css">
  	@import url(//fonts.googleapis.com/css?family=Lato:700);

	body {
		margin:0;
		font-family:'Lato', sans-serif;
		text-align:center;
		color: #2c3e50;
		padding-top: 20px;
  		padding-bottom: 20px;
	}

	.jumbotron {
	  text-align: center;
	  border-bottom: 1px solid #e5e5e5;
	}
	.jumbotron .btn {
	  padding: 14px 24px;
	  font-size: 21px;
	}

	.jumbotron h1 {
		font-size: 120px;
		font-weight: 500;
		margin: 16px 0 0 0;
	}
	.jumbotron h2 {
		font-size: 40px;
		font-weight: 500;
		margin: 16px 0 0 0;
	}
  </style>

</head>
<body class="overflow-hidden">
	 <div class="container">
		<div class="jumbotron">
	        <h1>404</h1>
	        <h4>PAGE NOT FOUND</h4>
	        <br>
	        <h2>OH MY GOSH! YOU FOUND IT !!!</h2>
	        <br>
	        <p class="lead">
	        	Looks like the page you're trying to visit doesnt exist.<br>
	        	Please check the URL and try your luck again.
	        </p>
	        <br>
	        <p><a class="btn btn-lg btn-danger" href="/" role="button">Take Me Home</a></p>
	    </div>
	</div>

	<!-- Jquery -->
  	<script type="text/javascript" src="bower_components/jquery/dist/jquery.min.js"></script>
  	<!-- Bootstrap -->
  	<script type="text/javascript" src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
</body>
</html>
