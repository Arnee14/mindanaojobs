<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


// ==============================================
//              PAGE ROUTING
// ==============================================

// Route::get('/hello', 'MiscController@showWelcome');
// Route::get('/email', 'MiscController@showEmail');

Route::get('/404', 'MiscController@show404');

Route::get('/', 'MiscController@showHome');
Route::get('/homepage', 'MiscController@showHome');


// ==============================================
//              JOB SEEKER ROUTING
// ==============================================

Route::get('/login', 'OauthController@jobseekerlogin')->before('guest');
Route::post('/login', 'OauthController@loginAccount');

Route::get('/profile', 'UserController@profile')->before('auth');
Route::get('/my-profile', 'UserController@my_profile')->before('auth');
Route::get('/account', 'UserController@account')->before('auth');
Route::get('/messages', 'UserController@jobseekerMessage')->before('auth');
Route::get('/notifications', 'UserController@jobseekerNotification')->before('auth');
Route::get('/confirm','OauthController@confirmAccount')->before('auth');
Route::post('/reset','OauthController@resetAccount');


Route::group(array('prefix' => 'api'), function()
{
    // ==============================================
    //              JOB SEEKER API ROUTES
    // ==============================================

    Route::post('/users', 'OauthController@signUpUser');
    Route::put('/users', 'UserController@updateUser')->before('auth');

    Route::post('/uploadpic', 'UserController@uploadPic')->before('auth');
    Route::delete('/uploadpic', 'UserController@deleteUploadPic')->before('auth');

    Route::post('/uploadresume', 'UserController@uploadResume')->before('auth');
    Route::get('/uploadresume/{file}', 'UserController@getuploadResume')->before('auth');

    Route::put('/jobseeker', 'UserController@updateJobseeker')->before('auth');
    Route::post('/jobseeker', 'UserController@saveJobseeker')->before('auth');
    Route::get('/jobseeker', 'UserController@getJobSeekerSkills')->before('auth');

    Route::get('/joblocation','MiscController@getAllJobLocation')->before('block');
    Route::get('/joblocation2', 'MiscController@getAllJobLocation2')->before('block');
    Route::get('/jobcategories', 'MiscController@getAllJobCategories')->before('block');
    Route::get('/skills', 'MiscController@getAllSkills')->before('block');
    Route::get('/allmonths', 'MiscController@getAllMonths')->before('block');
    Route::get('/findSkills', 'MiscController@findAllSkills')->before('block');

});

// ==============================================
//              EMPLOYER ROUTING
// ==============================================
Route::get('/Employer', 'OauthController@employerlogin')->before('guest');
Route::get('/EmployerProfile', 'EmployerController@employerProfile')->before('authEmp');
Route::get('/CompanyProfile', 'EmployerController@showCompanyProfile')->before('authEmp');
Route::get('/EmployerAccount', 'EmployerController@showEmployerAccount')->before('authEmp');
Route::get('/MyJobs', 'EmployerController@employerJobList')->before('authEmp');
Route::get('/joblist', 'EmployerController@showRecentJobList');
Route::get('/jobpost', 'EmployerController@showJobPost')->before('authEmp');
Route::get('/jobdetail', 'EmployerController@showJobDetail')->before('auth|authEmp');

// Route::get('/jobPost-edit', 'EmployerController@showJobPost')->before('authEmp');

Route::post('/Employer', 'OauthController@EmployerAuth');
Route::post('/EmployerAccount', 'EmployerController@updateEmployer');
Route::post('/jobpost', 'JobController@CreateJob');
Route::put('/jobpost', 'JobController@UpdateJob');
Route::post('/EmployerForm', 'EmployerController@saveEmployer');
Route::post('/uploadpic', 'EmployerController@uploadPic');
Route::delete('/delete', 'JobController@JobDelete');

Route::put('/EmployerPassword', 'EmployerController@updateEmpPass');



Route::group(array('prefix' => 'jobs'), function()
{
    // ==============================================
    //              JOBS API ROUTES
    // ==============================================
    Route::get('/category', 'JobController@showJobListByCategory')->before('authEmp');
    Route::get('/search', 'JobController@searchJob')->before('authEmp');
    Route::get('/Empsearch', 'JobController@searchJobForEmployee')->before('authEmp');
    Route::get('/filterplace', 'JobController@filterByPlace')->before('authEmp');

    Route::get('/jobskills', 'JobController@getJobSkills')->before('authEmp');
    Route::get('/jobcategories', 'JobController@getJobCategories')->before('authEmp');
    Route::get('/create','EmployerController@checkIfEmployerIsVerified')->before('authEmp');

});



// ==============================================
//              API ROUTING
// ==============================================

Route::get('/logout', 'MiscController@accountLogout')->before('auth');
Route::get('/joinoption', 'MiscController@showJoinOption');
Route::get('/join', 'MiscController@showJoin');
Route::get('/EmployerForm', 'MiscController@showEmployerJoin')->before('guest');
Route::get('/recovery', 'MiscController@showPasswordRecovery');
Route::get('/aboutus', 'MiscController@showAboutUs');
Route::get('/privacy', 'MiscController@showPrivacy');
Route::get('/terms', 'MiscController@showTerms');



Route::group(array('prefix'=>'api/email'),function(){
    // ==============================================
    //              EMAIL ROUTING
    // ==============================================

/*    Route::get('/contact', 'MiscController@sendMail');
    Route::get('/subscription', 'MiscController@subscription');*/

    Route::post('/recover', 'OauthController@recoverPassword');
    Route::post('/resendconfirm', 'OauthController@resendConfirm');
    Route::post('/applyJob', 'JobController@applyJob');
});


// ==============================================
//              SOCIAL ROUTING
// ==============================================

Route::get('/google', 'SocialMediaController@loginWithGoogle')->before('guest');
Route::get('/facebook', 'SocialMediaController@loginWithFacebook')->before('guest');;
Route::get('/linkedIn', 'SocialMediaController@loginWithLinkedin')->before('guest');;






Route::group(array('prefix' => 'api/jobseeker/v1'), function()
{
    Route::post('/logout', 'AuthAPIController@accountLogout');

    Route::post('/login', 'AuthAPIController@loginAccount');
    Route::post('/signup', 'RegisterAPIController@signUpUser');

});

Route::group(array('prefix' => 'api/employer/v1'), function()
{
    Route::post('/logout', 'AuthAPIController@accountLogout');

    Route::post('/login', 'AuthAPIController@EmployerAuth');
    Route::post('/signup', 'RegisterAPIController@saveEmployer');
});
