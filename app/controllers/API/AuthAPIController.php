<?php
require 'vendor/autoload.php';
use GuzzleHttp\Client;

class AuthAPIController extends BaseController {

	public function accountLogout(){
		$value = Auth::logout();
		return Response::json($value);
	}

	public function loginAccount(){

		$data = Input::all();

		$userdata = array(
			'u_email' 	=> Input::get('username'),
			'password' => Input::get('password'),
			'isactive' => '1',
			'u_accounttype'=>'local'
		);
		if (Auth::attempt($userdata)) {
			if (Auth::user()->u_classification == 'jobseeker'){
				return Response::json(Auth::user());
			}else{
				Auth::logout();
				$error['title']='Oh snap! You got an error!';
				$error['message'] = 'Your username/password combination was incorrect.';
				return Response::json($error);
			}
		} else {
			$error['title']='Oh snap! You got an error!';
			$error['message'] = 'Your username/password combinationsss was incorrect.';
			return Response::json($error);
		}
	}



	public function EmployerAuth() {
		$data = Input::all();
		$EmployerData = array(
			'u_email'	=>	Input::get('username'),
			'password'	=>	Input::get('password'),
		 	'isactive'	=> '1'
		);

		if(Auth::attempt($EmployerData)) {
			if (Auth::user()->u_classification == 'employer'){
				return Response::json(Auth::user());
			}else{
				Auth::logout();
				$error['title']='Oh snap! You got an error!';
				$error['message'] = 'Your username/password combination was incorrect.';
				return Response::json($error);
			}
		} else {
			$error['title']='Oh snap! You got an error!';
			$error['message'] = 'Your username/password combination was incorrect.';
			return Response::json($error);
		}
	}



}
