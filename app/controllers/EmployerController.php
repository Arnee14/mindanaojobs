<?php
require 'vendor/autoload.php';
use GuzzleHttp\Client;

class EmployerController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public $base_url = 'http://localhost:3000';


	public function employerProfile(){

		$javascript = array('js/mindanaojobs.js','js/jobs.js');
		$css = array('css/style.css','css/flatui.css','css/parsley.css','css/animation.css');

		$id = Auth::id();

		$currentUser = User::getCurrentUser(array($id));
		if (isset($currentUser) && !empty($currentUser)){
			$data['User'] = $currentUser[0];
		}

		$currentEmployer = Employer::getCurrentEmployer(array($id));

		$pageLimit = 10;
		$page = Input::get('page', 1);
		$page = ($page - 1) * $pageLimit;

		$currentJob = Job::getMyJobs(array($id),array($id,$page,$pageLimit));

		$arrDay = array();
		foreach ($currentJob['paging'] as $key ) {


			$newData = array();
			$skilssarr = array();
			$categoryArr = array();

			foreach ($currentJob['skills'] as $row ) {
				if($key->j_id == $row->j_id){
					array_push($skilssarr,array('skills'=> $row));
				}
			}
			foreach ($currentJob['category'] as $row ) {
				if($key->j_id == $row->j_id){
					array_push($categoryArr,array('category'=> $row));
				}
			}

			$newData['categories'] = $categoryArr;
			$newData['skills'] = $skilssarr;
			$newData['info'] = array('data'=> $key);



			array_push($arrDay,$newData);
		}

		$paginator = Paginator::make($arrDay, count($currentJob['info']), $pageLimit );

        $data = [
			'page_js' 	=>	$javascript,
			'page_css' 	=>	$css,
			'Employer'  =>	$currentEmployer[0],
			'Employers'	=>	$currentEmployer[0],
			'Job'		=>	$paginator
		];

		View::share($data);
		return View::make('index')
			->nest('header', 'include.header-profile')
			->nest('main', 'page.employerprofile')
			->nest('footer', 'include.footer');
	}

	public function showEmployerAccount(){
		$javascript = array('js/mindanaojobs.js','js/employer_account.js','js/ajaxupload.js','js/jquery.fileDownload.js','js/jobs.js');
		$css = array('css/style.css','css/flatui.css','css/parsley.css','css/animation.css');

		$id = Auth::id();
		$currentEmployer = Employer::getCurrentEmployer(array($id));
		$Empid = $currentEmployer[0];

		$file2 = file_get_contents('public/json/location2.json');
		$joblocation2 = json_decode($file2,TRUE);

		$error = Session::get('error');

        $data = [
			'page_js' =>$javascript,
			'page_css' =>$css,
			'Employer' 	=>	$currentEmployer[0],
			'joblocation2' => $joblocation2,
			'error' =>$error
		];

		View::share($data);
		return View::make('index')
			->nest('header', 'include.header-profile')
			->nest('main', 'page.employerAccountInfo')
			->nest('footer', 'include.footer');
	}

	public function employerJobList(){

		$searchdata = Input::get('search_name');
		$j_id = Input::get( 'id' );
		$param = Input::get( 'param' );
		$input = '%'.$searchdata.'%';

		$pageLimit = 10;
		$page = Input::get('page', 1);
		$page = ($page - 1) * $pageLimit;


		if (isset($j_id) && !empty($j_id)){

			if ($param == 'edit'){

				$javascript = array('js/mindanaojobs.js','js/jobapply.js');
				$css = array('css/style.css','css/flatui.css','css/parsley.css','css/animation.css');
				$basis = array('Full-time','Part-time','Contract');

				$file = file_get_contents('public/json/jobcategories.json');
				$jobcategories = json_decode($file,TRUE);

				$currentJob = Job::getJobDetails(array($j_id));


		        $data = [
					'page_js' =>$javascript,
					'page_css' =>$css,
					'basis' => $basis,
					'Job'=>$currentJob['info'][0],
					'jobcategories' => $jobcategories,
					'search_name' => $searchdata
				];


				$id = Auth::id();
				$currentUser = User::getCurrentUser(array($id));

				if (isset($currentUser) && !empty($currentUser)){
					$data['User'] = $currentUser[0];
				}

				$currentEmployer = Employer::getCurrentEmployer(array($id));
				if (isset($currentEmployer) && !empty($currentEmployer)){
					$data['Employer'] = $currentEmployer[0];
				}

				View::share($data);
		        return View::make('index')
					->nest('header', 'include.header-profile')
					->nest('main', 'page.job-post')
					->nest('footer', 'include.footer');


			}elseif ($param == 'view') {

				$javascript = array('js/mindanaojobs.js');
				$css = array('css/style.css','css/flatui.css','css/parsley.css','css/animation.css');

				$id = Auth::id();
				$currentEmployer = Employer::getCurrentEmployer(array($id));

				$currentJob = Job::getJobDetails(array($j_id));

		        $data = [
					'page_js' =>$javascript,
					'page_css' =>$css,
					'Job'=>$currentJob['info'][0],
					'Categories'=>$currentJob['category'],
					'Skills'=>$currentJob['skills'],
					'search_name' => $searchdata
				];

				if (isset($currentEmployer) && !empty($currentEmployer)){
					$data['Employer'] = $currentEmployer[0];
				}

				View::share($data);
				return View::make('index')
					->nest('header', 'include.header-profile')
					->nest('main', 'page.empjobDetail')
					->nest('footer', 'include.footer');
			}

		}elseif(isset($searchdata) && !empty($searchdata)){

			// print_r($input);
			// die();
			$javascript = array('js/mindanaojobs.js');
			$css = array('css/style.css','css/flatui.css','css/parsley.css','css/animation.css');

			$id = Auth::id();
			$currentEmployer = Employer::getCurrentEmployer(array($id));

			$currentJob = Job::getSearchMyJobs(array($id,$input),array($id,$input,$page,$pageLimit));

	        $arrDay = array();
			foreach ($currentJob['paging'] as $key ) {


				$newData = array();
				$skilssarr = array();
				$categoryArr = array();

				foreach ($currentJob['skills'] as $row ) {
					if($key->j_id == $row->j_id){
						array_push($skilssarr,array('skills'=> $row));
					}
				}
				foreach ($currentJob['category'] as $row ) {
					if($key->j_id == $row->j_id){
						array_push($categoryArr,array('category'=> $row));
					}
				}

				$newData['categories'] = $categoryArr;
				$newData['skills'] = $skilssarr;
				$newData['info'] = array('data'=> $key);


				array_push($arrDay,$newData);
			}

			$paginator = Paginator::make($arrDay, count($currentJob['info']), $pageLimit );

	        $data = [
				'page_js' 	=>	$javascript,
				'page_css' 	=>	$css,
				'Employer'  =>	$currentEmployer[0],
				'Employers'	=>	$currentEmployer[0],
				'search_name' => $searchdata,
				'Jobs'		=>	$paginator
			];


			View::share($data);
	        return View::make('index')
				->nest('header', 'include.header-profile')
				->nest('main', 'page.employerJobList')
				->nest('footer', 'include.footer');
		}else{
			$javascript = array('js/mindanaojobs.js','js/jobs.js');
			$css = array('css/style.css','css/flatui.css','css/parsley.css','css/animation.css');

			if (Auth::check()) {

				$id = Auth::id();
				$currentUser = User::getCurrentUser(array($id));

				if (isset($currentUser) && !empty($currentUser)){
					$data['User'] = $currentUser[0];
				}

				$currentEmployer = Employer::getCurrentEmployer(array($id));
				if (isset($currentEmployer) && !empty($currentEmployer)){
					$data['Employer'] = $currentEmployer[0];
				}

				$currentJob = Job::getMyJobs(array($id),array($id,$page,$pageLimit ));

				$arrDay = array();
				foreach ($currentJob['paging'] as $key ) {

					$newData = array();
					$skilssarr = array();
					$categoryArr = array();

					foreach ($currentJob['skills'] as $row ) {
							if($key->j_id == $row->j_id){
								array_push($skilssarr,array('skills'=> $row));
							}
					}
					foreach ($currentJob['category'] as $row ) {
						if($key->j_id == $row->j_id){
							array_push($categoryArr,array('category'=> $row));
						}
					}

					$newData['categories'] = $categoryArr;
					$newData['skills'] = $skilssarr;
					$newData['info'] = array('data'=> $key);



					array_push($arrDay,$newData);
				}

				$paginator = Paginator::make($arrDay, count($currentJob['info']), $pageLimit );

		        $data = [
					'page_js' 	=>	$javascript,
					'page_css' 	=>	$css,
					'Employer'  =>	$currentEmployer[0],
					'Employers'	=>	$currentEmployer[0],
					'search_name' => $searchdata,
					'Jobs'		=>	$paginator
				];

				View::share($data);
		        return View::make('index')
					->nest('header', 'include.header-profile')
					->nest('main', 'page.employerJobList')
					->nest('footer', 'include.footer');

		    }else{
		    	View::share($data);
		    	return View::make('index')
					->nest('header', 'include.header-login')
					->nest('main', 'page.employerJobList')
					->nest('footer', 'include.footer');
		    }
		}

	}

	public function showJobDetail(){
		$javascript = array('js/mindanaojobs.js','js/jobapply.js');
		$css = array('css/style.css','css/flatui.css','css/parsley.css','css/animation.css');

		$j_id = Input::get( 'id' );

		if (Auth::check()) {

			if (isset($j_id) && !empty($j_id)){

				$id = Auth::id();
				$currentUser = User::getCurrentUser(array($id));
				$currentEmployer = Employer::getCurrentEmployer(array($id));

				$currentJob = Job::getJobDetails(array($j_id));
				$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

		        $data = [
					'page_js' =>$javascript,
					'page_css' =>$css,
					'Job'=>$currentJob['info'][0],
					'Categories'=>$currentJob['category'],
					'Skills'=>$currentJob['skills'],
					'Share'=> Share::load($actual_link, $currentJob['info'][0]->j_title)->services('facebook', 'gplus', 'linkedin','twitter'),
				];

				if (isset($currentUser) && !empty($currentUser)){
					$data['User'] = $currentUser[0];
				}

				if (isset($currentEmployer) && !empty($currentEmployer)){
					$data['Employer'] = $currentEmployer[0];
				}

				View::share($data);
				return View::make('index')
					->nest('header', 'include.header-profile')
					->nest('main', 'page.jobdetail')
					->nest('footer', 'include.footer');

			}else{
				return Redirect::to('/joblist');
			}
	    }else{
	    	return Redirect::to('/login');
	    }
	}

	public function showRecentJobList(){
		$javascript = array('js/mindanaojobs.js');
		$css = array('css/style.css','css/flatui.css','css/parsley.css','css/animation.css');


		$file = file_get_contents('public/json/jobcategories.json');
		$jobcategories = json_decode($file,TRUE);

		$file2 = file_get_contents('public/json/location.json');
		$joblocation = json_decode($file2,TRUE);

		$pageLimit = 20;
		$page = Input::get('page', 1);
		$page = ($page - 1) * $pageLimit;

		$search_name = Input::get('search_name');
		$places = Input::get( 'places' );
		$categories = Input::get( 'categories' );
		$type = Input::get( 'type' );

        $data = [
			'page_js' =>$javascript,
			'page_css' =>$css,
			'joblocation' => $joblocation,
			'jobcategories' => $jobcategories
		];

		$AllJobs = Job::getAllJobs(array($search_name,$places,$categories,$type),array($page,$pageLimit));

		if (Auth::check()) {

			$id = Auth::id();
			$currentUser = User::getCurrentUser(array($id));

			if (isset($currentUser) && !empty($currentUser)){
				$data['User'] = $currentUser[0];
			}

			$currentEmployer = Employer::getCurrentEmployer(array($id));
			if (isset($currentEmployer) && !empty($currentEmployer)){
				$data['Employer'] = $currentEmployer[0];
			}

			$newData = array();
			$arrDay = [];
			foreach ($AllJobs['paging'] as $key ) {
				$skilssarr = array();
				$categoryArr = array();

				foreach ($AllJobs['category'] as $row ) {
					if($key->j_id == $row->j_id){
						array_push($categoryArr,array('category'=> $row));
					}
				}
				foreach ($AllJobs['skills'] as $row ) {
					if($key->j_id == $row->j_id){
						array_push($skilssarr,array('skills'=> $row));
					}
				}
				$newData['categories'] = $categoryArr;
				$newData['skills'] = $skilssarr;
				$newData['info'] = array('data'=> $key);
				array_push($arrDay,$newData);
			}

			$paginator = Paginator::make($arrDay, count($AllJobs['info']), $pageLimit );

			if (isset($AllJobs) && !empty($AllJobs)){
				$data['Jobs'] = $paginator;
			}
			$data['search_name'] = $search_name;
			$data['places'] = $places;
			$data['categories'] = $categories;
			$data['type'] = $type;

			View::share($data);
	        return View::make('index')
				->nest('header', 'include.header-profile')
				->nest('main', 'page.joblist')
				->nest('footer', 'include.footer');

	    }else{
	    	$newData = array();
			$arrDay = [];
			foreach ($AllJobs['paging'] as $key ) {
				$skilssarr = array();
				$categoryArr = array();

				foreach ($AllJobs['category'] as $row ) {
					if($key->j_id == $row->j_id){
						array_push($categoryArr,array('category'=> $row));
					}
				}
				foreach ($AllJobs['skills'] as $row ) {
					if($key->j_id == $row->j_id){
						array_push($skilssarr,array('skills'=> $row));
					}
				}
				$newData['categories'] = $categoryArr;
				$newData['skills'] = $skilssarr;
				$newData['info'] = array('data'=> $key);
				array_push($arrDay,$newData);
			}

			$paginator = Paginator::make($arrDay, count($AllJobs['info']), $pageLimit );

			if (isset($AllJobs) && !empty($AllJobs)){
				$data['Jobs'] = $paginator;
			}

			$data['search_name'] = $search_name;
			$data['places'] = $places;
			$data['categories'] = $categories;
			$data['type'] = $type;

	    	View::share($data);
	    	return View::make('index')
				->nest('header', 'include.header-login')
				->nest('main', 'page.joblist')
				->nest('footer', 'include.footer');
	    }

	}

	public function showJobPost(){
		$javascript = array('js/mindanaojobs.js','js/jobs.js');
		$css = array('css/style.css','css/flatui.css','css/parsley.css','css/animation.css');
		$basis = array('Full-time','Part-time','Contract');

		$file = file_get_contents('public/json/jobcategories.json');
		$jobcategories = json_decode($file,TRUE);

        $data = [
			'page_js' =>$javascript,
			'page_css' =>$css,
			'basis' => $basis,
			'jobcategories' => $jobcategories
		];

		if (Auth::check()) {

			$id = Auth::id();
			$currentUser = User::getCurrentUser(array($id));

			if (isset($currentUser) && !empty($currentUser)){
				$data['User'] = $currentUser[0];
			}

			$currentEmployer = Employer::getCurrentEmployer(array($id));
			if (isset($currentEmployer) && !empty($currentEmployer)){
				$data['Employer'] = $currentEmployer[0];
			}

			View::share($data);
	        return View::make('index')
				->nest('header', 'include.header-profile')
				->nest('main', 'page.job-post')
				->nest('footer', 'include.footer');

	    }else{
	    	View::share($data);
	    	return View::make('index')
				->nest('header', 'include.header-login')
				->nest('main', 'page.job-post')
				->nest('footer', 'include.footer');
	    }
	}

	public function showCompanyProfile(){
		$javascript = array('../js/mindanaojobs.js');
		$css = array('../css/style.css','css/flatui.css','../css/parsley.css','../css/animation.css');

        $data = [
            'page_js' 	=>	$javascript,
            'page_css' 	=>	$css
        ];

        $u_id =Session::get('u_id');
        $e_id =Session::get('e_id');

        if (Auth::check()) {

        	$u_id = Input::get( 'u_id' );
        	$e_id = Input::get( 'e_id' );
        	$page = Input::get('page', 1);

            if( (isset($u_id )  && isset($e_id )) && (!empty($u_id) && !empty($e_id))){

                    $currentEmployer = Employer::getCurrentEmployer(array($u_id));
                    $currentEmployer = $currentEmployer[0];

                    $pageLimit = 10;
                    $currentJob = Job::getMyJobs(array($u_id),array($u_id,$page,$pageLimit ));

                    $newData = array();
					$arrDay = [];
					foreach ($currentJob['paging'] as $key ) {
						$skilssarr = array();
						$categoryArr = array();

						foreach ($currentJob['skills'] as $row ) {
							if($key->j_id == $row->j_id){
								array_push($skilssarr,array('skills'=> $row));
							}
						}
						foreach ($currentJob['category'] as $row ) {
							if($key->j_id == $row->j_id){
								array_push($categoryArr,array('category'=> $row));
							}
						}

						$newData['categories'] = $categoryArr;
						$newData['skills'] = $skilssarr;
						$newData['info'] = array('data'=> $key);
						array_push($arrDay,$newData);
					}

					$paginator = Paginator::make($arrDay, count($currentJob['info']), $pageLimit );

                    $data[ 'Employers' ]	=	$currentEmployer;
                    $data[ 'Job' ]	= $paginator;

                    $id = Auth::id();
                    $currentUser = User::getCurrentUser(array($id));
                    if (isset($currentUser) && !empty($currentUser)){
                        $data['User'] = $currentUser[0];
                    }


                    View::share($data);
                    return View::make('index')
                        ->nest('header', 'include.header-profile')
                        ->nest('main', 'page.employerprofile')
                        ->nest('footer', 'include.footer');

            }else{
                return Redirect::to('/joblist');
            }

        }else{


            View::share($data);
            return View::make('index')
                ->nest('header', 'include.header-login')
                ->nest('main', 'page.employerprofile')
                ->nest('footer', 'include.footer');

        }
	}


	public function saveEmployer() {
		$data = Input::all();

		if (Request::ajax()){

			if(strlen($data['employer']['password']) < 8){
				$result['success'] = 'false';
				$result['msg'] = "WARNING: Password doesn\'t match";
				return $result;
			}elseif($data['employer']['password'] != $data['employer']['password1']){
				$result['success'] = 'false';
				$result['msg'] = "WARNING: Password doesn\'t match";
				return $result;
			}else{

				$return = Employer::saveEmployer($data['employer']);
				$results = json_decode($return);

				if($results->success == 'true'){

					$userdata = array(
						'u_email' 	=>$data['employer']['email'],
						'password' => $data['employer']['password'],
						'isactive' => '1',
						'u_accounttype'=>'local'
					);

					if (Auth::attempt($userdata)) {
						$result['success'] = $results->success;
						$result['page'] = '/EmployerProfile';
						return $result;
					} else {
						$result['success'] = 'false';
						$result['msg'] = 'WARNING: Unknown error occur while logging in the account';
						$result['page'] = '/EmployerForm';
						return $result;
					}
				}elseif($results->field != ''){
						$result['field'] = $results->field;
						$result['fieldError'] =$results->fieldError;
						$result['success'] = $results->success;
						$result['msg'] =$results->msg;
						return $result;
				}else{
						$result['success'] = $results->success;
						$result['msg'] = $results->msg;
						$result['page'] = '/EmployerForm';
						return $result;
				}
			}

		}
	}

	public function updateEmployer() {
		$data = Input::all();

		$id = Auth::id();

		//validate the forms
		// $validator = Validator::make(Input::all(), $rules);

		// if($Validator->fails()){
		// 	$messages = $validator->messages();
		// 	return Redirect::to('/EmployerAccount')
		// 		->withErrors($validator);
		// }else{
			$callback = Employer::updateEmployer($id, $data);

			if($callback['success'] ==true){
				return Redirect::to('/EmployerProfile');
			}else{
				$error['title']='Oh snap! You got an error!';
				$error['message']=$callback['msg'];
				return Redirect::to('/EmployerAccount')->with('error',$error);
			}

		// }
	}

	public function updateEmpPass() {
		$data = Input::all();

		if (Request::ajax()){
			$id = Auth::id();
			$callback = Employer::updateEmployerPassword($id,$data['employer']);
			return $callback;
		}
	}

	function uploadPic(){
		$file = Input::file('qqfile');
		$id = Auth::id();
		$callback = Employer::uploadPic($id,$file);
		return $callback;
	}

	public function checkIfEmployerIsVerified(){
		if (Request::ajax()){
			$id = Auth::id();
			$currentEmployer = Employer::getCurrentEmployer(array($id));
			$currentEmployer= $currentEmployer[0];

			$check = DB::select('SELECT * FROM users u WHERE u.u_id = ? AND u.u_isverified = "1" LIMIT 1;',array($currentEmployer->u_id));
			if(isset($check) && !empty($check)){
				$result['success'] = 'true';
				$result['msg'] = '';
			}else{
				$result['success'] = 'false';
				$result['msg'] = 'Unable to Create Job when account is not yet verified. Please verify to continue this process';
			}
			return Response::json($result);
		}
	}


}
