<div class="main-container">
    <?php if(isset($Employer) && !empty($Employer)){
        if ($Employer->u_isverified != '1'){
    ?>
    <div class="section bg-danger paddingTB-10 alert-top section-border-bottom">
        <div class="container">
          <div class="row">
            <div class="col-sm-7">
                <i class="pull-left m-right-15 m-top-5 no-margin-xs fa fa-3x fa-exclamation-triangle text-danger"></i>
                <div class="font-21 font-semi-bold title-text"> Your account is currently disabled. </div>
                <div class="font-lato information-text">
                    <span class="font-14 font-semi-bold">Check your email to verify your account. <a href="javascript:resendConfirmation()">Click here</a></span> to resend confirmation
                </div>
            </div>
          </div>
        </div>
    </div>
    <?php
            }
        }
    ?>
    <div class="section paddingT-40">
        <div class="container">
            <div class="font-lato font-30">
                <span class="font-ultra-bold">My Jobs</span>
            </div>
            <div class="font-16">We found
                <span class="pc-cnt"><?php echo count($Jobs); ?></span> added jobs on your company.</div>
            <div class="row">
                <div class="col-md-5 col-md-offset-7">
                <form name="search" action="/MyJobs?param=search" method="get">
                    <div class="input-group m-bottom-20">
                        <input type="text" class="form-control" placeholder="Search here..." id="search_name" name="search_name" value="<?php if(isset($search_name)){echo $search_name;} ?>">
                        <span class="input-group-btn">
                            <button class="btn btn-info" type="submit" ><i class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                </form>
                </div>
                <!-- <div class="checkbox col-md-5 col-md-offset-7 pull right" style="margin-left: 588px; margin-top: -20px;font-size: 12px;">
                    <input id="showDeleted" type="checkbox" style='width: 12px;'>show Deleted job
                </div> -->
                <!-- ./col -->
            </div>
            <!-- ./row -->
        </div>
    </div>
    <div class="section">
        <div class="container">
            <div class="row find-project-grid">
                <form class="form-horizontal search-form">
                    <div class="col-md-12">
                        <form role="form" name="educForm" novalidate>
                                <input type="hidden" id="hidden_j_id" name="hidden_j_id">
                                <div class="panel panel-success">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Job Informations</h3>
                                    </div>

                                    <div class="panel-body">
                                        <div class="col-xs-2">
                                            <a href="javascript:beforeCreate();" class="btn btn-info btn-block pull right">Create Job</a>
                                        </div>
                                        <div class="row joblist">
                                        <?php if(isset($Jobs) && !empty($Jobs)){
                                            foreach ($Jobs as $key) {
                                                foreach ($key['info'] as $key1 ) {?>
                                            <div class="panel-group" id="accordionMinJob">
                                                <div class="row detailed-division">
                                                    <div class="row">
                                                        <div class="form-group col-md-5">
                                                            <a href="MyJobs?id=<?php echo $key1->j_id; ?>&param=view"><h4><?php echo $key1->j_title ?></h4></a>
                                                            <p><a href="/EmployerProfile?id=<?php echo  $key1->u_id ?>"><?php echo  $key1->e_companyname ?></a> - <?php echo  $key1->u_address ?></p>
                                                            <p style="width: 450px;font-size: 11px;color:#0E774A">Qualification: <?php echo  $key1->j_requirements ?></p>
                                                            <p style="width: 450px;font-size: 11px;">Categories:
                                                            <div style="width:850px;">
                                                               <?php
                                                                    foreach ($key['categories'] as $keys ) {
                                                                        echo '<div class="skill-tag skill-sm m-right-10" style="font-size: 8px;">'.$keys['category']->jc_jobcategory.'</div>';
                                                                    }
                                                                ?>
                                                            </p>
                                                            </div>
                                                            <p style="width: 450px;font-size: 11px;">Skills:
                                                            <div style="width:850px;">
                                                               <?php
                                                                    foreach ($key['skills'] as $keys ) {
                                                                        echo '<div class="skill-tag skill-sm m-right-10" style="font-size: 8px;">'.$keys['skills']->js_skill.'</div>';
                                                                    }
                                                                ?>
                                                            </p>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <p style="color:#0E774A">Php. <?php echo $key1->j_salaryfrom; ?> - <?php echo $key1->j_salaryto; ?></p>
                                                        </div>
                                                        <div class="form-group col-md-2">
                                                            <?php echo date_format(date_create($key1->j_createdon), 'M d, Y h:i A'); ?>
                                                        </div>
                                                        <div class="form-group col-md-2" style="float:right">
                                                            <a class="remove-jobs" data-id="<?php echo  $key1->j_id ?>" href="javascript:void(0)" style="float:right;border: 1px solid #E7D2D2;margin: 0px 2px 0px 0px;width: 20px;text-align: center;"><i class="glyphicon glyphicon-remove pull-rigth"></i></a>
                                                            <a class="edit-jobs" href="MyJobs?id=<?php echo  $key1->j_id ?>&param=edit" style="float:right;border: 1px solid #E7D2D2;margin: 0px 2px 0px 0px;width: 20px;text-align: center;"><i class="glyphicon glyphicon-pencil pull-rigth"></i></a>
                                                            <a class="view-jobs" href="MyJobs?id=<?php echo $key1->j_id ; ?>&param=view" style="float:right;border: 1px solid #E7D2D2;margin: 0px 2px 0px 0px;width: 20px;text-align: center;"><i class="glyphicon glyphicon-search pull-rigth"></i></a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <?php
                                             }
                                            }
                                        } ?>
                                        </div>
                                        <div class="panel-group text-right">
                                            <div class="SearchPagination">
                                                <?php print $Jobs->appends(array('search_name' =>$search_name))->links(); ?>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </form>
                    <!-- .col -->
                </form>
            </div>
            <!-- ./row -->

        </div>
        <!-- ./container -->
    </div>
    <!-- section -->
</div>
