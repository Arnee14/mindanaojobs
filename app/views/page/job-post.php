<div class="main-container">
    <div class="section p-bottom-70">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="paddingT-40">
                        <div class="font-24 block font-lato font-ultra-bold">Tell us about what you need and we'll match you with the best freelancers for the job</div>
                    </div>
                </div>
            </div>
            <!-- ./row -->

            <form role="form" class="m-top-20" id="jobForm" name="jobForm" data-parsley-validate >
                <input type="hidden" id="j_id" name="j_id" value="<?php if(isset($Job) && !empty($Job)){ echo $Job->j_id; } ?>"/>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group has-icon-alert m-bottom-20">
                            <label class="font-semi-bold">Select a category that matches your job:
                                <span class="form-check cid"><i class="fa fa-check text-dark-green"></i>
                                </span>
                            </label>
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="hidden" class="form-control cboSkills" id="cboCategories" name="cboCategories[]" data-parsley-group="block1" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group has-tip has-icon-alert m-bottom-20">
                            <label class="font-semi-bold">What should we call your job post?
                                <span class="form-check"><i class="fa fa-check text-dark-green"></i>
                                </span>
                            </label>
                            <input class="form-control" type="text" name="title" id="title" value="<?php if(isset($Job) && !empty($Job)){ echo $Job->j_title; } ?>" placeholder="e.g. Need to hire a PHP Developer for new website" data-parsley-group="block1" required/>
                        </div>
                        <div class="form-group has-tip has-icon-alert m-bottom-20">
                            <label class="font-semi-bold">Employment basis
                                <span class="form-check"><i class="fa fa-check text-dark-green"></i>
                                </span>
                            </label>
                            <select class="form-control combo-style" name="employmentbasis" id="employmentbasis">
                                <?php
                                    foreach ($basis as $data) {
                                        if(isset($Job) && !empty($Job)){
                                            if($Job->j_employmentbasis == $data){
                                                echo '<option value="'.$data.'" selected="selected">'.$data.'</option>';
                                            }else{
                                                echo '<option value="'.$data.'">'.$data.'</option>';
                                            }
                                        }else{
                                            echo '<option value="'.$data.'">'.$data.'</option>';
                                        }

                                    }
                                ?>
                           </select>
                        </div>
                        <div class="form-group has-tip has-icon-alert m-bottom-20">
                            <label class="font-semi-bold">Provide details about what you need:
                                <span class="form-check"><i class="fa fa-check text-dark-green"></i>
                                </span>
                            </label>
                            <textarea name="description" id="description"  class="form-control" data-parsley-length="[100, 5000]" data-parsley-group="block1" data-parsley-rangelength="[100,5000]" data-parsley-minlength="100" data-parsley-maxlength="5000" maxlength="5000" rows="12" required placeholder="Enter your details here..."><?php if(isset($Job) && !empty($Job)){ echo $Job->j_description; } ?></textarea>
                        </div>
                        <div class="form-group has-tip has-icon-alert m-bottom-40">
                            <label class="font-semi-bold">Add the Skills that are needed:
                                <span class="form-check"><i class="fa fa-check text-dark-green"></i>
                                </span>
                            </label>
                            <input type="hidden" class="form-control cboSkills" id="cboSkills" name="cboSkills[]" data-parsley-group="block1" required>
                            <ul id="parsley-id-multiple-skills[]" class="parsley-errors-list hidden">
                                <li class="parsley-required">This value is required.</li>
                            </ul>
                        </div>
                        <!-- <div class="form-group has-tip has-icon-alert m-bottom-40">
                            <label class="font-semi-bold">Add the qualification that are needed:
                                <span class="form-check"><i class="fa fa-check text-dark-green"></i>
                                </span>
                            </label>
                            <input name="qualification" id="qualification"  class="form-control" type="text" placeholder="" data-parsley-group="block1" required/>
                            <ul id="parsley-id-multiple-skills[]" class="parsley-errors-list hidden">
                                <li class="parsley-required">This value is required.</li>
                            </ul>
                        </div> -->
                        <div class="form-group has-tip has-icon-alert m-bottom-40">
                            <label class="font-semi-bold">Add the requirements that are needed:
                                <span class="form-check"><i class="fa fa-check text-dark-green"></i></span>
                            </label>
                            <input name="requirements" id="requirements" value="<?php if(isset($Job) && !empty($Job)){ echo $Job->j_requirements; } ?>" class="form-control" type="text" placeholder="e.g. " data-parsley-group="block1" required/>
                            <ul id="parsley-id-multiple-skills[]" class="parsley-errors-list hidden">
                                <li class="parsley-required">This value is required.</li>
                            </ul>
                        </div>
                        <div class="form-group has-tip has-icon-alert m-bottom-40">
                            <label class="font-semi-bold">No. of Years Experience:
                                <span class="form-check"><i class="fa fa-check text-dark-green"></i>
                                </span>
                            </label>
                            <input class="form-control" type="text" name="noOfExp" id="noOfExp" value="<?php if(isset($Job) && !empty($Job)){ echo $Job->j_experience; } ?>" placeholder="" data-parsley-group="block1" required/>
                            <ul id="parsley-id-multiple-skills[]" class="parsley-errors-list hidden">
                                <li class="parsley-required">This value is required.</li>
                            </ul>
                        </div>
                        <div class="form-group has-tip has-icon-alert m-bottom-40">
                            <label class="font-semi-bold">Salary:
                                <span class="form-check"><i class="fa fa-check text-dark-green"></i>
                                </span>
                            </label>
                            <div class="col-md-6">
                                <input id="salaryfrom" name="salaryfrom" value="<?php if(isset($Job) && !empty($Job)){ echo $Job->j_salaryfrom; } ?>" class="form-control col-md-6" type="text" placeholder="From: 0.00" data-parsley-group="block1" required/>
                            </div>
                            <div class="col-md-6">
                                <input id="salaryTo" name="salaryTo" value="<?php if(isset($Job) && !empty($Job)){ echo $Job->j_salaryto; } ?>" class="form-control col-md-6" type="text" placeholder="To: 0.00" data-parsley-group="block1" required/>
                            </div>
                            <ul id="parsley-id-multiple-skills[]" class="parsley-errors-list hidden">
                                <li class="parsley-required">This value is required.</li>
                            </ul>
                        </div>
                        <!-- ./form-group -->
                    </div>
                    <!-- ./col -->
                </div>
                <div class="m-top-15 m-top-20-sm">
                <div class="m-left-30">
                <button type="button" onclick="create()" class="btn btn-primary btn-lg btn-wide postanjob">Post Job<i class="fa fa-angle-double-right m-left-5 font-16"></i>
                </button>
                </div>
                <p class="m-top-10 text-muted font-11 m-top-20">By posting the job, you agree to the Mindanao Jobs <a href="/terms">User Agreement</a> and <a href="/privacy">Privacy Policy</a>
                </p>
            </div>
            </form>

        </div>
    </div>
</div>
