<?php
require 'vendor/autoload.php';
use GuzzleHttp\Client;

class OauthController extends BaseController {


	public function jobseekerlogin(){
		$javascript = array('js/mindanaojobs.js','js/jobseeker_login.js');
		$css = array('css/style.css','css/flatui.css','css/parsley.css','css/animation.css');

		$error = Session::get('error');

    	$data = [
			'page_js' =>$javascript,
			'page_css' =>$css,
			'error' =>$error
		];

		if (Auth::check()) {
	        return Redirect::to('/');
	    }else{
	    	View::share($data);
			return View::make('index')
				->nest('header', 'include.header-login')
				->nest('main', 'page.login')
				->nest('footer', 'include.footer');
	    }
	}

	public function loginAccount(){

		$data = Input::all();

		$rules = array(
			'username'    => 'required|email', // make sure the email is an actual email
			'password' => 'required|alphaNum|min:8' // password can only be alphanumeric and has to be greater than 3 characters
		);

		$validator = Validator::make($data, $rules);

		if ($validator->fails()) {
			$error['title']='Oh snap! You got an error!';
			$error['message'] = 'Your username/password combination was incorrect.';
			return Redirect::to('/login')->with('error',$error);
		} else {

			$userdata = array(
				'u_email' 	=> Input::get('username'),
				'password' => Input::get('password'),
				'isactive' => '1',
				'u_accounttype'=>'local',
				'u_classification'=> 'jobseeker'
			);
			if (Auth::attempt($userdata)) {
				return Redirect::to('/profile');
			} else {
				$error['title']='Oh snap! You got an error!';
				$error['message'] = 'Your username/password combination was incorrect.';
				return Redirect::to('/login')->with('error',$error);
			}
		}
	}

	public function signUpUser(){
		$data = Input::all();
		if (Request::ajax()){
			$return = User::signUpUser($data['users']);
			$results = json_decode($return);

			if($results->success == 'true'){

				$userdata = array(
					'u_email' 	=>$data['users']['u_email'],
					'password' => $data['users']['u_password'],
					'isactive' => '1',
					'u_accounttype'=>'local',
					'u_classification'=> 'jobseeker'
				);
				if (Auth::attempt($userdata)) {
					$result['success'] = $results->success;
					$result['page'] = '/profile';
					return $result;
				} else {
					$result['success'] = 'false';
					$result['msg'] = 'WARNING: Unknown error occur while logging in the account';
					$result['page'] = '/join';
					return $result;
				}
			}else{
				if($results->field){
					$result['field'] = $results->field;
					$result['fieldError'] = $results->fieldError;
				}
				$result['success'] = $results->success;
				$result['msg'] = $results->msg;
				$result['page'] = '/join';
				return $result;
			}
		}
	}



	public function employerlogin(){
		$javascript = array('js/mindanaojobs.js');
		$css = array('css/style.css','css/flatui.css','css/parsley.css','css/animation.css');

		$error = Session::get('error');

    	$data = [
			'page_js' =>$javascript,
			'page_css' =>$css,
			'error' =>$error
		];

		View::share($data);
		return View::make('index')
			->nest('header', 'include.header-login')
			->nest('main', 'page.employer-login')
			->nest('footer', 'include.footer');
	}

	public function EmployerAuth() {
		$data = Input::all();

		$rules = array(
			'username'	=>	'required|email',
			'password'	=>	'required|alphaNum|min:8'
		);

		$validator = Validator::make($data, $rules);

		if($validator->fails()) {
			$error['title']='Oh snap! You got an error!';
			$error['message'] = 'Your username/password combination was incorrect.';
			return Redirect::to('/Employer')->with('error',$error);
		} else {

			$EmployerData = array(
				'u_email'		=>	Input::get('username'),
				'password'	=>	Input::get('password'),
			 	'isactive'		=> '1',
			 	'u_classification'=> 'employer'
			);

			if(Auth::attempt($EmployerData)) {
				return Redirect::to('/EmployerProfile');
			} else {
				$error['title']='Oh snap! You got an error!';
				$error['message'] = 'Your username/password combination was incorrect.';
				return Redirect::to('/Employer')->with('error',$error);
			}
		}
	}

	public function confirmAccount(){
		$mode = Input::get('mode');
		$u = Input::get('u');

		if((isset($mode) && !empty($mode)) && (isset($u) && !empty($u)) && (strlen($u) == 32)){

			$users =  User::confirmAccount($u);
			if (isset($users) && !empty($users)){

				if (Auth::loginUsingId($users[0]['u_id'])) {
					$result['success'] = 'true';
					if ($mode=='j'){
						return Redirect::to('/profile');
					}elseif($mode=='e'){
						return Redirect::to('/EmployerProfile');
					}
				} else {
					$result['success'] = 'false';
					$result['msg'] = 'WARNING: Unknown error occur while logging in the account';

					if ($mode=='j'){
						return Redirect::to('/login');
					}elseif($mode=='e'){
						return Redirect::to('/Employer');
					}
				}
			}else{
				$result['success'] = 'false';
				$result['msg'] = 'WARNING: Account already confirmed';
				if ($mode=='j'){
					return Redirect::to('/login');
				}elseif($mode=='e'){
					return Redirect::to('/Employer');
				}
			}
		}else{
			return Redirect::to('/');
		}
	}

	public function resendConfirm(){
		if (Request::ajax()){
			$User ='';
			$fullname= '';
			$id = Auth::id();
			$currentUser = User::getCurrentUser(array($id));
			if (isset($currentUser) && !empty($currentUser)){
				$User = $currentUser[0];
				$fullname =  $User->js_firstname.' '.$User->js_lastname;
				$mode='j';
			}

			$currentEmployer = Employer::getCurrentEmployer(array($id));
			if (isset($currentEmployer) && !empty($currentEmployer)){
				$User = $currentEmployer[0];
				$fullname =  $User->e_companyname;
				$mode = 'e';
			}

			$token = $User->u_verificationurl;
			$email = $User->u_email;

			$info = array('token'=>$token,'mode'=>$mode);

			Mail::send('emails.confirmation',$info, function($message)use($email,$fullname){
			    $message->to($email,$fullname);
			    $message->from('no-reply@mydomain.com','Mindanao Jobs');
			    $message->subject('Account Confirmation');
			});

			if( count(Mail::failures()) > 0 ) {
			   	$result['success'] = 'false';
				$result['msg'] = 'There was an error while sending the email. Please try again';
			} else {
				$result['success'] = 'true';
				$result['msg'] = 'Email successfully sent';
			}
			return $result;
		}
	}

	public function recoverPassword(){
		if (Request::ajax()){
			$email = Input::get('email');

			if (!empty($email)) {

				$checkEmail = DB::select('SELECT * FROM users WHERE u_email = ? AND u_accounttype = "local"',array($email));
				$data = json_decode(json_encode((array) $checkEmail), true);

				if ($checkEmail) {
					$User ='';
					$fullname= '';
					$userToken = '';

					$token = bin2hex(mcrypt_create_iv(16, MCRYPT_DEV_RANDOM));
					DB::update('UPDATE users u SET u.u_recoveryurl = ?,u_isrecover="0" WHERE u.u_id = ?', array($token,$data[0]['u_id']));

					$currentUser = User::getCurrentUser(array($data[0]['u_id']));
					if (isset($currentUser) && !empty($currentUser)){
						$User = $currentUser[0];
						$fullname =  $User->js_firstname.' '.$User->js_lastname;
						$userToken = $User->u_recoveryurl;
						$mode='j';
					}

					$currentEmployer = Employer::getCurrentEmployer(array($data[0]['u_id']));
					if (isset($currentEmployer) && !empty($currentEmployer)){
						$User = $currentEmployer[0];
						$fullname =  $User->e_companyname;
						$userToken = $User->u_recoveryurl;
						$mode = 'e';
					}

					$info = array('token'=>$userToken,'mode'=>$mode,'fullname'=>$fullname,'email'=>$email);

					Mail::send('emails.reset',$info, function($message)use($email,$fullname){
					    $message->to($email,$fullname);
					    $message->from('no-reply@mydomain.com','Mindanao Jobs');
					    $message->subject('Mindanao Jobs Password Reset');
					});

					if( count(Mail::failures()) > 0 ) {
					   	$result['success'] = 'false';
						$result['msg'] = 'There was an error while sending the email. Please try again';
					} else {
						$result['success'] = 'true';
						$result['msg'] = '';
					}
					return $result;
				}else{
					$result['success'] = 'false';
					$result['msg'] = 'Unable to continue, Email Address not found!!!';
					return $result;
				}
			}
		}
	}

	public function resetAccount(){
		$mode = Input::get('mode');
		$id = Input::get('id');
		$newpass = Input::get('newpass');
		$newpass1 = Input::get('newpass1');


		if((isset($mode) && !empty($mode)) && (isset($id) && !empty($id)) && (strlen($id) == 32)){

			if($newpass != $newpass1){
				$result['field'] = '#newPasswordInput';
				$result['fieldError'] = '<li class="parsley-required">New password does not match with Verify password</li>';
				$result['success'] = 'false';
				$result['msg'] = 'WARNING: New password does not match with Verify password!';
				return $result;
			}

			$user = DB::select('SELECT * FROM users WHERE u_recoveryurl = ? AND u_isrecover = "0" AND u_accounttype="local" LIMIT 1;', array($id));
			$data = json_decode(json_encode((array) $user), true);

			if (isset($user) && !empty($user)){

				$arr = array(Hash::make($newpass),$id);

				$qry_results = DB::update('UPDATE users SET u_password=?, u_isrecover = "1" WHERE u_recoveryurl = ?', $arr);
				if($qry_results){

					$User ='';
					$fullname= '';
					$email = '';

					$currentUser = User::getCurrentUser(array($data[0]['u_id']));
					if (isset($currentUser) && !empty($currentUser)){
						$User = $currentUser[0];
						$fullname =  $User->js_firstname.' '.$User->js_lastname;
						$email = $User->u_email;
					}

					$currentEmployer = Employer::getCurrentEmployer(array($data[0]['u_id']));
					if (isset($currentEmployer) && !empty($currentEmployer)){
						$User = $currentEmployer[0];
						$fullname =  $User->e_companyname;
						$email = $User->u_email;
					}
					$info = array('fullname'=>$fullname,'email'=>$email);

					Mail::send('emails.resetsuccess',$info, function($message)use($email,$fullname){
					    $message->to($email,$fullname);
					    $message->from('no-reply@mydomain.com','Mindanao Jobs');
					    $message->subject('Mindanao Jobs Password reset successfully');
					});

					if( count(Mail::failures()) > 0 ) {
					   	$result['success'] = 'false';
						$result['page'] = '';
						$result['field'] = '';
						$result['fieldError'] = '';
						$result['msg'] = 'There was an error while sending the email. Please try again';
					} else {
						$result['success'] = 'true';
						$result['page'] = '';
						$result['field'] = '';
						$result['fieldError'] = '';
						$result['msg'] = 'Your password has been updated successfully';
					}
					return $result;
				}else{
					$result['success'] = 'false';
					$result['page'] = '';
					$result['field'] = '';
					$result['fieldError'] = '';
					$result['msg'] = 'WARNING: Unknown error occur while resetting password!';
					return $result;
				}
			}else{
				$result['field'] = '';
				$result['fieldError'] = '';
				$result['success'] = 'false';
				$result['page'] = '';
				$result['msg'] = 'WARNING: Session was already been expired!';
				return $result;
			}
		}else{
			return Redirect::to('/');
		}
	}
}
