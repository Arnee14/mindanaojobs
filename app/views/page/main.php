 <div class="main-container">
    <div class="section paddingT-40">
        <div class="container">
            <div class="font-lato font-30">
                <span id="lblJobs" class="font-ultra-bold">All Jobs</span>
            </div>
            <div class="row">
                <div class="col-md-5 col-md-offset-7">
                    <div class="input-group m-bottom-20">
                        <input type="text" class="form-control" placeholder="Search here..." id="search_name">
                        <span class="input-group-btn">
                            <button class="btn btn-info" type="button" onclick="searchText()"><i class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                </div>
                <!-- ./col -->
            </div>
            <!-- ./row -->
        </div>
    </div>
    <div class="section">
        <div class="container">
            <div class="row find-project-grid">
                <form class="form-horizontal search-form">
                    <div class="col-md-9">
                        <div class="bg-grey padding-10 paddingTB-5">
                            <div class="row row-merge">
                                <div class="col-xs-9 col-sm-push-3 text-right text-left-xs no-margin-xs">
                                    <span class="m-left-5 m-right-5">Filter By Places</span>
                                    <select class="cboFilter">
                                        <option value="">- All -</option>
                                        <?php
                                            if (isset($joblocation['location'])){
                                                foreach ($joblocation['location'] as $location =>$value) {
                                                    echo '<option value="'. $value['town_cities_name'].'">'. $value['town_cities_name'] .'</option>';
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="project-list m-top-md">
                                <ul class="project_list">
                                    <?php if(isset($Jobs) && !empty($Jobs)){
                                        foreach ($Jobs as $key) {
                                            foreach ($key['info'] as $key1 ) {?>

                                    <li >
                                        <a href="/jobdetail?id=<?php echo $key1->j_id; ?>" class="font-18 font-semi-bold"><?php echo  $key1->j_title ?></a>
                                        <!---->
                                        <div class="m-top-xs font-16">
                                            <span class="font-semi-bold">Employer: </span><a href="/CompanyProfile?u_id=<?php echo $key1->e_id; ?>&e_id=<?php echo $key1->u_id; ?>"><?php echo  $key1->e_companyname ?></a> ( <?php echo  $key1->u_email ?> )</div>
                                        <div class="m-top-xs">
                                            <div class="inline-block">
                                                <span class="font-semi-bold">Address:</span>
                                                <span class="text-dark-green"><?php echo  $key1->u_address ?></span>
                                            </div>
                                        </div>
                                        <div class="m-top-xs">
                                            <div class="inline-block">
                                                <span class="font-semi-bold">Salary:</span>
                                                <span class="text-dark-green">Php. <?php echo  $key1->j_salaryfrom ?> - <?php echo  $key1->j_salaryto ?></span>
                                                <span class="m-left-5 m-right-5">|</span>
                                            </div>
                                            <div class="inline-block">
                                                <span class="font-semi-bold">Date Posted:</span>
                                                <span class="text-dark-green"><?php echo date_format(date_create($key1->j_createdon), 'M d, Y h:i A'); ?></span>
                                            </div>
                                        </div>
                                        <div class="m-top-xs">
                                            <div class="inline-block" ng-repeat="qualify in jobs.qualification">
                                                <span class="font-semi-bold">Qualification:</span>
                                                <span class="text-dark-green" ><?php echo  $key1->j_requirements ?></span>
                                            </div>
                                        </div>
                                        <b class="font-16 m-top-20" style="color: #E77D16;">
                                            <?php echo  $key1->j_employmentbasis ?>
                                        </b>
                                        <div class="row m-top-30 ">
                                            <div class="col-sm-8">
                                                <div class="font-18">Skills</div>
                                                <div class="m-left-20 no-margin-xs">
                                                    <?php
                                                        if(isset($key['skills']) && !empty($key['skills'])){
                                                            foreach ($key['skills'] as $keys ) {
                                                                echo '<div class="skill-tag skill-sm static-text m-right-10">'.$keys['skills']->js_skill.'</div>';
                                                            }
                                                        }
                                                    ?>
                                                </div>
                                                <div class="font-18">Categories</div>
                                                <div class="m-left-20 no-margin-xs">
                                                    <?php
                                                        if(isset($key['categories']) && !empty($key['categories'])){
                                                            foreach ($key['categories'] as $keys ) {
                                                                echo '<div class="skill-tag skill-sm m-right-10">'.$keys['category']->jc_jobcategory.'</div>';
                                                            }
                                                        }
                                                    ?>
                                                </div>
                                            </div>
                                            <!-- ./col -->
                                            <div class="col-sm-4 text-right paddingR-20 m-top-40-xs">
                                                <a href="/jobdetail?id=<?php echo $key1->j_id; ?>" class="btn decline-btn">Apply Job</a>
                                            </div>
                                            <!-- ./col -->
                                        </div>
                                        <!-- ./row -->
                                    </li>
                                <?php
                                        }
                                    }
                                } ?>
                                </ul>
                            <div class="SearchPagination">
                                <?php echo $Jobs->links(); ?>
                            </div>

                        </div>
                        <!-- ./project-list -->
                    </div>
                    <div class="col-md-3 m-bottom-40 hidden-xs hidden-sm filter-search">
                        <div class="bg-grey paddingB-40 paddingT-30 paddingLR-15">
                            <div class="font-16 font-semi-bold">Filter Search Results</div>
                            <div class="font-16 font-semi-bold m-top-20">Categories</div>
                            <?php
                                if (isset($jobcategories['jobcategories'])){
                                    foreach ($jobcategories['jobcategories'] as $cat =>$value) {
                                        echo '<div class="m-top-10 font-12 font-semi-bold">'.
                                                $value['description'].
                                                '</div>';

                                        if (isset($value['subcategory']) && !empty($value['subcategory'])){
                                            foreach ($value['subcategory'] as $cat) {
                                                echo '<div class="m-top-10 font-12">
                                                        <a href="#" class="clscategory" data-category="'.$cat['description'].'">'.
                                                        $cat['description'].
                                                        '</a>
                                                    </div>';
                                            }
                                        }
                                    }
                                }
                            ?>
                            <hr class="line-dotted">
                            </hr>

                        </div>
                    </div>
                    <!-- .col -->
                </form>
            </div>
            <!-- ./row -->

        </div>
        <!-- ./container -->
    </div>
    <!-- section -->
</div>
