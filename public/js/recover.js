$(document).ready(function() {
    checkicon();

    hideall('step1');

    $("input").focus(function() {
        $('.error_recovery').html('');
    });

    $(".autosubmit input").keydown(function(event) {
        if (event.keyCode == 13) {
            $('.autosubmitform').click();
            return false;
        }
    });


    $('#sendemail').on('click', function() {
        if (true === $('#recoverpassword').parsley().validate('block1')) {
            recoverpassword('#recoverpassword');
        }
    });

    $('#passwordchange').on('click', function() {
        if (true === $('#dopasswordchage').parsley().validate('block1')) {
            passwordchange('#dopasswordchage');
        }
    });
});


function recoverpassword(obj)
{
    $('#overlay').fadeIn(800);
    var params = $(obj).serialize();
    $.ajax({
        type : 'POST',
        url  : 'api/email/recover',
        data : params,
        dataType : 'json',
        success  : function(data)
        {
            $('#overlay').fadeOut(800);
            if(data.msg)
            {
                MessageBox('error',data.msg);
            }
            else
            {
                hideall('step2');
            }
        }
    });
}

function passwordchange(obj){

    $('#overlay').fadeIn(800);
    var params = $(obj).serialize();
    $.ajax({
        type : 'POST',
        url  : '/reset',
        data : params,
        dataType : 'json',
        success  : function(data)
        {
            $('#overlay').fadeOut(800);
            if(data.success == 'false')
            {
                MessageBox('error',data.msg);
                if (data.field) {
                    $(data.field).next('ul').html(data.fieldError);
                    return false;
                }
                if (data.page) {
                    window.location.href=data.page;
                }
            }
            else
            {
                hideall('step2');
            }
        }
    });
}

function hideall(id){
    $('#step1').hide();
    $('#step2').hide();

    $('#'+id).show();
}
