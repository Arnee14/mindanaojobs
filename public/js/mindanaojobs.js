$(window).load(function() {

    //Pace.stop();

    // Fade out the overlay div
    $('#overlay').fadeOut(800);

    $('body').removeClass('overflow-hidden');

    //Enable animation
    $('.wrapper').removeClass('preload');


});

$(function() {
    // modal
    jQuery('[data-dismiss="modal"]').on('click', function() {
        jQuery('.modal').hide();
        jQuery('.modal-backdrop').hide();
    });

    // Popover
    $("[data-toggle=popover]").popover();

    // Tooltip
    $("[data-toggle=tooltip]").tooltip();

    //scroll to top of the page
    $("#scroll-to-top").click(function() {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });

    //upload file
    $('.upload-demo').change(function() {
        var filename = $(this).val().split('\\').pop();
        $(this).parent().find('span').attr('data-title', filename);
        $(this).parent().find('label').attr('data-title', 'Change file');
        $(this).parent().find('label').addClass('selected');
    });

    $('.remove-file').click(function() {
        $(this).parent().find('span').attr('data-title', 'No file...');
        $(this).parent().find('label').attr('data-title', 'Select file');
        $(this).parent().find('label').removeClass('selected');

        return false;
    });

    $('.has-icon-alert .skill-tag').click(function(e) {
        $('#skillNeeded').parent().find('.form-check').addClass('active');
    });


    $('#search_name').keypress(function(e) {
        if (e.which == 13) {
            searchjob('input');
        }
    });


    $('.cboFilterplaces').change(function(){
        var search_name = $('#search_name').val();
        $.get('/joblist', {
            search_name: search_name,
            place: $('.cboFilterplaces').val(),
            type: 'place'
        },function(){
            window.location.href='/joblist?search_name='+search_name+'&type=place&places='+$('.cboFilterplaces').val()+' ';
        });
    });

    $('.clscategory').click(function() {
        $('#lblJobs').text('All Jobs under ' + $(this).data('category'));

        jQuery.get('/jobs/category', {
            cat: $(this).data('category'),
            place: $('.cboFilter').val()
        }, function(data, textStatus, xhr) {

            $('#overlay').fadeIn(800);
            $('.project_list').empty();

            for (var i = 0; i < data.length; i++) {
                var $key1 = data[i].info.data;
                var skills = data[i].skills;
                var categories = data[i].categories;

                var html = '<li >\
                                <a href="/jobdetails/' + $key1.j_id + '" class="font-18 font-semi-bold">' + $key1.j_title + '</a>\
                                <div class="m-top-xs font-16">\
                                    <span class="font-semi-bold">Employer: </span><a href="/CompanyProfile/' + $key1.e_id + '/' + $key1.u_id + '">' + $key1.e_companyname + '</a> ( ' + $key1.u_email + ')</div>\
                                <div class="m-top-xs">\
                                    <div class="inline-block">\
                                        <span class="font-semi-bold">Address:</span>\
                                        <span class="text-dark-green">' + $key1.u_address + '</span>\
                                    </div>\
                                </div>\
                                <div class="m-top-xs">\
                                    <div class="inline-block">\
                                        <span class="font-semi-bold">Salary:</span>\
                                        <span class="text-dark-green">Php.' + $key1.j_salaryfrom + ' - ' + $key1.j_salaryto + '</span>\
                                        <span class="m-left-5 m-right-5">|</span>\
                                    </div>\
                                    <div class="inline-block">\
                                        <span class="font-semi-bold">Date Posted:</span>\
                                        <span class="text-dark-green">' + $key1.j_createdon + '</span>\
                                    </div>\
                                </div>\
                                <div class="m-top-xs">\
                                    <div class="inline-block" ng-repeat="qualify in jobs.qualification">\
                                        <span class="font-semi-bold">Qualification:</span>\
                                        <span class="text-dark-green" >' + $key1.j_requirements + '</span>\
                                    </div>\
                                </div>\
                                <b class="font-16 m-top-20" style="color: #E77D16;">' + $key1.j_employmentbasis + '</b>\
                                <div class="row m-top-30 ">\
                                    <div class="col-sm-8">\
                                        <div class="font-18">Skills</div>\
                                        <div class="m-left-20 no-margin-xs">';
                if (skills.length > 0) {
                    for (var a = 0; a < skills.length; a++) {
                        html += '<div class="skill-tag skill-sm static-text m-right-10">' + skills[a].skills.js_skill + '</div>';
                    };
                }
                html += '</div>\
                                                        <div class="font-18">Categories</div>\
                                                        <div class="m-left-20 no-margin-xs">';

                if (categories.length > 0) {
                    for (var a = 0; a < categories.length; a++) {
                        html += '<div class="skill-tag skill-sm static-text m-right-10">' + categories[a].category.jc_jobcategory + '</div>';
                    };
                }
                html += '</div>\
                                    </div>\
                                    <div class="col-sm-4 text-right paddingR-20 m-top-40-xs">\
                                        <a href="/jobdetails/' + $key1.j_id + '" class="btn decline-btn">Apply Job</a>\
                                    </div>\
                                </div>\
                            </li>';

                $('.project_list').append(html);
            };
            $('#overlay').fadeOut(800);
        });

    })


});


function searchText(type) {

    var search_name = $('#search_name').val();

    jQuery.get('/jobs/search', {
        value: search_name,
        place: $('.cboFilter').val()
    }, function(data, textStatus, xhr) {
        $('#overlay').fadeIn(800);
        $('.project_list').empty();

        for (var i = 0; i < data.length; i++) {
            var $key1 = data[i].info.data;
            var skills = data[i].skills;
            var categories = data[i].categories;

            var html = '<li >\
                                <a href="/jobdetails/' + $key1.j_id + '" class="font-18 font-semi-bold">' + $key1.j_title + '</a>\
                                <div class="m-top-xs font-16">\
                                    <span class="font-semi-bold">Employer: </span><a href="/CompanyProfile/' + $key1.e_id + '/' + $key1.u_id + '">' + $key1.e_companyname + '</a> ( ' + $key1.u_email + ')</div>\
                                <div class="m-top-xs">\
                                    <div class="inline-block">\
                                        <span class="font-semi-bold">Address:</span>\
                                        <span class="text-dark-green">' + $key1.u_address + '</span>\
                                    </div>\
                                </div>\
                                <div class="m-top-xs">\
                                    <div class="inline-block">\
                                        <span class="font-semi-bold">Salary:</span>\
                                        <span class="text-dark-green">Php.' + $key1.j_salaryfrom + ' - ' + $key1.j_salaryto + '</span>\
                                        <span class="m-left-5 m-right-5">|</span>\
                                    </div>\
                                    <div class="inline-block">\
                                        <span class="font-semi-bold">Date Posted:</span>\
                                        <span class="text-dark-green">' + $key1.j_createdon + '</span>\
                                    </div>\
                                </div>\
                                <div class="m-top-xs">\
                                    <div class="inline-block" ng-repeat="qualify in jobs.qualification">\
                                        <span class="font-semi-bold">Qualification:</span>\
                                        <span class="text-dark-green" >' + $key1.j_requirements + '</span>\
                                    </div>\
                                </div>\
                                <b class="font-16 m-top-20" style="color: #E77D16;">' + $key1.j_employmentbasis + '</b>\
                                <div class="row m-top-30 ">\
                                    <div class="col-sm-8">\
                                        <div class="font-18">Skills</div>\
                                        <div class="m-left-20 no-margin-xs">';
            if (skills.length > 0) {
                for (var a = 0; a < skills.length; a++) {
                    html += '<div class="skill-tag skill-sm static-text m-right-10">' + skills[a].skills.js_skill + '</div>';
                };
            }
            html += '</div>\
                                        <div class="font-18">Categories</div>\
                                        <div class="m-left-20 no-margin-xs">';

            if (categories.length > 0) {
                for (var a = 0; a < categories.length; a++) {
                    html += '<div class="skill-tag skill-sm static-text m-right-10">' + categories[a].category.jc_jobcategory + '</div>';
                };
            }
            html += '</div>\
                                    </div>\
                                    <div class="col-sm-4 text-right paddingR-20 m-top-40-xs">\
                                        <a href="/jobdetails/' + $key1.j_id + '" class="btn decline-btn">Apply Job</a>\
                                    </div>\
                                </div>\
                            </li>';

            $('.project_list').append(html);
        };
        $('#overlay').fadeOut(800);
    });

}

function searchjob(type) {
    var search_name = $('#search_name').val();
    var showDel;

    if($('#showDeleted').prop('checked') == true){
        showDel = 0;
    }else{
        showDel = 1;
    }

    jQuery.get('/jobs/Empsearch', {
        value: search_name,
        active: showDel,
        type: type
    }, function(data, textStatus, xhr) {
        $('#overlay').fadeIn(800);
        $('.joblist').empty();

        for (var i = 0; i < data.data.length; i++) {

            var $key1 = data.data[i].info.data;
            var skills = data.data[i].skills;
            var categories = data.data[i].categories;

            var html = '<div class="panel-group" id="accordionMinJob">\
            <div class="row detailed-division">\
                <div class="row">\
                    <div class="form-group col-md-5">\
                         <a href="MyJobs?id='+ $key1.j_id +'>&param=view"><h4>' + $key1.j_title + '</h4></a>\
                        <p><a href="/EmployerProfile?id='+ $key1.j_id +'>">' + $key1.e_companyname + '</a> - ' + $key1.u_address + '</p>\
                        <p style="width: 450px;font-size: 11px;color:#0E774A">Qualification: ' + $key1.j_requirements + '</p>\
                        <p style="width: 450px;font-size: 11px;">Categories: \
                        <div style="width:850px;">';
                           if (categories.length > 0) {
                                for (var a = 0; a < categories.length; a++) {
                                    html += '<div class="skill-tag skill-sm m-right-10" style="font-size: 8px;">' + categories[a].category.jc_jobcategory + '</div>';
                                };
                            }
                        html +='</p>\
                        </div>\
                        <p style="width: 450px;font-size: 11px;">Skills: \
                        <div style="width:850px;">';
                           if (skills.length > 0) {
                                for (var a = 0; a < skills.length; a++) {
                                    html += '<div class="skill-tag skill-sm m-right-10" style="font-size: 8px;">' + skills[a].skills.js_skill + '</div>';
                                };
                            }
                        html += '</p>\
                        </div>\
                    </div>\
                    <div class="form-group col-md-3">\
                        <p style="color:#0E774A">Php. ' + $key1.j_salaryfrom + ' - ' + $key1.j_salaryto + '</p>\
                    </div>\
                    <div class="form-group col-md-2">';
                    var d = new Date($key1.j_createdon);
                    var dte = d.getDate();
                    var yr = d.getFullYear();
                    var month = new Array();
                        month[0] = "Jan";
                        month[1] = "Feb";
                        month[2] = "Mar";
                        month[3] = "Apr";
                        month[4] = "May";
                        month[5] = "Jun";
                        month[6] = "Jul";
                        month[7] = "Aug";
                        month[8] = "Sep";
                        month[9] = "Oct";
                        month[10] = "Nov";
                        month[11] = "Dec";
                        var mnth = month[d.getMonth()];

                    var hours = d.getHours();
                    var minutes = d.getMinutes();
                    var ampm = hours >= 12 ? 'PM' : 'AM';
                    hours = hours % 12;
                    hours = hours ? hours : 12; // the hour '0' should be '12'
                    minutes = minutes < 10 ? '0'+minutes : minutes;
                    var strTime = hours + ':' + minutes + ' ' + ampm;
                    var newDate = mnth + ' ' +dte+', ' +yr+ ' '+strTime;
                       html +=' ' + newDate + ' \
                    </div>\
                    <div class="form-group col-md-2" style="float:right">\
                        <a class="remove-jobs" data-id="' + $key1.j_id +'" href="javascript:void(0)" style="float:right;border: 1px solid #E7D2D2;margin: 0px 2px 0px 0px;width: 20px;text-align: center;"><i class="glyphicon glyphicon-remove pull-rigth"></i></a>\
                        <a class="edit-jobs" href="MyJobs?id=' + $key1.j_id +'&param=edit" style="float:right;border: 1px solid #E7D2D2;margin: 0px 2px 0px 0px;width: 20px;text-align: center;"><i class="glyphicon glyphicon-pencil pull-rigth"></i></a>\
                        <a class="view-jobs" href="MyJobs?id=' + $key1.j_id +'&param=view" style="float:right;border: 1px solid #E7D2D2;margin: 0px 2px 0px 0px;width: 20px;text-align: center;"><i class="glyphicon glyphicon-search pull-rigth"></i></a>\
                    </div>\
                </div>';
            $('.joblist').append(html);
        };
        $('#overlay').fadeOut(800);
    });
}

function parsleyLeftload() {
    var obj = '.leftchar';
    var left = parseInt($(obj).parent().find('.left').html());

    var nleft = 0;
    if (typeof($(obj).val()) !== 'undefined') {
        var y = $(obj).val();
        nleft = parseInt(y.length);
    }

    var r = left - nleft;
    if (r <= 0) {
        $(obj).parent().find('.left').html(0);
        return false;
    }
    $(obj).parent().find('.left').html(r);
}

function parsleyLeft(obj) {
    var left = parseInt($(obj).attr('maxlength'));
    var nleft = parseInt($(obj).val().length);
    var r = left - nleft;
    if (r <= 0) {
        $(obj).parent().find('.left').html(0);
        return false;
    }
    $(obj).parent().find('.left').html(r);

}

function saveChanges(content) {
    $('.save-changes-header').hide();
    if (content)
        $('.main-container').prepend('<div class="container verification-text save-changes-header"> ' + content + ' </div>');
    else
        $('.main-container').prepend('<div class="container verification-text save-changes-header"> Your changes have been saved </div>');
    $("html, body").animate({
        scrollTop: 0
    }, 600);

    if ((FormName) || FormName != '')
        Confirm(FormName);


    // return false;

}
var FormName = '';
var form_original_data = '';
var ActionConfirm = '';

function Confirm(FormNames) {
    FormName = FormNames;
    form_original_data = $(FormName).serialize();
    //ActionConfirm = FormName +' .btn-success';

    //alert(FormName);
}

function checkiconNonempty() {

    $('.has-icon-alert input[type=text]').each(function() {
        if ($(this).val() != '' && $(this).val() != null)
            $(this).parent().find('.form-check').addClass('active');
        else
            $(this).parent().find('.form-check').removeClass('active');
    });

    $('.has-icon-alert input[type=email]').blur(function() {
        if ($(this).val() != '' && $(this).val() != null)
            $(this).parent().find('.form-check').addClass('active');
        else
            $(this).parent().find('.form-check').removeClass('active');
    });
    $('.has-icon-alert input[type=password]').blur(function() {
        if ($(this).val() != '' && $(this).val() != null)
            $(this).parent().find('.form-check').addClass('active');
        else
            $(this).parent().find('.form-check').removeClass('active');
    });

    $('.has-icon-alert textarea').each(function() {
        if ($(this).val() != '' && $(this).val() != null && $(this).val().length > 100)
            $(this).parent().find('.form-check').addClass('active');
        else
            $(this).parent().find('.form-check').removeClass('active');
    });

    $('.has-icon-alert select').each(function() {

        if ($(this).val() != '' && $(this).val() != null)
            $(this).parent().find('.form-check').addClass('active');
        else
            $(this).parent().find('.form-check').removeClass('active');
    });



}

function validateEmail(sEmail) {
    var filter = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,11})?$/;
    if (filter.test(sEmail)) {
        return true;
    } else {
        return false;
    }
}

function checkicon() {
    checkiconNonempty();
    //Display Check Icon

    $('.has-icon-alert input[type=text]').blur(function() {
        if ($(this).val() != '' && $(this).val() != null)
            $(this).parent().find('.form-check').addClass('active');
        else
            $(this).parent().find('.form-check').removeClass('active');
    });
    $('.has-icon-alert input[type=email]').blur(function() {
        if ($(this).val() != '' && $(this).val() != null && validateEmail($(this).val()))
            $(this).parent().find('.form-check').addClass('active');
        else
            $(this).parent().find('.form-check').removeClass('active');
    });
    $('.has-icon-alert input[type=password]').blur(function() {
        var val = $(this).val();
        if ($(this).attr('id') == 'password2' && $('#password').length == 1) {
            if (val != '' && $(this).val() != null && val == $('#password').val() && val.length > 7) {
                $(this).parent().find('.form-check').addClass('active');
            } else {
                $(this).parent().find('.form-check').removeClass('active');
            }
        } else if ($(this).attr('id') == 'password' && $('#password2').length == 1) {
            if (val != '' && val != null && val == $('#password2').val() && val.length > 7) {
                $(this).parent().find('.form-check').addClass('active');
                $('#password2').parent().find('.form-check').addClass('active');
            } else if (val != '' && val != null && val.length > 7) {
                $(this).parent().find('.form-check').addClass('active');
                $('#password2').parent().find('.form-check').removeClass('active');
            } else {
                $(this).parent().find('.form-check').removeClass('active');
                $('#password2').parent().find('.form-check').removeClass('active');
            }
        } else {
            //alert(1);
            if ($(this).val() != '' && $(this).val() != null)
                $(this).parent().find('.form-check').addClass('active');
            else
                $(this).parent().find('.form-check').removeClass('active');
        }
    });

    $('.has-icon-alert textarea').blur(function() {
        if ($(this).val() != '' && $(this).val() != null && $(this).val().length > 100)
            $(this).parent().find('.form-check').addClass('active');
        else
            $(this).parent().find('.form-check').removeClass('active');
    });

    $('.has-icon-alert select').change(function() {

        if ($(this).val() != '' && $(this).val() != null) {
            $(this).parent().find('.form-check').addClass('active');
        } else {

            $(this).parent().find('.form-check').removeClass('active');
        }
    });
}

function MessageBox(type, message,refresh) {
    // type= 'alert','success','information','error','warning','notification','success'
    refresh = typeof refresh !== 'undefined' ? refresh : false;

    if (refresh == true){
        var n = noty({
            text        : message,
            type        : type,
            dismissQueue: true,
            layout      : 'top',
            theme       : 'relax',
            closeWith   : ['button', 'click'],
            maxVisible  : 5,
            modal       : false,
            timeout     : 1000,
            callback: {
                afterClose: function() {
                    window.location.reload();
                },
            }
        });
    }else{
        var n = noty({
            text        : message,
            type        : type,
            dismissQueue: true,
            layout      : 'top',
            theme       : 'relax',
            closeWith   : ['button', 'click'],
            maxVisible  : 5,
            modal       : false,
            timeout     : 2000
        });
    }
}


function resendConfirmation(){
    $('#overlay').fadeIn(800);
    $.ajax({
        type: 'POST',
        url: 'api/email/resendconfirm',
        dataType: 'json',
        success: function(data) {
            console.log(data);
            if (data.success === 'true') {
                $('#overlay').fadeOut(800);
                MessageBox('success',data.msg);
                return;
            } else {
                $('#overlay').fadeOut(800);
                MessageBox('error',data.msg);
                return;
            }
        }
    });
}
