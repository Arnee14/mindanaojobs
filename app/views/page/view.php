<div class="main-container no-bottom-space">
    <?php if(isset($User) && !empty($User)){
        if ($User->u_isverified != '1'){
    ?>
    <div class="section bg-danger paddingTB-10 alert-top section-border-bottom">
        <div class="container">
          <div class="row">
            <div class="col-sm-7">
                <i class="pull-left m-right-15 m-top-5 no-margin-xs fa fa-3x fa-exclamation-triangle text-danger"></i>
                <div class="font-21 font-semi-bold title-text"> Your account is currently disabled. </div>
                <div class="font-lato information-text">
                    <span class="font-14 font-semi-bold">Check your email to verify your account. <a href="javascript:resendConfirmation()">Click here</a></span> to resend confirmation
                </div>
            </div>
          </div>
        </div>
    </div>
    <?php
            }
        }
    ?>
    <div class="section section-border-bottom paddingB-20">
        <div class="container">
            <div class="paddingTB-20">
                <div class="row"></div>
                <!-- ./row -->
            </div>
        </div>
    </div>

    <div class="section">
        <div class="bg-grey">
            <div class="container">
                <ul id="myTab" class="nav nav-tabs custom-tab">
                    <li class="active"><a href="#overviewTab" >Overview</a></li>
                    <li class=""><a href="#educationalTab" >Educational Background</a></li>
                    <li class="tab-lg "><a href="#skillTab" >Skills And Specialization</a></li>
                    <li class="tab-lg "><a href="#professionTab" >Professional Experience</a></li>
                </ul>

                <div id="myTabContent" class="tab-content">
                    <div role="tabpanel" class="tab-pane overviewTab fade in active" id="overviewTab" aria-labelledBy="overviewTab">
                        <div class="bg-grey paddingB-100">
                            <div class="container">
                                <div class="container">
                                    <div class="paddingTB-40">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <div class="clearfix">
                                                    <?php if(isset($User) && !empty($User) && !empty($User->u_displaypicture)) { ?>
                                                        <img src="<?php echo $User->u_displaypicture; ?>" alt="..." class="applicant-profile-pic"  style="border-radius:50em">
                                                    <?php }else{ ?>
                                                        <img src="../img/photo.jpg" alt="..." class="applicant-profile-pic" style="border-radius:50em">
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div class="col-md-8 m-top-20-sm">
                                                <div class="font-lato font-30"><?php if(isset($User) && !empty($User)){ echo $User->Fullname; } ?></div>
                                                <ul class="list-style-icon m-top-40">
                                                    <li class="m-bottom-15">
                                                        <div class="list-icon text-center">
                                                            <i class="fa fa-money fa-2x"></i>
                                                        </div>
                                                        <div class="list-content font-semi-bold">Expected Salary: <?php if(isset($User) && !empty($User)){ echo $User->js_desiredsalary; } ?></div>
                                                    </li>
                                                    <li class="m-bottom-15">
                                                        <div class="list-icon text-center">
                                                            <i class="fa fa-map-marker fa-lg"></i>
                                                        </div>
                                                        <div class="list-content font-semi-bold "><?php if(isset($User) && !empty($User)){ echo $User->u_address; } ?></div>
                                                    </li>
                                                    <li class="m-bottom-15">
                                                        <div class="list-icon text-center">
                                                            <i class="fa fa-phone fa-lg"></i>
                                                        </div>
                                                        <div class="list-content font-semi-bold">
                                                           <?php if(isset($User) && !empty($User)){ echo $User->u_telephoneno; } ?>
                                                        </div>
                                                        <div class="list-icon text-center">
                                                            <i class="fa fa-mobile-phone fa-lg"></i>
                                                        </div>
                                                        <div class="list-content font-semi-bold">
                                                           <?php if(isset($User) && !empty($User)){ echo $User->u_mobileno; } ?>
                                                        </div>
                                                        <div class="list-icon text-center">
                                                            <i class="fa fa-fax fa-lg"></i>
                                                        </div>
                                                        <div class="list-content font-semi-bold">
                                                           <?php if(isset($User) && !empty($User)){ echo $User->u_fax; } ?>
                                                        </div>
                                                    </li>
                                                    <li class="m-bottom-15">
                                                        <div class="list-icon text-center">
                                                            <i class="fa fa-envelope fa-lg"></i>
                                                        </div>
                                                        <div class="list-content font-semi-bold "><?php if(isset($User) && !empty($User)){ echo $User->u_email; } ?></div>
                                                    </li>
                                                </ul>
                                                <span class="short-text">
                                                    <?php if(isset($User) && !empty($User)){ echo $User->js_desc; } ?>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- ./container -->
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade educationalTab" id="educationalTab" aria-labelledBy="educationalTab">
                        <div class="bg-grey paddingB-100">
                            <div class="container">
                                <div class="paddingTB-40">
                                    <?php if(isset($Education) && !empty($Education)){
                                        foreach ($Education as $key) { ?>

                                        <div class="font-20 font-ultra-bold m-top-40">
                                            <?php echo $key->ed_institution;?>
                                        </div>
                                        <div class="font-16 font-semi-bold m-top-20">
                                            <h3><?php echo $key->ed_field;?> <small><?php echo $key->ed_major;?></small></h3>
                                        </div>
                                        <div class="font-italic m-top-5">
                                            <?php echo $key->ed_attainment;?> (<?php echo $key->ed_yearfrom .' - '. $key->ed_yearto;?>)
                                        </div>
                                        <p class="font-16 m-top-5"><?php echo $key->ed_address;?></p>
                                    <?php
                                        }
                                    } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane skillTab fade" id="skillTab" aria-labelledBy="skillTab">
                        <div class="bg-grey paddingB-100">
                            <div class="container">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12 m-top-20-sm">
                                            <div class="paddingTB-40">
                                                <div class="font-24">Specialization</div>
                                                <div class="m-top-25 m-left-20 font-12">
                                                    <?php
                                                        if (isset($Categories) && !empty($Categories)){
                                                            foreach ($Categories as $key) { ?>
                                                            <div class="skill-tag skill-sm m-right-10"><?php echo $key->jc_name; ?></div>
                                                    <?php
                                                            }
                                                        }
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="paddingTB-40">
                                                <div class="font-24">Skills</div>
                                                <div class="m-top-25 m-left-20 font-12">
                                                    <?php
                                                        if (isset($Skills) && !empty($Skills)){
                                                            foreach ($Skills as $key) { ?>
                                                            <div class="skill-tag skill-sm m-right-10"><?php echo $key->s_name; ?></div>
                                                    <?php
                                                            }
                                                        }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- ./container -->
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane professionTab fade" id="professionTab" aria-labelledBy="professionTab">
                        <div class="bg-grey paddingB-100">
                            <div class="container">
                                <div class="paddingTB-40">
                                    <?php if(isset($Experience) && !empty($Experience)){
                                        foreach ($Experience as $key) { ?>
                                        <div class="font-20 font-semi-bold m-top-30">
                                            <?php echo $key->we_companyname; ?> : <?php echo $key->we_position; ?></div>
                                        <div class="font-italic m-top-5"><?php echo $key->we_location; ?> : <?php echo $key->we_yearstart.' - '.$key->we_yearend; ?></div>
                                        <p class="font-16 m-top-5"><?php echo $key->we_description; ?></p>
                                    <?php
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


