<div class="main-container no-bottom-space">
    <div class="section section-border-bottom paddingT-40 paddingB-60">
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <!--<div class="font-30 font-lato">Browse Job Requests <span class="font-ultra-bold">For Free</span></div>-->
                    <div class="font-30 font-lato">Create a
                        <span class="text-blue">FREE</span> profile and start applying jobs</div>
                    <form role="form" class="m-top-40" id="registerform" name="regForm" novalidate data-parsley-validate>
                        <input type="hidden" name="cmd" value="_register_as_freelancer" />
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group has-icon-alert">
                                    <label class="font-12 font-normal">First Name:
                                        <span class="form-check"><i class="fa fa-check text-dark-green"></i>
                                        </span>
                                    </label>
                                    <input class="form-control custom-input" type="text" name="FirstName" id="FirstName" data-parsley-group="block1" data-parsley-required-message="Please enter your First Name" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group has-icon-alert">
                                    <label class="font-12 font-normal">Last Name:
                                        <span class="form-check"><i class="fa fa-check text-dark-green"></i>
                                        </span>
                                    </label>
                                    <input class="form-control custom-input" type="text" name="LastName" id="LastName" data-parsley-group="block1" data-parsley-required-message="Please enter your Last Name" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group has-icon-alert">
                            <label class="font-12 font-normal">Email:
                                <span class="form-check"><i class="fa fa-check text-dark-green"></i>
                                </span>
                            </label>
                            <input class="form-control custom-input" type="email" name="email" id="email" data-parsley-group="block1" data-parsley-required-message="Please enter an Email Address" required>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group has-icon-alert">
                                    <label class="font-12 font-normal">Password:
                                        <span class="form-check"><i class="fa fa-check text-dark-green"></i>
                                        </span>
                                    </label>
                                    <input class="form-control custom-input" type="password" name="password" id="password" data-parsley-group="block1" data-parsley-minlength="8" data-parsley-required-message="Please enter a Password" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group has-icon-alert">
                                    <label class="font-12 font-normal">Confirm Password:
                                        <span class="form-check"><i class="fa fa-check text-dark-green"></i>
                                        </span>
                                    </label>
                                    <input class="form-control custom-input" type="password" name="confirmpassword" id="confirmpassword"  data-parsley-group="block1" data-parsley-equalto="#password" data-parsley-required-message="Please confirm your Password" required>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- ./col -->
                <div class="col-md-4 col-md-offset-1 m-top-40-sm">
                    <div class="bg-grey bordered rounded paddingTB-20 paddingLR-25">
                        <div class="font-16 font-semi-bold text-center">Sign up using Social Media</div>
                        <hr class="m-top-10 m-bottom-10">
                        <ul class="list-style-icon icon-lg signup-sidebar">
                            <li>
                                <a href="/facebook" class="btn btn-social btn-block btn-facebook">
                                    <i class="fa fa-facebook"></i> Sign in with Facebook
                                </a>
                            </li>
                            <li>
                                <a href="/google" class="btn btn-social btn-block btn-google-plus">
                                    <i class="fa fa-google-plus"></i> Sign in with Google
                                </a>
                            </li>
                            <li>
                                <a href="/linkedIn" class="btn btn-social btn-block btn-linkedin">
                                    <i class="fa fa-linkedin"></i> Sign in with LinkedIn
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- ./col -->
            </div>
            <!-- ./row -->
        </div>
        <!-- ./container -->
    </div>
    <!-- ./section -->

    <div class="section bg-grey">
        <div class="container">
            <div class="text-center m-top-40 m-bottom-100"> <a href="javascript:void(0);" class="btn btn-primary btn-lg jobseeker-signup">Get Started<i class="fa fa-angle-double-right m-left-5"></i></a>
                <div class="font-11">
                    <div class="m-top-20">By choosing to continue, you agree to the
                        <br>Outsource <a href="/terms" class="text-blue">User Agreement</a> and <a href="/privacy" class="text-blue">Privacy Policy</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- ./container -->
    </div>
    <!-- ./section -->
</div>
