<div class="main-container no-bottom-space">
    <div class="section">
        <div class="bg-grey">
            <div class="container">
                        <div id="myTabContent" class="tab-content">
                            <div class="tab-pane fade active in" id="overviewTab">
                                <div class="bg-grey paddingT-40 paddingB-100">
                                    <div class="container">
                                        <div class="font-24 font-semi-bold line-1">Profile Overview</div>
                                        <form role="form" data-parsley-validate name="registerform" id="registerform" class="form-horizontal m-top-40">
                                            <div class="col-lg-8">
                                            <div class="form-group">
                                                <label class="col-lg-12 col-sm-12 control-label font-normal font-14 text-left">Company Name:</label>
                                                <div class="col-sm-12 col-lg-12">
                                                    <input type="text" id="compname" name="compname" class="form-control style-form" data-parsley-group="block1" required placeholder="Company Name">
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label for="professionalTitle" class="col-lg-12 col-sm-12 control-label font-normal font-14 text-left">Description :</label>
                                                <div class="col-sm-12 col-lg-12">
                                                    <textarea id="desc" name="desc" class="form-control leftchar" data-parsley-length="[50, 3000]" rows="12"  data-parsley-group="block1" required data-parsley-rangelength="[100,3000]" data-parsley-minlength="100" data-parsley-maxlength="3000" maxlength="3000"></textarea>
                                                    <div class="col-md-12 text-right">
                                                        <span class="left">3000</span>characters left</div>
                                                    <p class="block text-muted font-12 m-top-10">Outsource is built on trust. Help clients get to know you and your professional background.</p>
                                                </div>
                                            </div>
                                            <div class="form-group m-top-10">
                                                <label for="address" class="col-lg-12 col-sm-12 control-label font-normal font-14 text-left">Landline:</label>
                                                <div class="col-sm-12 col-md-12">
                                                    <input type="text" class="form-control" id="telephone" name="telephone" placeholder="Landline Number">
                                                </div>
                                            </div>
                                            <div class="form-group m-top-10">
                                                <label for="address" class="col-lg-12 col-sm-12 control-label font-normal font-14 text-left">Mobile:</label>
                                                <div class="col-sm-12 col-md-12">
                                                    <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Mobile Number">
                                                </div>
                                            </div>
                                            <div class="form-group m-top-10">
                                                <label for="address" class="col-lg-12 col-sm-12 control-label font-normal font-14 text-left">Address:</label>
                                                <div class="col-sm-12 col-md-12">
                                                    <select name="address" id="address" class="form-control">
                                                <?php
                                                    if (isset($joblocation2['location2']) && !empty($joblocation2['location2'])){
                                                        foreach ($joblocation2['location2'] as $location) { ?>
                                                            <optgroup label="<?php echo $location['region']; ?>">
                                                                <?php
                                                                    if (isset($location['Town_Cities']) && !empty($location['Town_Cities'])){
                                                                        foreach ($location['Town_Cities'] as $item) {
                                                                            echo '<option value="'.$item['locationname'].'">
                                                                                '.$item['locationname'].'
                                                                            </option>';

                                                                        }
                                                                    }
                                                                ?>
                                                            </optgroup>';
                                                <?php
                                                        }
                                                    }
                                                ?>
                                            </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-12 col-sm-12 control-label font-normal font-14 text-left">Website:</label>
                                                <div class="col-sm-12 col-lg-12">
                                                    <input type="text" class="form-control style-form" id="website" name="website" data-parsley-group="block1" required placeholder="Website" >
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-12 col-sm-12 control-label font-normal font-14 text-left">Email Address:</label>
                                                <div class="col-sm-12 col-lg-12">
                                                    <input type="text" class="form-control style-form" id="email" name="email" data-parsley-group="block1" required placeholder="Email Address">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-12 col-sm-12 control-label font-normal font-14 text-left">Password:</label>
                                                <div class="col-sm-12 col-lg-12">
                                                    <input type="password" class="form-control style-form" id="password" name="password" required placeholder="Password" data-parsley-group="block1">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-12 col-sm-12 control-label font-normal font-14 text-left">Confirm Password:</label>
                                                <div class="col-sm-12 col-lg-12">
                                                    <input type="password" class="form-control style-form" id="password1" name="password1" required placeholder="Confirm Password" data-parsley-group="block1">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="professionalTitle" class="col-lg-12 col-sm-12 control-label font-normal font-14 text-left"></label>
                                                <div class="col-sm-12 col-lg-12 text-right">
                                                    <button type="button" class="btn btn-danger btn-wide registeremployer">Save</button>
                                                </div>
                                            </div>
                                        </div>
                                        </form>
                                    </div>
                                    <!-- ./container -->
                                </div>
                                <!-- ./bg-grey -->
                            </div>
                            <!-- overview tab -->
                        </div>
            </div>
        </div>
    </div>
</div>
