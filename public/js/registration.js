// JavaScript Document

$(function() {
    $('.jobseeker-signup').on('click', function() {

        if (true === $('#registerform').parsley().validate('block1')) {
            check_jobseeker();
        }

    });

    blureffect('#FirstName');
    blureffect('#LastName');
    blureffect('#email');
    blureffect('#password');
    blureffect('#confirmpassword');
});

function blureffect(obj) {
    $(obj).on('focus', function() {
        $(obj).removeClass('scripterror');

    });
}


function check_jobseeker() {

    $('#overlay').fadeIn(800);

    var users = new Object();
    users.js_lastname = $('#LastName').val();
    users.js_firstname = $('#FirstName').val();
    users.u_email = $('#email').val();
    users.u_password = $('#password').val();
    users.u_accounttype = 'local';

    $.ajax({
        type: 'POST',
        url: '/api/users',
        data: {
            users: users
        },
        dataType: 'json',
        success: function(data) {
            var decode = data;
            if (decode.success === 'true') {
                window.location.href = decode.page;
            } else {
                $('#overlay').fadeOut(800);
                console.log(decode.field);
                if (decode.field) {
                    MessageBox('error',data.msg);
                    $(decode.field).next('ul').html(decode.fieldError);
                    return false;
                }
                MessageBox('error',data.msg);
                // window.location.href = decode.page;
            }
        }
    });

}
