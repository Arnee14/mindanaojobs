var _activeType = '';

$(document).on('click', '.signup-option-widget', function() {
    $('.signup-option-widget').removeClass('active');
    $(this).addClass('active');

    _activeType = $(this).attr('id');

    var _href = '';

    if (_activeType == 'signupType1') {
        _href = '/EmployerForm';
    } else {
        _href = '/join';
    }

    if (_activeType != '') {
        window.location = _href;
    }

});


$(document).on('click', '#continueSignup', function() {

    var _href = '';

    if (_activeType == 'signupType1') {
        _href = '/EmployerForm';
    } else {
        _href = '/join';
    }

    if (_activeType != '') {
        window.location = _href;
    }

    return false;

});
