$(function() {

    $('#myTab a').click(function(e) {
        e.preventDefault()
        $(this).tab('show')
    });


    $('#datetimepicker1').datetimepicker({
        pickTime: false
    });

    $("#cboSkills").select2({
        placeholder: "Select a Skills",
        initSelection: function(element, callback) {},
        ajax: {
            url: '/API/allskills',
            type: 'POST',
            dataType: 'json',
            data: function(term, page) {
                return {
                    command: 'search_student2',
                    TermID: config.TermID,
                    value: term
                };
            },
            results: function(data, page) {
                var results = [];
                $.each(data.students, function(index, item) {
                    results.push({
                        id: item.StudentID,
                        text: item.Fullname
                    });
                });
                return {
                    results: results
                };
            }
        }
    });

    jQuery('[data-dismiss="modal"]').on('click', function() {
        jQuery('.modal').hide();
        jQuery('.modal-backdrop').hide();
    });

    bindFileUploadCrop('#upload-profile-photo');
    bindFileUploadCrop('.img-rounded');
    parsleyLeftload();

});

function blureffect(obj) {
    $(obj).on('focus', function() {
        $(obj).removeClass('scripterror');

    });
}


function saveUserPersonalInfo() {
    if (true === $('#accountdetails').parsley().validate('block-account')) {

        $('#overlay').fadeIn(800);

        var users = new Object();
        users.js_lastname = $('#LastName').val();
        users.js_firstname = $('#FirstName').val();
        users.js_middleinitial = $('#middleInitial').val();
        users.js_gender = $('#gender').val();
        // users.js_birthdate = $('#datetimepicker1').data("DateTimePicker").getDate();
        users.js_birthdate = $('#datetimepicker1').find('input').val();
        users.u_email = $('#emailInput').val();
        users.u_address = $('#address').val();
        users.u_city = $('#city').val();
        users.u_province = $('#stateInput').val();
        users.u_zipcode = $('#zipCode').val();
        users.u_telephoneno = $('#telephoneNo').val();
        users.u_mobileno = $('#mobileNo').val();
        users.u_fax = $('#faxNo').val();
        users.u_website = $('#inputWebsite').val();

        $.ajax({
            type: 'PUT',
            url: 'api/users',
            data: {
                command: 'account',
                users: users
            },
            dataType: 'json',
            success: function(data) {
                if (data.success === true) {
                    MessageBox('success',data.msg,true);
                    $('#overlay').fadeOut(800);
                } else {
                    $('#overlay').fadeOut(800);
                    if (data.field) {
                        $(data.field).next('ul').html(data.fieldError);
                        MessageBox('error',data.msg);
                        return false;
                    }
                    MessageBox('error',data.msg);
                    return false;
                }
            }
        });
    }
}

function saveUserPassword() {

    if (true === $('#passwordchange').parsley().validate('block-account')) {

        $('#overlay').fadeIn(800);

        var users = new Object();
        users.password = $('#password').val();
        users.newpassword = $('#newPasswordInput').val();
        users.confirm_password = $('#verifyPasswordInput').val();

        $.ajax({
            type: 'PUT',
            url: 'api/users',
            data: {
                command: 'password',
                users: users
            },
            dataType: 'json',
            success: function(data) {
                if (data.success === true) {
                    MessageBox('success',data.msg,true);
                    $('#overlay').fadeOut(800);
                } else {
                    $('#overlay').fadeOut(800);

                    if (data.field) {
                        $(data.field).next('ul').html(data.fieldError);
                        MessageBox('error',data.msg);
                        return false;
                    }
                    MessageBox('error',data.msg);
                    return false;
                }
            }
        });
    }

    blureffect('#password');
    blureffect('#newPasswordInput');
    blureffect('#verifyPasswordInput');
}

function saveUserNotification() {
    // if (true === $('#notifications').parsley().validate('block-account')) {

    // }
}


function bindFileUploadCrop(photoid) {
    // var btnUpload = $('#upload-profile-photo');
    var btnUpload = $(photoid);
    new AjaxUpload(btnUpload, {
        action: 'api/uploadpic',
        name: 'qqfile',
        responseType: 'json',
        onSubmit: function(file, ext) {
            if (!(ext && /^(jpg|png|jpeg|gif)$/.test(ext))) {
                // extension is not allowed
                alert('Only JPG, PNG or GIF files are allowed');
                return false;
            }
            $('#overlay').fadeIn(800);
        },
        onComplete: function(file, response) {
            if (response.msg) {
                MessageBox('success',response.msg,true);
            } else {
                MessageBox('error',response.msg);
            }
        }
    });
    $('#ajax_upload_file_swer').mouseover(function() {
        $('.profile-picture-wrapper').addClass('active');
    });
    $('#ajax_upload_file_swer').mouseout(function() {
        $('.profile-picture-wrapper').removeClass('active');
    });


}

function delete_logo() {
    $('#overlay').fadeIn(800);
    var params = {
        cmd: 'delete_logo'
    };
    $.ajax({
        type: 'DELETE',
        url: 'api/uploadpic',
        data: params,
        dataType: 'json',
        success: function(data) {
            $('#overlay').fadeOut(800);
            if (data.success) {
                if (data.msg != '') {
                    MessageBox('success',data.msg,true);
                    return false;
                }
                MessageBox('success',data.msg,true);
            } else {
                if (data.msg != '') {
                    MessageBox('success',data.msg,true);
                    return false;
                }
            }
        }
    });

}


function FormAdd(lobj) {
    $('.' + lobj + '-form').append($('.' + lobj + '-form-hidden').html()).find('.form-active').fadeIn(400).removeClass('form-active');
}

function RForm(obj, lobj) {
    var k = $('.' + lobj + '-form .' + lobj + '-area').length;
    if (k > 1) {
        $(obj).parent().parent().parent().fadeOut(400, function() {
            $(this).remove()
        });
    }
}
