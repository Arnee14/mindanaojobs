<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Profile extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;


	protected $fillable = array('provider', 'user_id');

    public function user() {
        return $this->belongsTo('User');
    }


	public function profiles() {
	    return $this->hasMany('Profile');
	}

}

?>
