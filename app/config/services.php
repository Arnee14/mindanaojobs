<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Third Party Services
	|--------------------------------------------------------------------------
	|
	| This file is for storing the credentials for third party services such
	| as Stripe, Mailgun, Mandrill, and others. This file provides a sane
	| default location for this type of information, allowing packages
	| to have a conventional place to find your various credentials.
	|
	*/

	'mailgun' => array(
		'domain' => 'sandboxfcbb916b2714462fafb4848ad489aedc.mailgun.org',
		'secret' => 'key-9fc41d653fa0eed2e7b3ee5b20f1fe6a',
	),

	'mandrill' => array(
		'secret' => 'p7J5LlhKVF0IsG_1aZUNVg',
	),

	'stripe' => array(
		'model'  => 'User',
		'secret' => '',
	),

);
