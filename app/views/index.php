<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
  <meta charset="utf-8">
  <title>Hire and Find Jobs Today | MindanaoJobs.com</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="keywords" content="" />
  <meta name="author" content="">


  <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />

  <!-- Bootstrap core CSS -->
  <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
  <!-- Font Awesome -->
  <link href="bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet">
  <!-- Jasny Bootstrap -->
  <link href="bower_components/jasny-bootstrap/dist/css/jasny-bootstrap.min.css" rel="stylesheet">
  <!-- Select2 -->
  <link href="bower_components/select2/select2.css" rel="stylesheet">
  <!-- Select2 Bootstrap 3 -->
  <link href="bower_components/select2-bootstrap-css/select2-bootstrap.css" rel="stylesheet">
  <!-- Date Picker -->
  <link rel="stylesheet" href="bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />



  <!-- Mindanao Jobs -->
  <?php if (isset($page_css)){
    foreach ($page_css as $key) {
      echo '<link href="'.$key.'" rel="stylesheet">';
      echo "\r\n";
    }
  } ?>

</head>

<body class="overflow-hidden">

  <!-- Overlay Div -->
  <div id="overlay" style="display:none;">
      <div class="overlay-inner">
      </div>
  </div>

  <div class="wrapper preload">
      <?php echo $header; ?>

      <?php echo $main; ?>

      <?php echo $footer;?>

      <div class="modal fade" id="confirmDeleteJob">
          <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-body text-center">
                    <div class="font-30 font-lato font-ultra-bold">Confirm Delete</div>

                    <p class="font-16 m-top-20 line-32">
                      Are you sure want to delete this Job Posting?
                    </p>

                    <div class="text-center m-top-30">
                      <button type="button" class="btn btn-danger  m-bottom-5" data-dismiss="modal">Cancel</button>
                      <button type="button" class="btn btn-success  m-bottom-5" onclick="confirmactionSuccess();">Delete Posting</button>
                    </div>
                  </div>
              </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
      </div>
  </div>

  <!-- Jquery -->
  <script type="text/javascript" src="bower_components/jquery/dist/jquery.min.js"></script>
  <!-- Bootstrap -->
  <script type="text/javascript" src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- Jasny Bootstrap -->
  <script type="text/javascript" src="bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js"></script>
  <!-- Select2 -->
  <script type="text/javascript" src="bower_components/select2/select2.js"></script>
  <!-- Moment JS -->
  <script type="text/javascript" src="bower_components/moment/min/moment.min.js"></script>
  <!-- Date Picker -->
  <script type="text/javascript" src="bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
  <!-- pnotify -->
  <script type="text/javascript" src="bower_components/noty/js/noty/packaged/jquery.noty.packaged.min.js"></script>


  <!-- Mindanao Jobs -->
  <script type="text/javascript" src="js/parsley.js"></script>

  <?php if (isset($page_js)){
    foreach ($page_js as $key) {
      echo '<script type="text/javascript" src="'.$key.'"></script>';
      echo "\r\n";
    }
  } ?>

</body>

</html>
