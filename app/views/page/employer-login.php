<div class="main-container">
    <?php if(isset($error) && !empty($error)){?>
    <div class="alert alert-danger alert-dismissible fade in" role="alert" style="z-index:99;position:fixed;width:100%;text-align: center;">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">Ã—</span><span class="sr-only">Close</span></button>
        <h4><?php echo $error['title'];?></h4>
        <p><?php echo $error['message'];?></p>
    </div>
    <?php } ?>
    <div class="container">
        <div class="signup-widget widget-sm">
            <div class="text-center">
                <div class="font-30 font-lato">
                    <span class="font-ultra-bold">EMPLOYER Account</span>
                </div>
            </div>
            <form role="form" action="/Employer" method="post" class="m-top-md" data-parsley-validate novalidate>
                <div class="form-group has-feedback feedback-left" style="float:left;">
                    <label class="font-normal">Employer Login ID:</label>
                    <input class="form-control custom-input" type="email" name="username" id="username" data-parsley-group="block1" data-parsley-required-message="The email address you entered has invalid" required value="" size="40">
                    <span class="fa fa-edit fa-lg form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback feedback-left" style="float:left;">
                    <label class="font-normal">Password:</label>
                    <input class="form-control custom-input" type="password" name="password" id="password" size="40" data-parsley-group="block1" data-parsley-required-message="The password you entered has invalid" required />
                    <span class="fa fa-lock fa-lg form-control-feedback"></span>
                </div>

                <div class="m-top-40 text-center">
                    <button type="submit" class="btn btn-lg btn-primary btn-wide">Login <i class="fa fa-angle-double-right"></i>
                    </button>
                    <div class="font-12 m-top-20">forgot <a href="/recovery">password</a>?</div>
                </div>


            </form>
        </div>
    </div>
    <!-- ./container -->
</div>
<!-- ./main-container -->
