<div class="main-container">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="sidebar-menu bg-grey">
                    <ul>
                        <li class="active">
                            <a href="javascript:void(0);" onclick="messages('inbox')"><i class="fa fa-inbox m-right-xs"></i>Inbox</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);" onclick="messages('sent')"><i class="fa fa-send m-right-xs"></i>Sent</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);" onclick="messages('archived')"><i class="fa fa-archive m-right-xs"></i>Archive</a>
                        </li>
                    </ul>
                </div>
                <!-- ./sidebar-menu -->
            </div>
            <!-- ./col -->
            <div class="col-md-9" id="result_message">

                <div class="row m-top-40">
                    <div class="col-md-6">
                        <div class="font-lato font-30 font-ultra-bold">Messages</div>
                    </div>
                    <!-- ./col -->
                    <div class="col-md-6 text-right text-left-xs"> <a class="btn btn-primary btn-sm m-right-10" href="javascript:void(0)" onclick="messages('inbox')">Refresh</a>
                        <div class="btn-group"> <a href="#" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown"> More <span class="caret"></span> </a>
                            <ul class="dropdown-menu animated-dropdown" role="menu">
                                <li><a href="javascript:void(0)" onclick="message_action(1)">Mark as Unread</a>
                                </li>
                                <li><a href="javascript:void(0)" onclick="message_action(2)">Archive</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- ./col -->
                </div>
                <div class="message-center-body m-top-30">

                    <div class="message-empty">You have no messages.</div>

                    <!-- ./message-list -->
                </div>

            </div>
            <!-- col -->
        </div>
        <!-- ./row -->
    </div>
    <!-- ./container -->
</div>


<input type="hidden" id="type" value="inbox" />
