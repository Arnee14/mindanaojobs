<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta charset="utf-8">
		<title>Hire and Find Jobs Today | MindanaoJobs.com</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="keywords" content="" />
		<meta name="author" content="">
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
		<style>
			@import url(//fonts.googleapis.com/css?family=Lato:700);
		</style>
	</head>
	<body style="margin:0;font-family:'Lato', sans-serif;text-align:center;color: #000;padding-top: 20px;padding-bottom: 20px;">
		<div style="
			width: 775px;
			padding-right: 15px;
			padding-left: 15px;
			margin-right: auto;
			margin-left: auto;
			">
			<div style="padding-right: 60px;padding-left: 60px;border-radius: 6px;text-align:left;border-bottom: 1px solid #e5e5e5;padding: 30px;margin-bottom: 30px;color: inherit;background-color: #fff;border: 5px solid #eee;">
				<img src="http://s2.postimg.org/p0frb7ug5/logo.png">
				<br><br><br>
				<p style="margin-bottom: 15px;font-size: 14px;font-weight: 200;">
				{{ $messages }}
				</p>
				<br><br><br>
				<p style="text-align:center;margin-bottom: 15px;font-size: 14px;font-weight: 300;">
					---------- Sent via MindanaoJobs contact ----------
				</p>
				<p style="text-align:center;margin-bottom: 15px;font-size: 12px;font-weight: 300;">
					Contact us at <a href="mailto:support@mindanaojobs.com">support@mindanaojobs.com</a> or at 123-4567-890.
				</p>
			</div>
		</div>
	</body>
</html>

