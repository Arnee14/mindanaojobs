<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Carbon\Carbon;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	protected $fillable = array('u_email','u_password','isactive','u_accounttype');
	protected $table = 'users';
	protected $primaryKey = "u_id";
	protected $hidden = array('u_password');


	public function getAuthIdentifier()
	{
		return $this->getKey();
		// return $this->u_email;
	}

	public function getAuthPassword()
	{
		return $this->u_password;
	}

	public function getReminderEmail()
	{
		return $this->u_email;
	}

	public function getRememberToken()
	{
	    return $this->remember_token;
	}

	public function setRememberToken($value)
	{
	    $this->remember_token = $value;
	}

	public function getRememberTokenName()
	{
	    return 'remember_token';
	}











	public static function saveUser($data){
		$results = DB::table('users')->insert($data);
		if($results){
			$result['success'] = true;
			$result['msg'] = 'Record Successfully Saved';
		}else{
			$result['success'] = false;
			$result['msg'] = 'WARNING: Unknown error occur while saving the record';
		}
		return $result;
	}

	public static function updateUser($u_id,$data){
		$results = DB::table('users')->where('u_id', $u_id)->update($data);
		if($results){
			$result['success'] = true;
			$result['msg'] = 'Record Successfully Updated';
		}else{
			$result['success'] = false;
			$result['msg'] = 'WARNING: Unknown error occur while saving the record';
		}
		return $result;
	}


	public static function saveJobSeeker($data){
		$results = DB::table('jobseekers')->insert($data);
		if($results){
			$result['success'] = true;
			$result['msg'] = 'Record Successfully Saved';
		}else{
			$result['success'] = false;
			$result['msg'] = 'WARNING: Unknown error occur while saving the record';
		}
		return $result;
	}

	public static function updateJobSeeker($js_id,$data){
		$results = DB::table('jobseekers')->where('js_id', $js_id)->update($data);
		if($results){
			$result['success'] = true;
			$result['msg'] = 'Record Successfully Updated';
		}else{
			$result['success'] = false;
			$result['msg'] = 'WARNING: Unknown error occur while saving the record';
		}
		return $result;
	}

	public static function insertJobSeekerEducation($js_id,$data){
		DB::table('education')->where('js_id', $js_id)->delete();
		$results = DB::table('education')->insert($data);
		if($results){
			$result['success'] = true;
			$result['msg'] = 'Record Successfully Saved';
		}else{
			$result['success'] = false;
			$result['msg'] = 'WARNING: Unknown error occur while saving the record';
		}
		return $result;
	}

	public static function insertJobSeekerProfession($js_id,$data){
		DB::table('work_experience')->where('js_id', $js_id)->delete();
		$results = DB::table('work_experience')->insert($data);
		if($results){
			$result['success'] = true;
			$result['msg'] = 'Record Successfully Saved';
		}else{
			$result['success'] = false;
			$result['msg'] = 'WARNING: Unknown error occur while saving the record';
		}
		return $result;
	}

	public static function insertJobSeekerSkills($js_id,$data){
		$results = DB::table('skills')->where('js_id', $js_id)->delete();
		$results = DB::table('skills')->insert($data);
		if($results){
			$result['success'] = true;
			$result['msg'] = 'Record Successfully Updated';
		}else{
			$result['success'] = false;
			$result['msg'] = 'WARNING: Unknown error occur while saving the record';
		}
		return $result;
	}

	public static function insertJobSeekerCategories($js_id,$data){
		$results = DB::table('jobs_categories')->where('js_id', $js_id)->delete();
		$results = DB::table('jobs_categories')->insert($data);
		if($results){
			$result['success'] = true;
			$result['msg'] = 'Record Successfully Updated';
		}else{
			$result['success'] = false;
			$result['msg'] = 'WARNING: Unknown error occur while saving the record';
		}
		return $result;
	}











	public static function signUpUser($data){

		$checkEmail = DB::select('SELECT * FROM users WHERE u_email = ? AND u_accounttype = ?',array($data['u_email'],$data['u_accounttype']));

		$result = array();
		if ($checkEmail) {
			$result['field'] = '#email';
			$result['fieldError'] = '<li class="parsley-required">This email is already in use. <a href="/login"  class="ParsleyErrorHref"> Login here.</a></li>';
			$result['success'] = 'false';
			$result['msg'] = 'WARNING:Please use another email address.Email Adress already Exist!';
		}else{
			$token = bin2hex(mcrypt_create_iv(16, MCRYPT_DEV_RANDOM));
			$users = array('u_email' => $data['u_email'], 'u_password' => Hash::make($data['u_password']),'u_classification'=>'jobseeker','u_accounttype'=>$data['u_accounttype'],'u_verificationurl'=>$token,'u_isverified'=>0,'isactive'=>1,'isadmin'=>0,'u_createdon'=>Carbon::now());
			$u_id = DB::table('users')->insertGetId($users);

			$results = DB::table('jobseekers')->insert(array('js_lastname' => $data['js_lastname'],'js_firstname'=>$data['js_firstname'],'u_id'=>$u_id));

			if($results){
				$result['success'] = 'true';
				$result['msg'] = 'Record Successfully Saved';
			}else{
				$result['success'] = 'false';
				$result['msg'] = 'WARNING: Unknown error occur while saving the record';
			}

			$fullname =  $data['js_firstname'].' '.$data['js_lastname'];
			$email =  $data['u_email'];
			$info = array('email'=>$email,'fullname'=>$fullname,'token'=>$token,'mode'=>'j');

			Mail::send('emails.confirmation',$info, function($message)use($email,$fullname){
			    $message->to($email,$fullname);
			    $message->from('no-reply@mydomain.com','Mindanao Jobs');
			    $message->subject('Account Confirmation');
			});
		}
		return json_encode($result);
	}

	public static function updateUserAccount($u_id,$data){
		$users = array($data['u_email'],$data['u_address'],$data['u_city'],$data['u_province'],$data['u_zipcode'],$data['u_telephoneno'],$data['u_mobileno'],$data['u_fax'],$data['u_website'],
			$data['js_lastname'],$data['js_firstname'],$data['js_middleinitial'],$data['js_gender'],$data['js_birthdate'],$u_id);

		$results = DB::update('UPDATE users U INNER JOIN jobseekers J ON U.u_id=J.u_id SET U.u_email=?,U.u_address=?,
			U.u_city=?,U.u_province=?,U.u_zipcode=?,U.u_telephoneno=?,U.u_mobileno=?,U.u_fax=?,U.u_website=?,J.js_lastname=?,
			J.js_firstname=?,J.js_middleinitial=?,J.js_gender=?,J.js_birthdate=? WHERE U.u_id=?',$users);

		if($results){
			$result['success'] = true;
			$result['msg'] = 'Record Successfully Saved';
		}else{
			$result['success'] = false;
			$result['msg'] = 'WARNING: Unknown error occur while saving the record';
		}
		return $result;
	}


	public static function updateUserPassword($id,$data){

		if($data['newpassword'] != $data['confirm_password']){
			/*$result['field'] = '#newPasswordInput';
			$result['fieldError'] = '<li class="parsley-required">New password does not match with Verify password</li>';*/
			$result['success'] = false;
			$result['msg'] = 'WARNING: New password does not match with Verify password!';
			return json_encode($result);
		}

		$user = DB::select('SELECT * FROM users WHERE u_id = ? LIMIT 1;', array($id));
		if (Hash::check($data['password'],$user[0]->u_password)){

			$arr = array(Hash::make($data['newpassword']),$id);

			$qry_results = DB::update('UPDATE users SET u_password=? WHERE u_id = ?', $arr);
			if($qry_results){
				$result['success'] = true;
				$result['msg'] = 'User password successfully changed';
			}else{
				$result['success'] = false;
				$result['msg'] = 'WARNING: Unknown error occur while changing password!';
			}
		}else{
			/*$result['field'] = '#password';
			$result['fieldError'] = '<li class="parsley-required">Your Current password does not match</li>';*/
			$result['success'] = false;
			$result['msg'] = 'WARNING: Your Current password does not match!';

		}
		return json_encode($result);
	}


	public static function uploadPic($u_id,$file) {
		$destinationPath = 'public/images/users/';
		$extension =$file->getClientOriginalExtension();
		$fileName = '../images/users/'.$u_id .".".$extension;
		$file->move($destinationPath, $fileName);

		$info = array($fileName,$u_id);

		$results = DB::update('UPDATE users u SET u.u_displaypicture = ? Where u.u_id = ?', $info);

		if($results){
			$result['img1'] = $destinationPath.$fileName;
			$result['img'] = $destinationPath.$fileName;
			$result['success'] = true;
			$result['msg'] = 'Record Successfully Saved';
		}else{
			$result['success'] = false;
			$result['msg'] = 'WARNING: Unknown error occur while saving the record';
		}
		return $result;
	}

	public static function uploadResume($u_id,$file) {
		$destinationPath = 'public/resume/';
		$extension =$file->getClientOriginalExtension();
		$fileName = $u_id .".".$extension;
		$upload_success = $file->move($destinationPath, $fileName);
		$info = array($fileName,$u_id);
		DB::update('UPDATE users u SET u.u_resume = ? Where u.u_id = ?', $info);
		if($upload_success){
			$result['success'] = true;
			$result['msg'] = 'Record Successfully Saved';
		}else{
			$result['success'] = false;
			$result['msg'] = 'WARNING: Unknown error occur while saving the record';
		}
		return $result;
	}

	public static function confirmAccount($u){
		$users = DB::select('SELECT * FROM users WHERE u_verificationurl = ? LIMIT 1;',array($u));
		$data = json_decode(json_encode((array) $users), true);
		if (isset($users) && !empty($users)){
			$check = DB::select('SELECT * FROM users u WHERE u.u_id = ? AND u.u_isverified = "1" LIMIT 1;',array($data[0]['u_id']));
			if($check){
				return null;
			}else{
				DB::update('UPDATE users u SET u.u_isverified = "1" WHERE u.u_id = ?',array($data[0]['u_id']));
				return $data;
			}
		}
	}









	public static function getCurrentUser($u_id){
		return $results = DB::select('SELECT U.*,J.*,CONCAT(J.js_firstname," ",J.js_lastname) AS Fullname FROM users U INNER JOIN jobseekers J ON U.u_id = J.u_id WHERE U.u_id = ?', $u_id);
	}

	public static function getJobSeekerEducation($js_id){
		return $results = DB::select('SELECT * FROM education WHERE js_id=?;',$js_id);
	}

	public static function getJobSeekerProfession($js_id){
		return $results = DB::select('SELECT * FROM work_experience WHERE js_id=?;',$js_id);
	}

	public static function getJobSeekerSkills($js_id){
		return $results = DB::select('SELECT * FROM skills WHERE js_id=?;',$js_id);
	}

	public static function getJobSeekerCategories($js_id){
		return $results = DB::select('SELECT * FROM jobs_categories WHERE js_id=?;',$js_id);
	}

	public static function getAllUser(){
		return $results = DB::select('SELECT * FROM users;');
	}









	public static function findUser($u_id){
		return $results = DB::select('SELECT * FROM users WHERE u_id = ?', $u_id);
	}

	public static function deleteUserByID($u_id){
		return $results = DB::select('DELETE FROM users WHERE u_id = ?', $u_id);
	}

	public static function findUserByValue($value){
		return $results = DB::select('SELECT * FROM users WHERE u_id = ?', $u_id);
	}


}
