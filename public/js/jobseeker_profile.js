$(function() {
    var arr = new Array();

    // DATE PICKER
    $('#datetimepicker1').datetimepicker({
        pickTime: false
    });

    // PROFILE TABS
    $('#myTab a').click(function(e) {
        e.preventDefault()
        $(this).tab('show')
    });

    // JOB SEEKER SKILL AUTOCOMPLETE
    $.get("api/findSkills", function(data) {
        var results = [];
        $.each(data.Skills, function(index, item) {
            results.push({
                id: item.skills_name,
                text: item.skills_name
            });
        });
        $('#cboSkills').select2({
            data: results,
            multiple: true
        });
    });

    $.get("api/jobseeker", function(data) {
        var results = [];
        $.each(data, function(index, item) {
            results.push({
                id: item.s_name,
                text: item.s_name
            });
        });
        $('#cboSkills').select2("data", results);
    });

    var k = $('.education-form .education-area').length;
    if (k == 0) {
        FormAdd('education')
        $('.education-form .rm-education').hide();
    }

    var a = $('.profession-form .profession-area').length;
    if (a == 0) {
        FormAdd('profession')
        $('.profession-form .rm-profession').hide();
    }


    bindFileUploadCustom();


    $('#editCategories').on('hidden.bs.modal', function(e) {
        // do something...
    });

    $('.resume').click(function() {
        $.fileDownload('api/uploadresume/' + $(this).text(), {
            successCallback: function(url) {
                MessageBox('error','You just got a file download dialog or ribbon for this URL :' + url);
            },
            failCallback: function(html, url) {
                MessageBox('error','Your file download just failed for this URL:' + url + '\r\n' +
                    'Here was the resulting error HTML: \r\n' + html);
            }
        });
    });

    var prevKey = -1, prevControl = '';
    $('input[name="desiredSalary"]').keydown(function (event) {
        if (!(
            event.keyCode == 8 // backspace
            || event.keyCode == 9 // tab
            || event.keyCode == 17 // ctrl
            || event.keyCode == 46 // delete
            || (event.keyCode >= 35 && event.keyCode <= 40) // arrow keys/home/end
            || (event.keyCode >= 48 && event.keyCode <= 57) // numbers on keyboard
            || (event.keyCode >= 96 && event.keyCode <= 105) // number on keypad
            || (event.keyCode == 65 && prevKey == 17 && prevControl == event.currentTarget.id) // ctrl + a, on same control
            || ((event.keyCode == 190 || event.keyCode == 110) && $(this).val().indexOf('.') == -1 && $(this).val() != '') // If '.' and it's not existing
            )
        ) {
            event.preventDefault();     // Prevent character input
        }
        else {
            if((event.keyCode == 96 || event.keyCode == 48) && $(this).val() == '') { // Prevent '.' character if there is no existing value
                event.preventDefault();
            }
            if($(this).val().indexOf('.') != -1) { // Prevent input of the number of decimal places is already 2
                if(
                    $(this).val().substr(parseInt($(this).val().indexOf('.') + 1)).length == 2 &&
                    (
                        (event.keyCode >= 48 && event.keyCode <= 57) ||
                        (event.keyCode >= 96 && event.keyCode <= 105)
                    )
                ) {
                    event.preventDefault();
                }
            }
            prevKey = event.keyCode;
            prevControl = event.currentTarget.id;
        }
    });
});

function saveUserOverview() {

    if (true === $('#userForm').parsley().validate('block-account')) {

        $('#overlay').fadeIn(800);

        var userArr = new Array();
        var users = new Object();
        users.desiredSalary = $('#desiredSalary').val();
        users.qualification = $('#qualification').val();
        userArr.push(users);

        $.ajax({
            type: 'PUT',
            url: 'api/jobseeker',
            data: {
                command: 'overview',
                data: userArr
            },
            dataType: 'json',
            success: function(data) {
                if (data.success === true) {
                    MessageBox('success',data.msg,true);
                    $('#overlay').fadeOut(800);
                } else {
                    $('#overlay').fadeOut(800);
                    MessageBox('error',data.msg);
                    return false;
                }
            }
        });
    }
}

function saveUserSpecialization() {
    $('#overlay').fadeIn(800);
    var arr = [];
    $('.cls_category').each(function() {
        var chk = $(this).find('input.item-check:checkbox:checked').val();
        var spc = new Object();

        if (chk) {
            spc.specialization = $.trim($(this).text());
            spc.value = chk;
            arr.push(spc);
        }
    });

    $.ajax({
        type: 'PUT',
        url: 'api/jobseeker',
        data: {
            command: 'specialization',
            data: arr
        },
        dataType: 'json',
        success: function(data) {
            if (data.success === true) {
                MessageBox('success',data.msg,true);
                $('#overlay').fadeOut(800);
                $('#editCategories').modal('hide');
                jQuery('.modal').hide();
                jQuery('.modal-backdrop').hide();
            } else {
                MessageBox('error',data.msg);
                $('#overlay').fadeOut(800);
                return false;
            }
        }
    });
}

function saveUserSkills() {
    var arrSkills = [];

    $('#overlay').fadeIn(800);

    $.each($("#cboSkills").select2('data'), function() {
        var users = new Object();
        users.s_name = $(this)[0].text;
        arrSkills.push(users);
    });

    $.ajax({
        type: 'PUT',
        url: 'api/jobseeker',
        data: {
            command: 'skills',
            data: arrSkills
        },
        dataType: 'json',
        success: function(data) {
            if (data.success === true) {
                MessageBox('success',data.msg,true);
                $('#overlay').fadeOut(800);
            } else {
                MessageBox('error',data.msg);
                $('#overlay').fadeOut(800);
                return false;
            }
        }
    });
}

function saveUserEducation() {
    if (true === $('#educForm').parsley().validate('block-account')) {
        $('#overlay').fadeIn(800);

        var userArr = new Array();
        $('.education-form .education-area').each(function() {
            var users = new Object();
            users.ed_institution = $(this).find('input[name="universityname[]"]').val();
            users.ed_attainment = $(this).find('input[name="attainment[]"]').val();
            users.ed_field = $(this).find('input[name="degree[]"]').val();
            users.ed_major = $(this).find('input[name="fos[]"]').val();
            users.ed_address = $(this).find('input[name="address[]"]').val();
            users.ed_yearfrom = $(this).find('select[name="sypday[]"]').val();
            users.ed_yearto = $(this).find('select[name="eypday[]"]').val();

            userArr.push(users);
        });

        $.ajax({
            type: 'PUT',
            url: 'api/jobseeker',
            data: {
                command: 'education',
                data: userArr
            },
            dataType: 'json',
            success: function(data) {
                if (data.success === true) {
                    MessageBox('success',data.msg,true);
                    $('#overlay').fadeOut(800);
                } else {
                    MessageBox('error',data.msg);
                    $('#overlay').fadeOut(800);
                    return false;
                }
            }
        });
    }
}

function saveUserWork() {
    if (true === $('#proForm').parsley().validate('block-account')) {
        $('#overlay').fadeIn(800);

        var userArr = new Array();
        $('.profession-form .profession-area').each(function() {
            var users = new Object();
            users.we_companyname = $(this).find('input[name="companyname[]"]').val();
            users.we_position = $(this).find('input[name="title[]"]').val();
            users.we_location = $(this).find('input[name="location[]"]').val();
            // users.we_isactive = $(this).find('input[name="pwork[]"]').val();
            users.we_description = $(this).find('textarea[name="description[]"]').val();
            users.we_yearstart = $(this).find('select[name="sypday[]"]').val();
            users.we_yearend = $(this).find('select[name="eypday[]"]').val();

            userArr.push(users);
        });

        $.ajax({
            type: 'PUT',
            url: 'api/jobseeker',
            data: {
                command: 'profession',
                data: userArr
            },
            dataType: 'json',
            success: function(data) {
                if (data.success === true) {
                    MessageBox('success',data.msg,true);
                    $('#overlay').fadeOut(800);
                } else {
                    MessageBox('error',data.msg);
                    $('#overlay').fadeOut(800);
                    return false;
                }
            }
        });
    }
}

function bindFileUploadCustom() {
    var btnUpload = $('#upload-profile-photos');
    new AjaxUpload(btnUpload, {
        action: 'api/uploadresume',
        name: 'qqfile',
        responseType: 'json',
        onSubmit: function(file, ext) {
            if (!(ext && /^(doc|docx|pdf)$/.test(ext))) {
                // extension is not allowed
                MessageBox('error','Only DOC, DOCX, PDF files are allowed');
                return false;
            }
            $('#overlay').fadeIn(800);
        },
        onComplete: function(file, response) {
            if (response.msg) {
                MessageBox('success',response.msg,true);
            } else {
                MessageBox('error',data.msg);
            }

        }
    });
    $('#ajax_upload_file_swer').mouseover(function() {
        $('.profile-picture-wrapper').addClass('active');
    });
    $('#ajax_upload_file_swer').mouseout(function() {
        $('.profile-picture-wrapper').removeClass('active');
    });

}


function FormAdd(lobj) {
    $('.' + lobj + '-form').append($('.' + lobj + '-form-hidden').html()).find('.form-active').fadeIn(800).removeClass('form-active');
}

function RForm(obj, lobj) {
    var k = $('.' + lobj + '-form .' + lobj + '-area').length;
    if (k >= 1) {
        $(obj).parent().parent().parent().fadeOut(800, function() {
            $(this).remove()
        });
    }
}
