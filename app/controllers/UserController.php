<?php
require 'vendor/autoload.php';
use GuzzleHttp\Client;

class UserController extends BaseController {

	public $base_url = 'http://localhost:3000';

	public function profile(){
		$javascript = array('js/mindanaojobs.js','js/jobseeker_profile.js','js/ajaxupload.js','js/jquery.fileDownload.js');
		$css = array('css/style.css','css/flatui.css','css/parsley.css','css/animation.css');

		$file = file_get_contents('public/json/jobcategories.json');
		$jobcategories = json_decode($file,TRUE);

		$arrYear = array();
		$currYear = date("Y");
		for ($i=$currYear; $i >=1960; $i--) {
			array_push($arrYear,$i);
		}

		$id = Auth::id();
		$currentUser = User::getCurrentUser(array($id));
		$currentUser = $currentUser[0];

		$js_id = $currentUser->js_id;
		$education = User::getJobSeekerEducation(array($js_id));
		$profession = User::getJobSeekerProfession(array($js_id));
		$skills = User::getJobSeekerSkills(array($js_id));
		$categories = User::getJobSeekerCategories(array($js_id));

        $data = [
			'page_js' =>$javascript,
			'page_css' =>$css,
			'jobcategories' => $jobcategories,
			'Years' => $arrYear,
			'User' =>$currentUser,
			'Education'=>$education,
			'Experience' =>$profession,
			'Skills'=>$skills,
			'Categories' => $categories
		];



		View::share($data);
		return View::make('index')
			->nest('header', 'include.header-profile')
			->nest('main', 'page.my-profile')
			->nest('footer', 'include.footer');
	}

	public function my_profile(){
		$javascript = array('js/mindanaojobs.js','js/jobseeker_profile.js','js/ajaxupload.js','js/jquery.fileDownload.js');
		$css = array('css/style.css','css/flatui.css','css/parsley.css','css/animation.css');

		$id = Auth::id();
		$currentUser = User::getCurrentUser(array($id));
		$currentUser = $currentUser[0];

		$js_id = $currentUser->js_id;
		$education = User::getJobSeekerEducation(array($js_id));
		$profession = User::getJobSeekerProfession(array($js_id));
		$skills = User::getJobSeekerSkills(array($js_id));
		$categories = User::getJobSeekerCategories(array($js_id));


        $data = [
			'page_js' =>$javascript,
			'page_css' =>$css,
			'User' =>$currentUser,
			'Education'=>$education,
			'Experience' =>$profession,
			'Skills'=>$skills,
			'Categories' => $categories
		];

		View::share($data);
		return View::make('index')
			->nest('header', 'include.header-profile')
			->nest('main', 'page.view')
			->nest('footer', 'include.footer');
	}

	public function account(){
		$javascript = array('js/ajaxupload.js','js/mindanaojobs.js','js/jobseeker_account.js');
		$css = array('css/style.css','css/flatui.css','css/parsley.css','css/animation.css');


		$file2 = file_get_contents('public/json/location2.json');
		$joblocation2 = json_decode($file2,TRUE);


		$id = Auth::id();
		$currentUser = User::getCurrentUser(array($id));

        $data = [
			'page_js' =>$javascript,
			'page_css' =>$css,
			'joblocation2'=> $joblocation2,
			'User' =>$currentUser[0]
		];

		View::share($data);
		return View::make('index')
			->nest('header', 'include.header-profile')
			->nest('main', 'page.account')
			->nest('footer', 'include.footer');
	}

	public function jobseekerMessage(){
		$javascript = array('js/mindanaojobs.js');
		$css = array('css/style.css','css/flatui.css','css/parsley.css','css/animation.css');

		$file2 = file_get_contents('public/json/location2.json');
		$joblocation2 = json_decode($file2,TRUE);

        $id = Auth::id();
		$currentUser = User::getCurrentUser(array($id));

        $data = [
			'page_js' =>$javascript,
			'page_css' =>$css,
			'joblocation2'=> $joblocation2,
			'User' =>$currentUser[0]
		];

		View::share($data);
		return View::make('index')
			->nest('header', 'include.header-profile')
			->nest('main', 'page.messages')
			->nest('footer', 'include.footer');
	}

	public function jobseekerNotification(){
		$javascript = array('js/mindanaojobs.js');
		$css = array('css/style.css','css/flatui.css','css/parsley.css','css/animation.css');

		$file2 = file_get_contents('public/json/location2.json');
		$joblocation2 = json_decode($file2,TRUE);

        $id = Auth::id();
		$currentUser = User::getCurrentUser(array($id));

        $data = [
			'page_js' =>$javascript,
			'page_css' =>$css,
			'joblocation2'=> $joblocation2,
			'User' =>$currentUser[0]
		];

		View::share($data);
		return View::make('index')
			->nest('header', 'include.header-profile')
			->nest('main', 'page.notifications')
			->nest('footer', 'include.footer');
	}

	public function saveAccountInformation(){
		$data = Input::all();
		if (Request::ajax()){
			$return = User::saveUser($data['users']);
			return $return;
		}
	}

	public function updateUser(){
		$input = Input::all();

		switch ($input['command']) {
			case 'password':
				if (Request::ajax()){
					$id = Auth::id();
					$return = User::updateUserPassword($id,$input['users']);
					return $return;
				}
				break;
			case 'account':
				if (Request::ajax()){
					$id = Auth::id();
					$return = User::updateUserAccount($id,$input['users']);
					return $return;
				}
				break;
			case 'email_notif':

				break;
		}
	}

	public function saveJobSeeker(){
		$data = Input::all();
		if (Request::ajax()){
			$return = User::updateUserPassword($data['users']);
			return $return;
		}
	}

	public function updateJobSeeker(){
		$input = Input::all();

		$id = Auth::id();
		$currentUser = User::getCurrentUser(array($id));
		$currentUser = $currentUser[0];
		$js_id = $currentUser->js_id;


		switch ($input['command']) {
			case 'overview':
					if (Request::ajax()){
						if(isset($input['data']) && !empty($input['data'])){
							$input = $input['data'][0];

							$data = array('js_desiredsalary'=>$input['desiredSalary'],'js_desc'=>$input['qualification']);
							$return = User::updateJobSeeker($js_id,$data);
							return $return;
						}
					}
				break;
			case 'education':
					if (Request::ajax()){
						$data = array();
						if(isset($input['data']) && !empty($input['data'])){
							foreach ($input['data'] as $key) {
								$data[] =  array(
									'js_id' => $js_id,
									'ed_institution'=>$key['ed_institution'],
									'ed_attainment'=>$key['ed_attainment'],
									'ed_field'=>$key['ed_field'],
									'ed_major'=>$key['ed_major'],
									'ed_address'=>$key['ed_address'],
									'ed_yearfrom'=>$key['ed_yearfrom'],
									'ed_yearto'=>$key['ed_yearto']
								);
							}
							$return = User::insertJobSeekerEducation($js_id,$data);
							return $return;
						}else{
							$results=DB::table('education')->where('js_id', $js_id)->delete();
							if($results){
								$result['success'] = true;
								$result['msg'] = 'Record Successfully Updated';
							}else{
								$result['success'] = false;
								$result['msg'] = 'WARNING: Unknown error occur while saving the record';
							}
							return $result;
						}
					}
				break;

			case 'skills':
					if (Request::ajax()){
						$data = array();
						if(isset($input['data']) && !empty($input['data'])){
							foreach ($input['data'] as $key) {
								$data[] =  array(
									'js_id' => $js_id,
									's_name'=>$key['s_name'],
									's_level'=>'',
									'isactive'=>'1',
								);
							}
							$return = User::insertJobSeekerSkills($js_id,$data);
							return $return;
						}else{
							$results = DB::table('skills')->where('js_id', $js_id)->delete();
							if($results){
								$result['success'] = true;
								$result['msg'] = 'Record Successfully Updated';
							}else{
								$result['success'] = false;
								$result['msg'] = 'WARNING: Unknown error occur while saving the record';
							}
							return $result;
						}
					}
				break;
			case 'specialization':
					if (Request::ajax()){
						$data = array();
						if(isset($input['data']) && !empty($input['data'])){
							foreach ($input['data'] as $key) {
								$data[] =  array(
									'js_id' => $js_id,
									'jc_name'=>$key['specialization'],
									'isactive'=>'1',
								);
							}
							$return = User::insertJobSeekerCategories($js_id,$data);
							return $return;
						}else{
							$results = DB::table('jobs_categories')->where('js_id', $js_id)->delete();
							if($results){
								$result['success'] = true;
								$result['msg'] = 'Record Successfully Updated';
							}else{
								$result['success'] = false;
								$result['msg'] = 'WARNING: Unknown error occur while saving the record';
							}
							return $result;
						}
					}
				break;

			case 'profession':
					if (Request::ajax()){
						$data = array();
						if(isset($input['data']) && !empty($input['data'])){
							foreach ($input['data'] as $key) {
								$data[] =  array(
									'js_id' => $js_id,
									'we_companyname'=>$key['we_companyname'],
									'we_position'=>$key['we_position'],
									'we_location'=>$key['we_location'],
									'we_description'=>$key['we_description'],
									'we_yearstart'=>$key['we_yearstart'],
									'we_yearend'=>$key['we_yearend']
								);
							}
							$return = User::insertJobSeekerProfession($js_id,$data);
							return $return;
						}else{
							$results=DB::table('work_experience')->where('js_id', $js_id)->delete();
							if($results){
								$result['success'] = true;
								$result['msg'] = 'Record Successfully Updated';
							}else{
								$result['success'] = false;
								$result['msg'] = 'WARNING: Unknown error occur while saving the record';
							}
							return $result;
						}
					}
				break;
		}

	}

	public function getuploadResume(){
		$file=Route::getCurrentRoute()->getParameter('file');
		$filepath= public_path(). "/resume/".$file;
        $headers = array(
          'Content-Type'=> 'application/pdf',
        );

        return Response::download($filepath, $file, $headers);
	}

	public function getJobSeekerSkills(){
		$id = Auth::id();
		$currentUser = User::getCurrentUser(array($id));
		$currentUser = $currentUser[0];

		$js_id = $currentUser->js_id;
		$skills = User::getJobSeekerSkills(array($js_id));
		return $skills;
	}

	function uploadPic(){
		$file = Input::file('qqfile');

		$id = Auth::id();

		$callback = User::uploadPic($id,$file);
		return $callback;
	}

	function deleteUploadPic(){

	}

	function uploadResume(){
		$file = Input::file('qqfile');

		$id = Auth::id();

		$callback = User::uploadResume($id,$file);
		return $callback;
	}





}
