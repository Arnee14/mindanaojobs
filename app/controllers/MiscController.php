<?php

class MiscController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Miscellaneous Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'MiscController@showWelcome');
	|
	*/

	public function show404(){
		return View::make('errors.missing');
	}

	public function showWelcome()
	{
        $javascript = array('js/joinoption.js','js/jobseeker.js','js/mindanaojobs.js');
        $css = array('css/joinoption.css','css/mindanaojobs.css');

        $data = [
			'page_js' =>$javascript,
			'page_css' =>$css
		];

		return View::make('hello')->with($data);
	}

	public function showEmail()
	{
        $data = array('email'=>'philipgaray2@gmail','fullname'=>'Philip Cesar','mode'=>'b','token'=>'Lorem ipsum dolor sit amet','messages'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.');

		return View::make('emails.reset')->with($data);
	}

	public function showHome(){
		$javascript = array('js/mindanaojobs.js');
		$css = array('css/style.css','css/flatui.css','css/parsley.css','css/animation.css');

		$error = Session::get('error');

		$data = [
			'page_js' =>$javascript,
			'page_css' =>$css,
			'error' =>$error
		];

		if (Auth::check()) {
			$id = Auth::id();
			$currentUser = User::getCurrentUser(array($id));

			if (isset($currentUser) && !empty($currentUser)){
				$data['User'] = $currentUser[0];
			}

			$currentEmployer = Employer::getCurrentEmployer(array($id));
			if (isset($currentEmployer) && !empty($currentEmployer)){
				$data['Employer'] = $currentEmployer[0];
			}

			View::share($data);
	        return View::make('index')
				->nest('header', 'include.header-profile')
				->nest('main', 'page.home-main')
				->nest('footer', 'include.footer');
	    }else{
	    	View::share($data);
	    	return View::make('index')
				->nest('header', 'include.header-login')
				->nest('main', 'page.home-main')
				->nest('footer', 'include.footer');
	    }
	}

	public function showAboutUs(){
		$javascript = array('js/mindanaojobs.js');
		$css = array('css/style.css','css/flatui.css','css/parsley.css','css/animation.css');

        $data = [
			'page_js' =>$javascript,
			'page_css' =>$css
		];

		if (Auth::check()) {
			$id = Auth::id();
			$currentUser = User::getCurrentUser(array($id));
			if (isset($currentUser) && !empty($currentUser)){
				$data['User'] = $currentUser[0];
			}

			$currentEmployer = Employer::getCurrentEmployer(array($id));
			if (isset($currentEmployer) && !empty($currentEmployer)){
				$data['Employer'] = $currentEmployer[0];
			}

			View::share($data);
	        return View::make('index')
				->nest('header', 'include.header-profile')
				->nest('main', 'page.aboutus')
				->nest('footer', 'include.footer');
	    }else{

	    	View::share($data);
	    	return View::make('index')
				->nest('header', 'include.header-login')
				->nest('main', 'page.aboutus')
				->nest('footer', 'include.footer');
	    }
	}

	public function showTerms(){
		$javascript = array('js/mindanaojobs.js');
		$css = array('css/style.css','css/flatui.css','css/parsley.css','css/animation.css');

        $data = [
			'page_js' =>$javascript,
			'page_css' =>$css
		];

		if (Auth::check()) {
			$id = Auth::id();
			$currentUser = User::getCurrentUser(array($id));
			if (isset($currentUser) && !empty($currentUser)){
				$data['User'] = $currentUser[0];
			}

			$currentEmployer = Employer::getCurrentEmployer(array($id));
			if (isset($currentEmployer) && !empty($currentEmployer)){
				$data['Employer'] = $currentEmployer[0];
			}

			View::share($data);
	        return View::make('index')
				->nest('header', 'include.header-profile')
				->nest('main', 'page.terms')
				->nest('footer', 'include.footer');
	    }else{

	    	View::share($data);
	    	return View::make('index')
				->nest('header', 'include.header-login')
				->nest('main', 'page.terms')
				->nest('footer', 'include.footer');
	    }
	}

	public function showPrivacy(){
		$javascript = array('js/mindanaojobs.js');
		$css = array('css/style.css','css/flatui.css','css/parsley.css','css/animation.css');

        $data = [
			'page_js' =>$javascript,
			'page_css' =>$css
		];

		if (Auth::check()) {
			$id = Auth::id();
			$currentUser = User::getCurrentUser(array($id));
			if (isset($currentUser) && !empty($currentUser)){
				$data['User'] = $currentUser[0];
			}

			$currentEmployer = Employer::getCurrentEmployer(array($id));
			if (isset($currentEmployer) && !empty($currentEmployer)){
				$data['Employer'] = $currentEmployer[0];
			}

			View::share($data);
	        return View::make('index')
				->nest('header', 'include.header-profile')
				->nest('main', 'page.privacy')
				->nest('footer', 'include.footer');
	    }else{

	    	View::share($data);
	    	return View::make('index')
				->nest('header', 'include.header-login')
				->nest('main', 'page.privacy')
				->nest('footer', 'include.footer');
	    }
	}

	public function showJoinOption(){
		$javascript = array('js/joinoption.js','js/mindanaojobs.js');
		$css = array('css/style.css','css/flatui.css','css/parsley.css','css/animation.css');

        $data = [
			'page_js' =>$javascript,
			'page_css' =>$css
		];

		if (Auth::check()) {
	        return Redirect::to('/joblist');
	    }

		View::share($data);
		return View::make('index')
			->nest('header', 'include.header-login')
			->nest('main', 'page.joinoption')
			->nest('footer', 'include.footer');
	}

	public function showJoin(){
		$javascript = array('js/registration.js','js/mindanaojobs.js');
		$css = array('css/style.css','css/flatui.css','css/parsley.css','css/animation.css');

        $data = [
			'page_js' =>$javascript,
			'page_css' =>$css
		];

		if (Auth::check()) {
	        return Redirect::to('/joblist');
	    }

	    View::share($data);
		return View::make('index')
			->nest('header', 'include.header-login')
			->nest('main', 'page.join')
			->nest('footer', 'include.footer');
	}

	public function showEmployerJoin(){
		$javascript = array('js/mindanaojobs.js','js/employerregistration.js');
		$css = array('css/style.css','css/flatui.css','css/parsley.css','css/animation.css');

		$file2 = file_get_contents('public/json/location2.json');
		$joblocation2 = json_decode($file2,TRUE);

		// echo '<pre>';
		// print_r($joblocation2);die();

        $data = [
			'page_js' =>$javascript,
			'page_css' =>$css,
			'joblocation2' => $joblocation2
		];

		if (Auth::check()) {
	        return Redirect::to('/EmployerProfile');
	    }

	    View::share($data);
		return View::make('index')
			->nest('header', 'include.header-login')
			->nest('main', 'page.employerForm')
			->nest('footer', 'include.footer')->with($data);
	}

	public function showPasswordRecovery(){
		$javascript = array('js/mindanaojobs.js','js/recover.js');
		$css = array('css/style.css','css/flatui.css','css/parsley.css','css/animation.css');

		$mode = Input::get('mode');
		$id = Input::get('id');

        $data = [
			'page_js' =>$javascript,
			'page_css' =>$css,
			'mode'=>$mode,
			'id'=>$id

		];

		$mode = Input::get('mode');
		$id = Input::get('id');

		if((isset($mode) && !empty($mode)) && (isset($id) && !empty($id)) && (strlen($id) == 32)){

			$check = DB::select('SELECT * FROM users WHERE u_recoveryurl = ? LIMIT 1;',array($id));
			if (isset($check) && !empty($check)){
				$users = DB::select('SELECT * FROM users WHERE u_recoveryurl = ? AND u_isrecover = "1" LIMIT 1;',array($id));
				$result = json_decode(json_encode((array) $users), true);
				if (isset($users) && !empty($users)){
					return Redirect::to('/');
				}else{

					View::share($data);
			    	return View::make('index')
						->nest('header', 'include.header-login')
						->nest('main', 'page.recovery2')
						->nest('footer', 'include.footer');
				}
			}else{
				$error['title']='Oh snap! You got an error!';
				$error['message']='WARNING: Token was already been expired!';
				return Redirect::to('/')->with('error',$error);
			}
		}else{
				View::share($data);
		    	return View::make('index')
					->nest('header', 'include.header-login')
					->nest('main', 'page.recovery')
					->nest('footer', 'include.footer');
		}

	}

	public function accountLogout(){
		Auth::logout();
		return Redirect::to('/');
	}








	public function getAllJobLocation(){
		$file = file_get_contents('public/json/location.json');
		$joblocation = json_decode($file,TRUE);

		return $joblocation;
	}

	public function getAllJobLocation2(){
		$file = file_get_contents('public/json/location2.json');
		$joblocation2 = json_decode($file,TRUE);

		return $joblocation2;
	}

	public function getAllJobCategories(){
		$file = file_get_contents('public/json/jobcategories.json');
		$jobcategories = json_decode($file,TRUE);

		return $jobcategories;
	}

	public function getAllSkills(){
		$file = file_get_contents('public/json/skills.json');
		$skills = json_decode($file,TRUE);

		return $skills;
	}

	public function getAllMonths(){
		$file = file_get_contents('public/json/month.json');
		$month = json_decode($file,TRUE);

		return $month;
	}

	public function findAllSkills(){
		$data = Input::all();

		$file = file_get_contents('public/json/skills.json');
		$skills = json_decode($file,TRUE);
		return $skills;
	}

}

?>
