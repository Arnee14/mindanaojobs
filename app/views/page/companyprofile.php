<div class="main-container no-bottom-space">
    <div class="section section-border-bottom paddingB-40 bg-emerald" id="overviewSection">
        <div class="container">
            <div class="paddingTB-40">
                <div class="row">
                    <div class="col-md-2">
                        <div class="clearfix">
                            <img src="../img/photo.jpg" alt="" class="applicant-profile-pic" style="border-radius:50em">
                        </div>
                    </div>
                    <div class="col-md-6 m-top-20-sm">
                        <div class="font-lato font-30">{{companyProfile[0].emp_id.name}}</div>
                        <div class="m-top-5 font-semi-bold">Programmer - Web Developer - Mobile Developer</div>
                        <ul class="list-style-icon m-top-40">
                            <li class="m-bottom-15">
                                <div class="list-icon text-center">
                                    <i class="fa fa-map-marker fa-lg"></i>
                                </div>
                                <div class="list-content font-semi-bold ">{{companyProfile[0].emp_id.address}}</div>
                            </li>
                            <li class="m-bottom-15">
                                <div class="list-icon text-center">
                                    <i class="fa fa-phone fa-lg"></i>
                                </div>
                                <div class="list-content font-semi-bold" ng-repeat="contact in companyProfile[0].emp_id.contacts">
                                    {{contact.NumberType}} : {{contact.number}}
                                </div>
                            </li>
                            <li class="m-bottom-15">
                                <div class="list-icon text-center">
                                    <i class="fa fa-globe fa-lg"></i>
                                </div>
                                <div class="list-content font-semi-bold ">{{companyProfile[0].emp_id.website}}</div>
                            </li>
                            <li class="m-bottom-15">
                                <div class="list-icon text-center">
                                    <i class="fa fa-envelope fa-lg"></i>
                                </div>
                                <div class="list-content font-semi-bold ">{{companyProfile[0].emp_id.email}}</div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section section-border-bottom paddingB-40" id="jobInformationSection">
        <div class="container">
            <div class="paddingTB-40">
                <div class="text-center">
                    <span class="font-30">Job Posting Information</span>
                    <hr class="star-primary">
                </div>
                <div ng-repeat="jobs in companyProfile">
                    <div class="font-16 font-semi-bold m-top-30">
                        {{jobs.position}} : {{jobs.salary}}</div>
                    <div class="font-italic m-top-5">Qualification: {{qualify.qualify}}</div>
                    <div class="font-italic m-top-5">Requirements: {{required.require}}</div>
                    <p class="font-16 m-top-5"> {{jobs.description}}</p>
                </div>

            </div>
        </div>
    </div>

</div>
