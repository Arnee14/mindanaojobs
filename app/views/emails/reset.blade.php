<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta charset="utf-8">
		<title>Hire and Find Jobs Today | MindanaoJobs.com</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="keywords" content="">
		<meta name="author" content="">
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
		<style>
			@import url(//fonts.googleapis.com/css?family=Lato:700);
		</style>
	</head>
	<body style="margin:0;font-family:'Lato', sans-serif;text-align:center;color: #999;padding-top: 20px;padding-bottom: 20px;">
		<div style="
			width: 775px;
			padding-right: 15px;
			padding-left: 15px;
			margin-right: auto;
			margin-left: auto;
			">
			<div style="padding-right: 60px;padding-left: 60px;border-radius: 6px;text-align: left;border-bottom: 1px solid #e5e5e5;padding: 30px;margin-bottom: 30px;color: inherit;background-color: #eee;">
				<img src="http://s2.postimg.org/p0frb7ug5/logo.png">
				<br><br>
				<p>
					<p>Please do not reply to this email. This email has been sent by a machine, replies will not be read.</p> <br><br><br>
					<p>Dear {{$fullname}},</p>
					<p>Click the button below to reset your password.</p>
					<center>
						<a href="http://localhost:8000/recovery?mode={{$mode}}&id={{$token}}" role="button" style="text-decoration: none;padding: 14px 35px;font-size: 18px;line-height: 1.33;border-radius: 6px;color: #fff;background-color: #e74c3c;border-color: #c0392b;font-weight: 400;display: inline-block;text-align: center;white-space: nowrap;vertical-align: middle;cursor: pointer;background-image: none;border: 1px solid transparent;">
							Reset Password
						</a>
					</center>
					<p>If you need additional help, contact <a href="support@mindanaojobs.com">MindanaoJobs Support.</a></p>
				</p>
				<br><br><br>
				<p>Sincerely,</p>
				<p>
					Mindanao Jobs  <small>(<a href="#">www.mindanaojobs.com</a>)<small>
				</p>
				<br><br><br>
				<p style="text-align:center;margin-bottom: 15px;font-size: 16px;font-weight: 200;">
					Contact us at <a href="mailto:support@mindanaojobs.com">support@mindanaojobs.com</a> or at 123-4567-890.
				</p>
			</div>
		</div>
	</body>
</html>
