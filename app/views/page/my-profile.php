<div class="main-container no-bottom-space">
    <?php if(isset($User) && !empty($User)){
        if ($User->u_isverified != '1'){
    ?>
    <div class="section bg-danger paddingTB-10 alert-top section-border-bottom">
        <div class="container">
          <div class="row">
            <div class="col-sm-7">
                <i class="pull-left m-right-15 m-top-5 no-margin-xs fa fa-3x fa-exclamation-triangle text-danger"></i>
                <div class="font-21 font-semi-bold title-text"> Your account is currently disabled. </div>
                <div class="font-lato information-text">
                    <span class="font-14 font-semi-bold">Check your email to verify your account. <a href="javascript:resendConfirmation()">Click here</a></span> to resend confirmation
                </div>
            </div>
          </div>
        </div>
    </div>
    <?php
            }
        }
    ?>
    <div class="section section-border-bottom paddingB-40">
        <div class="container">
            <div class="paddingTB-40">
                <div class="row">
                    <div class="col-md-6">
                        <div class="font-30 font-ultra-bold font-lato line-1">
                            Update Profile
                        </div>
                        <div class="font-18 m-top-10 m-bottom-20">
                            Update/Edit Your Profile
                        </div>
                    </div>
                </div>
                <!-- ./row -->
            </div>
            <!-- ./paddingTB -->
        </div>
        <!-- ./container -->
    </div>

    <div class="section">
        <div class="bg-grey">
            <div class="container">
                <ul id="myTab" class="nav nav-tabs custom-tab">
                    <li class="active"><a href="#overviewTab" >Overview</a></li>
                    <li class=""><a href="#educationalTab" >Educational Background</a></li>
                    <li class="tab-lg "><a href="#skillTab" >Skills And Specialization</a></li>
                    <li class="tab-lg "><a href="#professionTab" >Professional Experience</a></li>
                </ul>

                <div id="myTabContent" class="tab-content">
                    <div role="tabpanel" class="tab-pane overviewTab fade in active" id="overviewTab" aria-labelledBy="overviewTab">
                        <div class="bg-grey paddingT-40 paddingB-100">
                            <div class="container">
                                <div class="font-24 font-semi-bold line-1">Profile Overview</div>
                                <form role="form" data-parsley-validate name="userForm" id="userForm" class="form-horizontal m-top-40">
                                    <input type="hidden" name="cmd" value="_add_lancer_detail" />
                                    <input type="hidden" name="zipcode" value="0" />
                                    <input type="hidden" name="subcmd" value="profile_page" />
                                    <div class="form-group">
                                        <label for="desiredSalary" class="col-lg-2 col-sm-3 control-label font-normal font-14 text-left">Desired Salary:</label>
                                        <div class="col-sm-9 col-lg-7">
                                            <input type="text" class="form-control style-form" id="desiredSalary" name="desiredSalary" value="<?php if(isset($User) && !empty($User)){ echo $User->js_desiredsalary; } ?>"
                                            required data-parsley-group="block-account" data-parsley-required-message="Please enter your desired Salary">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="professionalTitle" class="col-lg-2 col-sm-3 control-label font-normal font-14 text-left">Description :</label>
                                        <div class="col-sm-9 col-lg-7">
                                            <textarea class="form-control leftchar"  rows="12" id="qualification" name="qualification"
                                                onkeyup="parsleyLeft(this)" data-parsley-length="[100, 3000]" data-parsley-group="block-account"
                                                required data-parsley-rangelength="[100,3000]" data-parsley-minlength="100"
                                                data-parsley-maxlength="3000" maxlength="3000"><?php if(isset($User) && !empty($User)){ echo $User->js_desc; } ?></textarea>
                                            <div class="col-md-12 text-right">
                                                <span class="left">3000</span>characters left</div>
                                                <p class="block text-muted font-12 m-top-10">Help clients get to know you and your professional background.</p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="professionalTitle" class="col-lg-2 col-sm-3 control-label font-normal font-14 text-left">Upload Resume :</label>
                                        <div class="col-sm-9 col-lg-7">
                                            <div class="col-sm-3 col-lg-3">
                                                <div class="mask add-new-portfolio" id="upload-profile-photos">
                                                    <a class="btn btn-primary m-left-sm">Upload</a>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-lg-6">
                                                <div class="portfolio-inner">
                                                    <div class="portfolio-title">
                                                        <?php if(isset($User) && !empty($User) && !empty($User->u_resume)){
                                                            echo '<div class="font-semi-bold font-14">
                                                                    <a class="resume" href="javascript:void(0)">'.
                                                                        $User->u_resume
                                                                    .'</a>
                                                                </div>';
                                                        }else{
                                                            echo '<div class="font-semi-bold font-14">(No Attachment)</div>';
                                                        } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="professionalTitle" class="col-lg-2 col-sm-3 control-label font-normal font-14 text-left"></label>
                                        <div class="col-sm-9 col-lg-7 text-right"> <a class="btn btn-danger btn-wide" href="javascript:void(0)" onclick="saveUserOverview();">Save</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- ./container -->
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade educationalTab" id="educationalTab" aria-labelledBy="educationalTab">
                        <div class="bg-grey paddingT-20 paddingB-100">
                            <div class="container">
                                <form role="form" data-parsley-validate name="educForm" id="educForm" class="form-horizontal m-top-40">
                                    <div class="font-24 font-semi-bold line-1">
                                        Education
                                    </div>
                                    <div class="education-form">
                                        <?php
                                        if(isset($Education) && !empty($Education)) {
                                            $index = 0;
                                        foreach ($Education as $key) {
                                            $index += 1;
                                        ?>
                                        <div class="education-area form-active m-top-40">
                                            <div class="line-break"></div>

                                            <div class="form-group m-top-md">
                                                <label for="universityName" class="col-sm-3 control-label font-semi-bold font-14 text-left">Attainment :</label>
                                                <div class="col-sm-9 col-md-7">
                                                    <input type="text" value="<?php echo $key->ed_attainment; ?>" class="form-control" placeholder="Attainment..." id="attainment" name="attainment[]">
                                                </div>
                                            </div>

                                            <div class="form-group m-top-md">
                                                <label for="universityName" class="col-sm-3 control-label font-semi-bold font-14 text-left">University :</label>
                                                <div class="col-sm-9 col-md-7">
                                                    <input type="text" value="<?php echo $key->ed_institution; ?>" class="form-control" placeholder="University..." id="universityname" name="universityname[]">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="degree" class="col-sm-3 control-label font-semi-bold font-14 text-left">Degree :</label>
                                                <div class="col-sm-9 col-md-7">
                                                    <input type="text" class="form-control" value="<?php echo $key->ed_field; ?>" placeholder="Degree..." id="degree" name="degree[]">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="fieldOfStudy" class="col-sm-3 control-label font-semi-bold font-14 text-left">Field of Study :</label>
                                                <div class="col-sm-9 col-md-7">
                                                    <input type="text" class="form-control" value="<?php echo $key->ed_major; ?>" placeholder="Field of Study..." id="fieldOfStudy" name="fos[]">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="fieldOfStudy" class="col-sm-3 control-label font-semi-bold font-14 text-left">Address :</label>
                                                <div class="col-sm-9 col-md-7">
                                                    <input type="text" class="form-control" value="<?php echo $key->ed_address; ?>" placeholder="Address..." id="address" name="address[]">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-3 control-label font-semi-bold font-14 text-left">Time Period :</label>
                                                <div class="col-sm-9 col-md-7">
                                                    <div class="inline-block m-bottom-sm">
                                                        <select class="form-control inline-block" style="width:85px;" id="sypday" name="sypday[]" required data-parsley-group="block-account">
                                                            <option value="">Year</option>
                                                            <?php
                                                                if(isset($Years) && !empty($Years)){
                                                                    foreach ($Years as $year) {
                                                                        if ($year == $key->ed_yearfrom){
                                                                            echo '<option value="'.$year.'" selected="selected">'.$year.'</option>';
                                                                        }else{
                                                                            echo '<option value="'.$year.'">'.$year.'</option>';
                                                                        }
                                                                    }
                                                                }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <span class="m-left-xs m-right-xs chide">to</span>
                                                    <div class="inline-block chide">
                                                        <select class="form-control inline-block" style="width:85px;" id="eypday" name="eypday[]">
                                                            <option value="">Year</option>
                                                            <?php
                                                                if(isset($Years) && !empty($Years)){
                                                                    foreach ($Years as $year) {
                                                                        if ($year == $key->ed_yearto){
                                                                            echo '<option value="'.$year.'" selected="selected">'.$year.'</option>';
                                                                        }else{
                                                                            echo '<option value="'.$year.'">'.$year.'</option>';
                                                                        }
                                                                    }
                                                                }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group m-top-md rm-education">
                                                <label for="professionalTitle" class="col-sm-3 control-label"></label>
                                                <div class="col-sm-7 text-right">
                                                    <a href="javascript:void(0)" class="btn btn-danger" onclick="RForm(this,'education')"><i class="fa fa-minus"></i> Remove Education</a>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </div>
                                    <div class="form-group m-top-md">
                                        <label for="professionalTitle" class="col-sm-3 control-label"></label>
                                        <div class="col-sm-7 text-right">
                                            <div>
                                                <a class="btn btn-info" href="javascript:void(0)" onclick="FormAdd('education')"><i class="fa fa-plus"></i> Add Another Degree</a>
                                            </div>
                                            <div class="m-top-10">
                                                <a href="javascript:saveUserEducation();" class="btn btn-danger btn-wide">Save</a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane skillTab fade" id="skillTab" aria-labelledBy="skillTab">
                        <div class="bg-grey paddingT-40 paddingB-100">
                            <div class="container">
                                <div class="font-30 font-semi-bold line-1">Specialization</div>
                                <form role="form" data-parsley-validate name="educForm" class="form-horizontal m-top-40">
                                    <div class="form-group">
                                        <button type="button" data-toggle="modal" data-target="#editCategories" class="btn btn-info btn-sm"><i class="glyphicon glyphicon-plus"></i> Add Specialization
                                        </button>
                                    </div>
                                    <div class="form-group">
                                        <div class="m-top-25 m-left-20 font-12">
                                            <?php
                                                if (isset($Categories) && !empty($Categories)){
                                                    foreach ($Categories as $key) { ?>
                                                    <div class="skill-tag skill-sm m-right-10"><?php echo $key->jc_name; ?></div>
                                            <?php
                                                    }
                                                }
                                            ?>
                                        </div>
                                    </div>
                                </form>
                                <hr>
                                <div class="font-30 font-semi-bold line-1">Skills</div>
                                <form role="form" data-parsley-validate name="skillForm" id="skillForm" class="form-horizontal m-top-40">
                                    <!-- <div class="form-group">
                                        <a href="javascript:FormAdd('skills');" class="btn btn-info btn-sm"><i class="glyphicon glyphicon-plus"></i> Add Skills
                                        </a>
                                    </div> -->
                                    <div class="skills-form">
                                        <div class="skills-area">
                                            <div class="form-group">
                                                <label class="col-lg-2 col-sm-3 control-label font-semi-bold font-14 text-left">Search Skills:</label>
                                                <div class="col-sm-8 col-lg-8">
                                                    <input type="hidden" class="form-control cboSkills" id="cboSkills" name="cboSkills[]">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                                <div class="text-center">
                                    <label for="professionalTitle" class="col-lg-2 col-sm-3 control-label font-normal font-14 text-left"></label>
                                    <div class="col-sm-9 col-lg-7 text-right"> <a class="btn btn-danger btn-wide" href="javascript:void(0)" onclick="saveUserSkills()">Save</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane professionTab fade" id="professionTab" aria-labelledBy="professionTab">
                        <div class="bg-grey paddingT-40 paddingB-100">
                            <div class="container">
                                <div class="font-24 font-semi-bold line-1">Professional Experience</div>
                                <form role="form" data-parsley-validate name="proForm" id="proForm" class="form-horizontal m-top-40" novalidate>
                                    <div class="row">
                                        <div class="profession-form">
                                            <?php
                                                if(isset($Experience) && !empty($Experience)) {
                                                    $index = 0;
                                                    foreach ($Experience as $key) {
                                                        $index += 1;
                                            ?>
                                                <div class="profession-area paddingB-100">
                                                    <div class="form-group">
                                                        <label for="companyName" class="col-sm-3 control-label font-semi-bold font-14 text-left">Company Name :</label>
                                                        <div class="col-sm-9 col-md-7">
                                                            <input type="text" class="form-control" value="<?php echo $key->we_companyname; ?>" id="companyname" name="companyname[]" required data-parsley-group="block-account" data-parsley-required-message="Please enter Company Name">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="title" class="col-sm-3 control-label font-semi-bold font-14 text-left">Title :</label>
                                                        <div class="col-sm-9 col-md-7">
                                                            <input type="text" class="form-control" value="<?php echo $key->we_position; ?>" id="title" name="title[]" required data-parsley-group="block-account" data-parsley-required-message="Please enter Job Title">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="location" class="col-sm-3 control-label font-semi-bold font-14 text-left">Location :</label>
                                                        <div class="col-sm-9 col-md-7">
                                                            <input type="text" class="form-control" value="<?php echo $key->we_location; ?>"  id="location" name="location[]" required data-parsley-group="block-account" data-parsley-required-message="Please enter Company Location">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label font-semi-bold font-14 text-left">Time Period :</label>
                                                        <div class="col-sm-9 col-md-7">
                                                            <div class="inline-block m-bottom-sm">
                                                                <select class="form-control inline-block" style="width:85px;" id="sypday" name="sypday[]" required data-parsley-group="block-account">
                                                                    <option value="">Year</option>
                                                                    <?php
                                                                        if(isset($Years) && !empty($Years)){
                                                                            foreach ($Years as $year) {
                                                                                if($year == $key->we_yearstart){
                                                                                    echo '<option value="'.$year.'" selected="selected">'.$year.'</option>';
                                                                                }else{
                                                                                    echo '<option value="'.$year.'">'.$year.'</option>';
                                                                                }
                                                                            }
                                                                        }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                            <span class="m-left-xs m-right-xs chide">to</span>
                                                            <div class="inline-block chide">
                                                                <select class="form-control inline-block" style="width:85px;" id="eypday" name="eypday[]">
                                                                    <option value="">Year</option>
                                                                    <?php
                                                                        if(isset($Years) && !empty($Years)){
                                                                            foreach ($Years as $year) {
                                                                                if($year == $key->we_yearend){
                                                                                    echo '<option value="'.$year.'" selected="selected">'.$year.'</option>';
                                                                                }else{
                                                                                    echo '<option value="'.$year.'">'.$year.'</option>';
                                                                                }
                                                                            }
                                                                        }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label font-semi-bold font-14 text-left">Description :</label>
                                                        <div class="col-sm-7">
                                                            <textarea rows="12" class="form-control" id="description" name="description[]" required data-parsley-group="block-account" data-parsley-required-message="Please enter Job Description"><?php echo $key->we_description; ?></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-top-md rm-profession">
                                                        <label for="professionalTitle" class="col-sm-3 control-label"></label>
                                                        <div class="col-sm-7 text-right">
                                                            <a href="javascript:void(0)" class="btn btn-danger" onclick="RForm(this,'profession');"><i class="fa fa-trash"></i> Remove</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>

                                    <div class="form-group m-top-md">
                                        <label for="professionalTitle" class="col-sm-3 control-label"></label>
                                        <div class="col-sm-7 text-right">
                                            <div>
                                                <a href="javascript:void(0)" onclick="FormAdd('profession');" class="btn btn-info btn-wide">
                                                    <i class="glyphicon glyphicon-plus"></i> Add Work Experienced
                                                </a>
                                            </div>
                                            <div class="m-top-10">
                                                <a href="javascript:void(0);" class="btn btn-lg btn-danger btn-wide" onclick="saveUserWork()">Save <i class="fa fa-angle-double-right"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="profession-form-hidden hidden">
    <div class="profession-area form-active paddingB-100" style="display:none">
        <div class="form-group">
            <label for="companyName" class="col-sm-3 control-label font-semi-bold font-14 text-left">Company Name :</label>
            <div class="col-sm-9 col-md-7">
                <input type="text" class="form-control" id="companyname" name="companyname[]" required data-parsley-group="block-account" data-parsley-required-message="Please enter Company Name">
            </div>
        </div>
        <div class="form-group">
            <label for="title" class="col-sm-3 control-label font-semi-bold font-14 text-left">Title :</label>
            <div class="col-sm-9 col-md-7">
                <input type="text" class="form-control" id="title" name="title[]" required data-parsley-group="block-account" data-parsley-required-message="Please enter Job Title">
            </div>
        </div>
        <div class="form-group">
            <label for="location" class="col-sm-3 control-label font-semi-bold font-14 text-left">Location :</label>
            <div class="col-sm-9 col-md-7">
                <input type="text" class="form-control" id="location" name="location[]" required data-parsley-group="block-account" data-parsley-required-message="Please enter Company Location">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label font-semi-bold font-14 text-left">Time Period :</label>
            <div class="col-sm-9 col-md-7">
                <div class="inline-block m-bottom-sm">
                    <select class="form-control inline-block" style="width:85px;" id="sypday" name="sypday[]" required data-parsley-group="block-account">
                        <option value="">Year</option>
                        <?php
                            if(isset($Years) && !empty($Years)){
                                foreach ($Years as $year) {
                                    echo '<option value="'.$year.'">'.$year.'</option>';
                                }
                            }
                        ?>
                    </select>
                </div>
                <span class="m-left-xs m-right-xs chide">to</span>
                <div class="inline-block chide">
                    <select class="form-control inline-block" style="width:85px;" id="eypday" name="eypday[]">
                        <option value="">Year</option>
                        <?php
                            if(isset($Years) && !empty($Years)){
                                foreach ($Years as $year) {
                                    echo '<option value="'.$year.'">'.$year.'</option>';
                                }
                            }
                        ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label font-semi-bold font-14 text-left">Description :</label>
            <div class="col-sm-7">
                <textarea rows="12" class="form-control" id="description" name="description[]" required data-parsley-group="block-account" data-parsley-required-message="Please enter Job Description"></textarea>
            </div>
        </div>
        <div class="form-group m-top-md rm-profession">
            <label for="professionalTitle" class="col-sm-3 control-label"></label>
            <div class="col-sm-7 text-right">
                <a href="javascript:void(0)" class="btn btn-danger" onclick="RForm(this,'profession');"><i class="fa fa-trash"></i> Remove</a>
            </div>
        </div>
    </div>
</div>

<div class="skills-form-hidden hidden">
    <div class="skills-area form-active" style="display:none">
        <div class="form-group">
            <div class="col-sm-8 col-lg-8">
                <input type="hidden" name="cboSkills[]" class="form-control cboskills">
            </div>
            <div class="col-sm-3 col-lg-3">
                <select name="proficiecy" id="proficiecy" class="form-control" data-parsley-group="block-account">
                    <option value="beginner">Beginner</option>
                    <option value="intermediate">Intermediate</option>
                    <option value="advanced">Advanced</option>
                </select>
            </div>
            <div class="col-sm-1 col-lg-1">
                <a  href="javascript:void(0)" onclick="RForm(this,'skills')" style="margin-top: 5px;"><i class="glyphicon glyphicon-trash"></i></a>
            </div>
        </div>
    </div>
</div>

<div class="education-form-hidden hidden">
    <div class="education-area form-active m-top-40" style="display:none">
        <div class="line-break"></div>

        <div class="form-group m-top-md">
            <label for="universityName" class="col-sm-3 control-label font-semi-bold font-14 text-left">Attainment :</label>
            <div class="col-sm-9 col-md-7">
                <input type="text" value="" class="form-control" placeholder="Attainment..." id="attainment" name="attainment[]" required data-parsley-group="block-account">
            </div>
        </div>

        <div class="form-group m-top-md">
            <label for="universityName" class="col-sm-3 control-label font-semi-bold font-14 text-left">University :</label>
            <div class="col-sm-9 col-md-7">
                <input type="text" value="" class="form-control" placeholder="University..." id="universityname" name="universityname[]" required data-parsley-group="block-account">
            </div>
        </div>

        <div class="form-group">
            <label for="degree" class="col-sm-3 control-label font-semi-bold font-14 text-left">Degree :</label>
            <div class="col-sm-9 col-md-7">
                <input type="text" class="form-control" value="" placeholder="Degree..." id="degree" name="degree[]" required data-parsley-group="block-account">
            </div>
        </div>

        <div class="form-group">
            <label for="fieldOfStudy" class="col-sm-3 control-label font-semi-bold font-14 text-left">Field of Study :</label>
            <div class="col-sm-9 col-md-7">
                <input type="text" class="form-control" value="" placeholder="Field of Study..." id="fieldOfStudy" name="fos[]" required data-parsley-group="block-account">
            </div>
        </div>

        <div class="form-group">
            <label for="fieldOfStudy" class="col-sm-3 control-label font-semi-bold font-14 text-left">Address :</label>
            <div class="col-sm-9 col-md-7">
                <input type="text" class="form-control" value="" placeholder="Address..." id="address" name="address[]" required data-parsley-group="block-account">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label font-semi-bold font-14 text-left">Time Period :</label>
            <div class="col-sm-9 col-md-7">
                <div class="inline-block m-bottom-sm">
                    <select class="form-control inline-block" style="width:85px;" id="sypday" name="sypday[]" required data-parsley-group="block-account">
                        <option value="">Year</option>
                        <?php
                            if(isset($Years) && !empty($Years)){
                                foreach ($Years as $year) {
                                    echo '<option value="'.$year.'">'.$year.'</option>';
                                }
                            }
                        ?>
                    </select>
                </div>
                <span class="m-left-xs m-right-xs chide">to</span>
                <div class="inline-block chide">
                    <select class="form-control inline-block" style="width:85px;" id="eypday" name="eypday[]">
                        <option value="">Year</option>
                        <?php
                            if(isset($Years) && !empty($Years)){
                                foreach ($Years as $year) {
                                    echo '<option value="'.$year.'">'.$year.'</option>';
                                }
                            }
                        ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group m-top-md rm-education">
            <label for="professionalTitle" class="col-sm-3 control-label"></label>
            <div class="col-sm-7 text-right">
                <a href="javascript:void(0)" class="btn btn-danger" onclick="RForm(this,'education')"><i class="fa fa-minus"></i> Remove Education</a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="editCategories" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div class="text-center">
                    <div class="font-30 font-lato">What Type of
                        <span class="font-ultra-bold">Specialization are You Looking For?</span>
                    </div>
                    <div class="row m-top-10 m-bottom-10">
                        <div class="col-md-8 col-md-offset-2">
                            <p class="line-28">Select the categories that best match your skill set. We will use this information to match you to the best jobs. Select up to 20 categories below.</p>
                        </div>
                    </div>
                </div>

                <div class="row font-12">
                    <?php
                        if (isset($jobcategories['jobcategories'])){
                            foreach ($jobcategories['jobcategories'] as $categories) {
                                 if (isset($categories['subcategory']) && !empty($categories['subcategory'])){
                                    echo '<div class="col-md-4">
                                            <div class="font-14 font-semi-bold m-bottom-20 m-top-20">'.$categories['description'].'</div>';
                                            foreach ($categories['subcategory'] as $cat) {
                                                if (isset($Categories) && !empty($Categories)){
                                                    foreach ($Categories as $key) {
                                                        if(strcmp(trim($key->jc_name),trim($cat['description']))== 0){
                                                             $isChecked = 'checked="checked"';
                                                             break;
                                                        }else{
                                                            $isChecked = '';
                                                        }
                                                    }
                                                    echo '<div class="m-top-10 cls_category">
                                                        <div class="custom-checkbox">
                                                            <input type="checkbox" id="'.$cat['description'].'" class="item-check" name="subcat[]" value="'.$cat['description'].'"'.$isChecked .'>
                                                            <label for="'.$cat['description'].'"></label>
                                                        </div>
                                                        '.$cat['description'].'
                                                    </div>';
                                                } else{
                                                    echo '<div class="m-top-10 cls_category">
                                                        <div class="custom-checkbox">
                                                            <input type="checkbox" id="'.$cat['description'].'" class="item-check" name="subcat[]" value="'.$cat['description'].'" >
                                                            <label for="'.$cat['description'].'"></label>
                                                        </div>
                                                        '.$cat['description'].'
                                                    </div>';
                                                }
                                            }
                                    echo '</div>';
                                }
                            }
                        }
                    ?>
                </div>
                <div class="m-top-40 text-center">
                    <button onclick="saveUserSpecialization()" type="button" class="btn btn-danger">Save Changes</button>
                    <button type="button" class="btn btn-inverse m-left-10" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>
