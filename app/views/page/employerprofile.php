<div class="main-container no-bottom-space">
    <div class="section section-border-bottom paddingB-40 bg-grey" id="overviewSection">
        <div class="container">
            <div class="paddingTB-40">
                <div class="row">
                    <div class="col-md-2">
                        <div class="clearfix">
                            <?php if(isset($Employers) && !empty($Employers) && !empty($Employers->u_displaypicture)) { ?>
                                <img src="../images/employer/<?php echo $Employers->u_displaypicture; ?>" alt="..." class="applicant-profile-pic" style="border-radius:50em">
                            <?php }else{ ?>
                                <img src="../img/photo.jpg" alt="..."class="applicant-profile-pic" style="border-radius:50em">
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-md-6 m-top-20-sm">
                        <div class="font-lato font-30"><?php if(isset($Employers) && !empty($Employers)) { echo ($Employers->e_companyname); }?></div>
                        <div class="m-top-5 font-semi-bold"><?php if(isset($Employers) && !empty($Employers)) { echo ($Employers->e_description); }?></div>
                        <ul class="list-style-icon m-top-40">
                            <li class="m-bottom-15">
                                <div class="list-icon text-center">
                                    <i class="fa fa-map-marker fa-lg"></i>
                                </div>
                                <div class="list-content font-semi-bold "><?php if(isset($Employers) && !empty($Employers)) { echo ''.$Employers->u_address.', '.$Employers->u_city.', '.$Employers->u_province.' '.$Employers->u_zipcode; }?></div>
                            </li>
                            <li class="m-bottom-15">
                                <div class="list-icon text-center">
                                    <i class="fa fa-phone fa-lg"></i>
                                </div>
                                <div class="list-content font-semi-bold">
                                    <?php
                                        if($Employers->u_telephoneno != ''){
                                           echo 'Landline: '; echo ($Employers->u_telephoneno);
                                        }
                                        if($Employers->u_mobileno != ''){
                                           echo ' Mobile: '; echo ($Employers->u_mobileno);
                                        }
                                    ?>
                                </div>
                            </li>
                            <li class="m-bottom-15">
                                <div class="list-icon text-center">
                                    <i class="fa fa-globe fa-lg"></i>
                                </div>
                                <div class="list-content font-semi-bold "><?php if(isset($Employers) && !empty($Employers)) { echo ($Employers->u_website); }?></div>
                            </li>
                            <li class="m-bottom-15">
                                <div class="list-icon text-center">
                                    <i class="fa fa-envelope fa-lg"></i>
                                </div>
                                <div class="list-content font-semi-bold "><?php if(isset($Employers) && !empty($Employers)) { echo ($Employers->u_email); }?></div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section section-border-bottom paddingB-40" id="jobInformationSection">
        <div class="container">
            <div class="paddingTB-40">
                <div class="text-center">
                    <span class="font-30">Job Posting Information</span>
                    <hr class="star-primary">
                </div>
                <?php
                    if(isset($Job) && !empty($Job)){

                    foreach ($Job as $key) {
                        foreach ($key['info'] as $key1 ) { ?>

                        <div class="font-16 font-semi-bold m-top-30">
                            Position: <?php echo $key1->j_title ?>
                        </div>
                        <div class="font-italic m-top-5">Salary: â‚±<?php if(isset($Employers) && !empty($Employers)) { echo $key1->j_salaryfrom ?> - â‚±<?php echo $key1->j_salaryto; }?></div>
                        <div class="font-italic m-top-5">Requirements: <?php if(isset($Employers) && !empty($Employers)) { echo $key1->j_requirements; }?></div>
                        <p class="font-16 m-top-5"> Description: <?php if(isset($Employers) && !empty($Employers)) { echo $key1->j_description; }?></p>
                        <p style="width: 450px;font-size: 14px;">Categories:
                        <div style="width:850px;">
                           <?php
                                foreach ($key['categories'] as $keys ) {
                                    echo '<div class="skill-tag skill-sm m-right-10" style="font-size: 12px;">'.$keys['category']->jc_jobcategory.'</div>';
                                }
                            ?>
                        </p>
                        </div>
                        <p style="width: 450px;font-size: 14px;">Skills:
                        <div style="width:850px;">
                           <?php
                                foreach ($key['skills'] as $keys ) {
                                    echo '<div class="skill-tag skill-sm m-right-10" style="font-size: 12px;">'.$keys['skills']->js_skill.'</div>';
                                }
                            ?>
                        </p>
                        </div>
                        <div class="line-break"></div>
                   <?php
                            }
                        }
                    }
                ?>



            </div>
            <div class="panel-group text-center">
                <div class="SearchPagination">
                     <?php echo $Job->links(); ?>
                </div>
            </div>
        </div>
    </div>

</div>

