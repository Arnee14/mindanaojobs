if ($('#password').val() != '') {
    $("#password-val").hide();
}

function password_autocom() {
    if ($('#password').val() != '') {
        $("#password-val").hide();
    } else {
        $("#password-val").show();
    }
}

function password_chk() {
    $("#password-val").hide();
    $("#password").focus();

}

function u_chk() {
    if ($("#username").val() == 'Email') {
        $("#username").val('');
    }
}

function password_blur(obj) {

    if ($(obj).val() == '') {
        $("#password-val").show();
        $(obj).removeClass("input-bx-act");
    } else {
        $(obj).addClass("input-bx-act");
    }
}

function password_rem(obj) {
    $("#password-val").hide();
}

function u_blur(obj) {
    if ($("#username").val() == '') {
        $("#username").val('Email');
        $("#username").removeClass("input-bx-act");
    } else {
        $("#username").addClass("input-bx-act");
    }
}

function replaceBrwithNewline(str) {
    return str.replace(/<br\s*[\/]?>/gi, "\n");
}
